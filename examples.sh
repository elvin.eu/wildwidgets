dmake examples

for X in `ls examples/target/ -1`;
do
	answer=""
	until [ "$answer" = "y" ] || [ "$answer" = "n" ] || [ "$answer" = "q" ]
	do
		echo -n "run examples/target/$X? (y|n|q) "
		read -n 1 answer
		echo
	done

	if [ "$answer" = "y" ]
	then
		examples/target/$X;
	elif [ "$answer" = "q" ]
	then
		break;
	elif [ "$answer" = "n" ]
	then
		continue
	fi
done

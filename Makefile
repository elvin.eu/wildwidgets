# wcw Makefile
include ./Config

INCDIR = $(INSTALLDIR)/include/d/wcw
LIBDIR = $(INSTALLDIR)/lib

.PHONY: lib examples tools again clean all

all:
	$(MAKE) tools
	$(MAKE) lib
	$(MAKE) examples

tools:
	$(MAKE) -C tools

lib:
	$(MAKE) -C lib

examples:
	$(MAKE) -C examples

clean:
	$(MAKE) clean -C examples
	$(MAKE) clean -C lib
	$(MAKE) clean -C tools

again:
	$(MAKE) clean
	$(MAKE) all

install:
	install -v -D -t $(LIBDIR) lib/target/libwcw.so
	(cd lib/source && find -type f -exec install "{}" -D $(INCDIR)"/wcw/{}" \;)
	(cd include && find -type f -exec install "{}" -D $(INCDIR)"/{}" \;)

uninstall:
	rm $(LIBDIR)/libwcw.so
	rm -r $(INCDIR)
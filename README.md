**WildWidgets** is a set of widgets that are primarily being developed for **WildCAD**, a very simple 2D drawing application

**Clone** this project:
`git clone https://gitlab.com/elvin.eu/wildwidgets.git`

**Libraries:** X11, Cairo, Freetype2, Fontconfig

**Tools:** DMD, GNU Make

**Compile:** Cd into the WildCAD directory and call 'make'.

**Install:** As root, call 'make install'

**Examples:** there's a folder calles "examples/target" that contains a few, well, examples.

import wcw;

void main (string [] args)
{
	new Window("Hello World!").show();	// open a window with a headline and show it
	App.run;							// run the application until CTRL-C on the calling console
										// or the window's close button is pressed
	App.destroy;
}

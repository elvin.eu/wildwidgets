import wcw;
import std.stdio;


void main (string [] args)
{
    App (args);     // initialize X11, the main event loop

	auto window = new Window ("WildWidgets Menu Example");
	new VerticalLayout (window);

	auto menubar = new MenuBar (window);

    auto menu_file = new Menu ("File", menubar);
	auto item_file_load = new MenuItem ("Load", menu_file);
    auto item_file_save = new MenuItem ("Save", menu_file);
    auto item_file_quit = new MenuItem ("Quit", menu_file);

    auto menu_edit = new Menu ("Edit", menubar);
    auto item_edit_cut   = new MenuItem ("Cut",   menu_edit);
    auto item_edit_copy  = new MenuItem ("Copy",  menu_edit);
    auto item_edit_paste = new MenuItem ("Paste", menu_edit);

	auto menu_edit_format          = new Menu     ("Format", menu_edit);
	auto item_edit_format_reformat = new MenuItem ("Reformat Paragraph", menu_edit_format);
	auto item_edit_format_indent   = new MenuItem ("Indent Paragraph",   menu_edit_format);

    auto menu_help       = new Menu     ("Help", menubar);
    auto item_help_about = new MenuItem ("About", menu_help);
    auto item_help_help  = new MenuItem ("Help",  menu_help);

	item_file_quit.clicked.connect (&App.quit);

	auto widget = new Widget (window);
	widget.addFrame();
	widget.setFixSize (IPair(200,200));

	window.show;
	App.run;
	App.destroy;
}

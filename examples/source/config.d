import wcw;

void main (string [] args)
{
    App (args);

	auto window = new Window ("WildWidgets Vertical Layout Example");
	new VerticalLayout (window);

	auto save = new Button ("save", window);
	save.clicked.connect (&App.config.save);

	auto quit = new Button ("quit", window);
	quit.clicked.connect (&App.quit);

	window.show();
	App.run ();

}

import std;
import wcw;

void main (string [] args)
{
	App (args);

	auto m = new Main ();
	m.show;
	App.run;
	App.destroy;
}

class Main : Window
{
	this ()
	{
		super ();
		new VerticalLayout (this);

		auto y = new Button ("Yes/No", this);
		auto o = new Button ("Open File", this);
		auto s = new Button ("Save File", this);
		auto q = new Button ("Quit", this);

		y.clicked.connect (&yesNo);
		o.clicked.connect (&open);
		s.clicked.connect (&save);
		q.clicked.connect (&App.quit);
	}


	void yesNo ()
	{
		auto r = Dialog.Message (this, "Please answer the question:", "Yes or No?", "ync");
		writefln ("Dialog returned code %s", r);
	}

	void open ()
	{
		auto f = Dialog.FileOpen (this);
		writefln ("Dialog returned %s", f);
	}

	void save ()
	{
		auto f = Dialog.FileSave (this);
		writefln ("Dialog returned %s", f);
	}

}

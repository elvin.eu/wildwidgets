import std;

import wcw;
import cairo;
import cairo.classic;

static import  X11.Xlib;

void main (string [] args)
{
    App (args);
	auto window = new Window ();
	new GridLayout (window);

	auto x1 = new XWidget (window, 0, 0);
	auto x2 = new XWidget (window, 0, 1);
	auto x3 = new XWidget (window, 1, 0);
	auto x4 = new XWidget (window, 1, 1);

	window.show;
	App.run;
	App.destroy;
}

class XWidget : Widget
{
	this (Widget parent, int[] i...)
	{
		super (parent, i);

		setMinSize (IPair(50, 50));
		setMaxSize (IPair(-1, -1));
	}

	override void paint(CairoContext context)
	{
		super.paint(context);

		version (X11)
		{

			super.paint(context);

			context.set_source_color (Color.Green);
			context.arc (size.width/2, size.height*0.7, size.height/10, 0, cast(double)PI*2);
			context.stroke;

			// window.surface must be casted to a CairoXlibSurface. This works only,
			// because window.surface actually IS a CairoXlibSurface (see definition of backend)

			auto xsurface = cast(CairoXlibSurface) (window.surface);
			assert (xsurface, "window.surface is not a Xlib surface");

			auto display  = xsurface.get_display;
			auto drawable = xsurface.get_drawable;
			if (display is null || drawable == 0) return;

			X11.GC gc = X11.XCreateGC(display, drawable, 0, null);
			X11.XSetForeground(display, gc, 0xcc0000);

			// this is the whole area of the window; drawing needs to be restricted to the area
			// of the widget -- if neccessary by clipping

			// rectangle's from Widget
			with (rectangle)
			{
				auto r = X11.XRectangle (cast(short)(width*0.4),
				                         cast(short)(height*0.2),
				                         cast(ushort)(width*0.2),
				                         cast(ushort)(height*0.6));

				X11.XSetClipRectangles(display, gc, cast(int)x0, cast(int) y0, &r, 1, X11.Unsorted);

				int ax = cast(int)(x0+width*0.7); int ay = cast(int)(y0+height*0.9);
				int bx = cast(int)(x0+width*0.3); int by = cast(int)(y0+height*0.9);
				int cx = cast(int)(x0+width*0.7); int cy = cast(int)(y0+height*0.5);
				int dx = cast(int)(x0+width*0.3); int dy = cast(int)(y0+height*0.5);
				int ex = cast(int)(x0+width*0.5); int ey = cast(int)(y0+height*0.1);

				// Draw the lines
				X11.XDrawLine (display, drawable, gc, ax, ay, bx, by); 	// no clipping needed since we don't
				X11.XDrawLine (display, drawable, gc, bx, by, cx, cy);	// draw outside of the widget's area
				X11.XDrawLine (display, drawable, gc, cx, cy, dx, dy);
				X11.XDrawLine (display, drawable, gc, dx, dy, ax, ay);
				X11.XDrawLine (display, drawable, gc, ax, ay, cx, cy);
				X11.XDrawLine (display, drawable, gc, cx, cy, ex, ey);
				X11.XDrawLine (display, drawable, gc, ex, ey, dx, dy);
				X11.XDrawLine (display, drawable, gc, dx, dy, bx, by);
			}
			X11.XFlush(display);		// actually not needed here
			X11.XFreeGC(display, gc);
		}
	}
}

import wcw;

void main (string [] args)
{
    App (args);     								// initialize WildWidgets; the args parameter makes the program name appear in the window title
	auto window = new Window ();					// create an application window with the program name as default title
	auto l = new Label ("Hello, World", window);	// place some text at the default position at 0/0, the upper left corner of the window

	window.show; 									// show everything and run the application until the window gets closed or receives a termination signal (CTRL-C)
	App.run;
	App.destroy;
}

import wcw;

void main (string [] args)
{
    App (args);

	auto window = new Window ("WildWidgets Vertical Layout Example");
	new VerticalLayout (window);
	window.setAlignment (Align.CENTER);

	auto button1 = new Button ("quit 1", window);
	button1.setMaxSize (IPair (-1, 0));

	// buttons will be centered under button1
	auto button2 = new Button ("2", window);
	auto button3 = new Button ("3", window);
	auto button4 = new Button ("4", window);

	button1.clicked.connect (&App.quit);

	window.show;
	App.run;

	App.destroy;
}

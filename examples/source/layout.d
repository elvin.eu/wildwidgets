import std.conv;

import wcw;

import cairo;

void main (string [] args)
{
    App (args);

	auto window= new Window ("Layout Example");
	auto layout = new HorizontalLayout (window);
	window.setAlignment (Align.CENTER);

	auto v1 = new VerticalWidget (window);
	v1.setCaption ("v1");
	v1.setAlignment (Align.VCENTER);
	auto q = new Button ("v1 - quit", v1);

	auto v2 = new VerticalWidget (window);
	v2.setAlignment (Align.CENTER);
	v2.setCaption ("v2");

	auto h1 = new HorizontalWidget (v2);
	h1.setCaption ("h1");
	new Button ("h1 1", h1);
	new Button ("h1 2", h1);
	new Button ("h1 3", h1);

	auto h2 = new HorizontalWidget (v2);
	h2.setCaption ("h2");
	new Button ("h2 1", h2);
	new Spacer (h2);
	new Button ("h2 2", h2);

	auto h3 = new HorizontalWidget (v2);
	h3.setCaption ("h3");

	new Spacer (h3);
	new Button ("h3 1", h3);
	new Spacer (h3);
	new Button ("h3 2", h3);
	new Spacer (h3);

	auto h4 = new HorizontalWidget (v2);
	h4.setCaption ("h4");
	new Button ("h4 1", h4);
	new Button ("h4 2", h4);

	auto h5 = new HorizontalWidget (v2);
	h5.setCaption ("h5");
	new Button ("h5", h5);

	new Button ("v2", v2);

	q.clicked.connect (&App.quit);

	window.show;
	App.run;
	App.destroy;
}

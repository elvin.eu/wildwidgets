module wcw.layout;

import std.exception;
import std.algorithm;
import std.stdio; // debugging
import std.math;

import wcw.auxiliary;
import wcw.widget;
import wcw.window;
import wcw.application;
import wcw.cairo;
import wcw.event;
import wcw.enums;

/** Base class  for layouts. Controls the geometry of the widget it is assigned to and its subwidgets.
**/
class Layout
{
	Widget _owner;

public:
	/** Constructor

		Will assign itself to a widget.

		Params:
		widget = the owner of this Layout
	**/
	this (Widget widget)
	{
		enforce (widget);
		_owner = widget;
		_owner.setLayout (this);
	}

	// xxx
	~this ()
	{
		debug (shutdown) whereami ("~this Layout");
		destroyAllWidgets();
	}

	/// The widget, this layout is assigned to
	inout (Widget) owner () inout
	{
		assert (_owner);
		return _owner;
	}

	/// The spacing between widgets in this layout
	int spacing () const {return owner.spacing;}

	/// The margin at the borders of this widget
	int margin() const {return owner.margin;}

	/// The margin at the borders of this widget
	int frameWidth() const {return owner.frameWidth;}

	/// ditto
	IPair size () const {return owner.size;}
	/// ditto
	IPair position () const {return owner.position;}

	override istring toString () const
	{
		import std.string:lastIndexOf;

		string s = this.classinfo.name[lastIndexOf(this.classinfo.name, '.')+1..$];
		if (owner.caption != "") s ~= " " ~ owner.caption;
		return s;
	}

	/** Return a list of all Widgets that are mounted to this Layout.
	**/
	abstract Widget[] widgetList ();

	/// returns the nominal size of this widget depending on the nominal size of its subwidgets
	abstract IPair nomSize() const;

	/// returns the minimum size of this widget depending on the minimum size of its subwidgets
	abstract IPair minSize() const;

	/// returns the maximum size of this widget depending on the maximum size of its subwidgets
	abstract IPair maxSize() const;

	/// returns the extension size of this widget depending on the extension size of its subwidgets
	abstract IPair extSize() const;

	IPair[4] sizes () const
	{
		return [nomSize, minSize, maxSize, extSize];
	}

	/// returns the padding of this widget depending on the extended size of its subwidgets
	abstract IPair padding() const;

	/** Add a Widget to this Layout
	    There is a variable list of i parameters, the actual length depending on the
	    overridden methot in the subclasses
	**/
	abstract bool addWidget (Widget widget, int[] i ...);

	/** Remove and destroy all Widgets from this Layout
	**/
	abstract bool destroyAllWidgets ();

	/** This is the function, that recalculates the size and position of all widgets of this layout
	    (that is, all child widgets of the layout's owner widget)
	**/
	abstract void rearrange();
}

class SimpleLayout : Layout
{
protected:
	Widget [] _widgetList;

public:
	this (Widget widget)
	{
		super (widget);
	}

	override Widget[] widgetList () {return _widgetList;}


	override IPair nomSize() const {return owner.widgetNomSize;}
	override IPair minSize() const {return owner.widgetMinSize;}
	override IPair maxSize() const {return owner.widgetMaxSize;}
	override IPair extSize() const {return owner.widgetExtSize;}

	/// returns the padding of this widget depending on the extended size of its subwidgets
	override IPair padding() const {return IPair (0, 0);}

	/** Add a widget to this layout.
	**/
	override bool addWidget (Widget widget, int[] i ...)
	{
		enforce (i.length == 0, "wrong count of variadic parameters (exactly 0 allowed)");

		_widgetList ~= widget;
		widget.setParent (owner);

		return true;
	}

	override bool destroyAllWidgets ()
	{

		bool rc = _widgetList.length > 0;
		foreach (w; _widgetList) w.destroy;
		_widgetList.length = 0;
		return rc;
	}

	override void rearrange()
	{
		foreach (w; _widgetList)
		{
			w.setSize (w.widgetNomSize);
		}
	};
}

// helper struct and functions here, the function body comes after that:

// temporary structure, representing one element in a layout
// can be a margin, widget or spacing
enum _SPACING = -1;
enum _MARGIN  = -2;

struct _TmpParam
{
	size_t index;

	long min;
	long nom;
	long max;
	long ext;

	long diff;
	long size;
	long pos;
}


// calculate the length for all layout elements in templist
// length is the size either width or height, the layout's main direction
// layouts with 2 directions (e.g. grid) must call _rearrange twice, with different
// parameters
// elements include: a margin as first and last element,
//                   the widgets,
//                   and a space between each widget
// margins and spaces have a fixed size
// widgets can shrink or grow according to their min/nom/max settings
// target is the desired size in the layout's direction
// returns the size of the layout
int _arrange_linear (ref _TmpParam [] templist, const long target, Align aln)
{
	assert (target <= int.max);

	bool left, right, center;
	long remain = target;
	long gamma = 0;
	long delta = 0;
	foreach (ref tp; templist)
	{
		// stretch to min size
		tp.size = tp.min;
		remain -= tp.min;

		// prepare the next step
		tp.diff = tp.nom - tp.min;
		delta  += tp.diff;
	}

	// when remain is 0, the widgets occupy exactly the target space
	// when remain is negative, then the minimum size is larger
	// than target and the actual size is returned
	if (remain <= 0) goto spread;

	foreach (ref tp; templist)
	{
		// stretch to nom size
		if (tp.diff > 0)
		{
			long d = min (tp.diff, remain * tp.diff / delta);
			tp.size += d;
			remain  -= d;
			delta   -= tp.diff;
		}

		// prepare the next step
		tp.diff = tp.max - tp.nom;
		gamma += tp.diff;
	}

	// when remain is 0, the widgets occupy exactly the target space
	if (remain == 0) goto spread;
	assert (remain > 0);

	delta = 0;
	foreach (ref tp; templist)
	{
		// stretch to max size
		if (tp.diff > 0)
		{
			long d = min (tp.diff, remain * tp.diff / gamma);
			tp.size += d;
			remain  -= d;
			gamma   -= tp.diff;
		}

		// prepare the next step
		tp.diff = tp.ext - tp.max;
		delta += tp.diff;
	}

	if (delta > 0) foreach (ref tp; templist)
	{
		// stretch to ext size
		if (tp.diff > 0)
		{
			long d = min (tp.diff, remain * tp.diff / delta);
			tp.size += d;
			remain  -= d;
			delta   -= tp.diff;
		}
	}

	// when remain is 0, the widgets occupy exaclly the target space
	if (remain == 0) goto spread;
	assert (remain > 0);

	if      (aln==Align.HCENTER || aln==Align.VCENTER) center = true;
	else if (aln==Align.RIGHT   || aln==Align.BOTTOM)  right = true;
	else                                               left = true;

	if (left)
	{
		templist[$-1].size += remain;
	}
	else if (right)
	{
		templist[0].size += remain;
	}
	else if (center)
	{
		long leftmargin  = remain/2;
		long rightmargin = remain - leftmargin;

		templist[0].size += leftmargin;
		templist[$-1].size +=rightmargin;
	}

spread:
	long base = 0;

	foreach (ref tp; templist)
	{
		tp.pos = base;
		base += tp.size;
	}

	assert (remain <= target);
	return cast(int)(target-remain);
}

/** Layout class for horizontal or vertical layout

	for functions and properties refer to class _Layout
 **/
private class LinearLayout : SimpleLayout
{
private:
	long _index1;
	long _index2;

public:
	this (Widget widget, long i1, long i2)
	{
		_index1 = i1;
		_index2 = i2;
		super (widget);
	}

	/*  major coordinate: sum of nomsize of all widgets + padding
		minor coordinate: max of nomsize of all widgets + padding
	*/
	override IPair nomSize() const
	{
		Pair!long s;
		foreach (widget; _widgetList)
		{
			s[_index1] = s[_index1]+widget.nomSize[_index1];
			s[_index2] = max (s[_index2], widget.nomSize[_index2]);
		}
		s[_index1] = min (int.max, s[_index1] + padding[_index1]);
		s[_index2] = min (int.max, s[_index2] + padding[_index2]);

		return IPair (cast(int)s.x, cast(int)s.y);
	}

	/*  major coordinate: sum of minsize of all widgets + padding
		minor coordinate: max of minsize of all widgets + padding
	*/
	override IPair minSize() const
	{
		Pair!long s;
		foreach (widget; _widgetList)
		{
			s[_index1] = s[_index1]+widget.minSize[_index1];
			s[_index2] = max (s[_index2], widget.minSize[_index2]);
		}
		s[_index1] = min (int.max, s[_index1] + padding[_index1]);
		s[_index2] = min (int.max, s[_index2] + padding[_index2]);

		return IPair (cast(int)s.x, cast(int)s.y);

	}

	/*  major coordinate: sum of maxsize of all widgets + padding
		minor coordinate: max of maxsize of all widgets + padding
	*/
	override IPair maxSize() const
	{
		Pair!long s;
		foreach (widget; _widgetList)
		{
			s[_index1] = s[_index1]+widget.maxSize[_index1];
			s[_index2] = max (s[_index2], widget.maxSize[_index2]);
		}
		s[_index1] = min (int.max, s[_index1] + padding[_index1]);
		s[_index2] = min (int.max, s[_index2] + padding[_index2]);

		return IPair (cast(int)s.x, cast(int)s.y);
	}

	override IPair extSize() const
	{
		auto ext = owner.widgetExtSize;
		if (ext == IPair(int.max, int.max)) return ext;

		Pair!long s;
		foreach (widget; _widgetList)
		{
			s[_index1] = s[_index1]+widget.extSize[_index1];
			s[_index2] = max (s[_index2], widget.extSize[_index2]);
		}
		s[_index1] = min (int.max, s[_index1] + padding[_index1]);
		s[_index2] = min (int.max, s[_index2] + padding[_index2]);

		return IPair (max (cast(int)s.x, ext.x),
		              max (cast(int)s.y, ext.y));
	}

	/*  major coordinate: 2*margin + count of spacings between widgets (Spacer Widgets are not counted)
		minor coordinate: 2*margin
	*/
	override IPair padding () const
	{
		IPair p;
		if (_widgetList.length == 0) return p;

		int count;
		int spacers;

		foreach (widget; _widgetList)
		{
			count++;
			if (cast(Spacer)widget !is null) spacers++;
		}

		// y-margin + spacing
		p[_index1] = 2*margin + spacing*(count-1-spacers);
		// x-margin
		p[_index2] = 2*margin;
		return p;
	}

	override void rearrange ()
	{
		if (_widgetList.length == 0) return;

		long minor_min, minor_max, minor_ext;

		_TmpParam[] templist;

		_TmpParam tm, ts;            // additional margin or spacing space
		tm.index = _MARGIN;
		tm.nom = tm.min = tm.max = margin;
		ts.index = _SPACING;
		ts.nom = ts.min = ts.max = spacing;

		templist ~= tm;

		bool left_margin = true;
		foreach (i, widget; _widgetList)
		{
			_TmpParam tp;
			tp.index = i;
			tp.min = widget.minSize[_index1];
			tp.nom = widget.nomSize[_index1];
			tp.max = widget.maxSize[_index1];
			tp.ext = widget.extSize[_index1];

			minor_min = max (minor_min, widget.minSize[_index2]);
			minor_max = max (minor_max, widget.maxSize[_index2]);
			minor_ext = max (minor_ext, widget.extSize[_index2]);

			if (cast(Spacer)widget is null)	// is Widget
			{
				if (left_margin) left_margin = false;
				else             templist ~= ts;      // add Gap
			}
			templist ~= tp; // add Widget
		}

		templist ~= tm;

		minor_min += padding[_index2];
		minor_max += padding[_index2];
		minor_ext += padding[_index2];

		if (minor_ext > int.max)   minor_ext = int.max;
		if (minor_max > minor_ext) minor_max = minor_ext;
		if (minor_min > minor_max) minor_min = minor_max;

		Align align1;
		if (_index1==0) align1 = owner.alignment & Align.HMASK;
		else            align1 = owner.alignment & Align.VMASK;

		// size of this layout:
		int size1 = _arrange_linear (templist, size[_index1], align1);
		int size2 = max (cast(int)minor_min,
		            min (cast(int)minor_ext, size[_index2]));

		Align align2;
		if (_index2==0) align2 = owner.alignment & Align.HMASK;
		else            align2 = owner.alignment & Align.VMASK;

		bool center, bottom;
		if      (align2==Align.LEFT    || align2==Align.TOP)     {/* nothing is true :-( */}
		else if (align2==Align.RIGHT   || align2==Align.BOTTOM)  bottom = true;
		else                                                     center = true;

		// move and size the widgets
		foreach (ref tp; templist)
		{
			if (tp.index < -2)
			{
				auto widget = _widgetList[tp.index];
				bool expand;

				IPair newpos, newsize;

				newsize[_index1] = cast(int)tp.size;
				newsize[_index2] = max (widget.minSize[_index2],
				                   min (widget.extSize[_index2], size2-padding[_index2]));

				newpos[_index1] = cast(int)tp.pos;
				if      (bottom) newpos[_index2] = size2-margin-newsize[_index2];
				else if (center) newpos[_index2] = (size2-newsize[_index2])/2;
				else             newpos[_index2] = margin;

				// this is the final function that resizes and relocates the widgets

				_widgetList[tp.index].move (newpos, newsize);
			}
		}
	}
}

class GridLayout : Layout
{
private:
	Widget [][] _widgetGrid;
	long _cols = 0;
	long _rows = 0;

	// resize the size of the grid array
	void resize (long newcols, long newrows)
	{
		long r = newrows>_rows ? newrows : _rows;
		long c = newcols>_cols ? newcols : _cols;

		_widgetGrid.length = c;
		foreach (ref col; _widgetGrid) col.length = r;

		_rows = r;
		_cols = c;
	}

	void shift (long col, long row)
	{
		auto a = _widgetGrid[col];
		auto l = a.length;

		if (l < 2) return;

		for (auto to=l-1; to > row; to--)
		{
			a[to] = a[to-1];
		}
	}

	// insert a widget into the grid array
	void insert (long col, long row, Widget wgt)
	{
		if (col < _cols && row < _rows)
		{
			if (_widgetGrid[col][row] !is null)
			{
				if (_widgetGrid[col][_rows-1] !is null)
				{
					resize (_cols, _rows+1);
				}
				shift (col, row);
			}
		}
		else
		{
			resize (col+1, row+1);
		}

		_widgetGrid[col][row] = wgt;
	}

public:
	this (Widget widget)
	{
		super (widget);
	}

	override Widget[] widgetList ()
	{
		Widget[] vw;
		vw.reserve (_rows*_cols);
		foreach (col; _widgetGrid) foreach (wgt; col)  if (wgt !is null) vw ~= wgt;
		return vw;
	}

	override bool addWidget (Widget widget, int[] i ...)
	{
		enforce (i.length == 2, "wrong count of variadic parameters (exactly 2 allowed)");

		insert (i[0], i[1], widget);
		widget.setParent (owner);
		return true;
	}

	override bool destroyAllWidgets ()
	{
		bool rc = _widgetGrid.length > 0;
		foreach (list; _widgetGrid)
			foreach (w; list) w.destroy;
		_widgetGrid.length = 0;
		return rc;
	}

	/// returns the nominal size of this widget depending on the nominal size of its subwidgets
	override IPair nomSize() const
	{
		Pair!long s;
		int[] widths;
		int[] heights;

		widths.length = _cols;
		heights.length = _rows;

		foreach (c, column; _widgetGrid)
		{
			foreach (r, widget; column)
			{
				if (widget)
				{
					widths[c]  = max (widths[c],  widget.nomSize.x);
					heights[r] = max (heights[r], widget.nomSize.y);
				}
			}
		}

		foreach (w; widths)  s.x += w;
		foreach (h; heights) s.y += h;

		auto p = padding();
		s.x = min (s.x+p.x, int.max);
		s.y = min (s.y+p.x, int.max);

		return IPair(cast(int)s.x, (cast(int)s.y));
	}

	/// returns the minimum size of this widget depending on the minimum size of its subwidgets
	override IPair minSize() const
	{
		Pair!long  s;
		int[] widths;
		int[] heights;

		widths.length = _cols;
		heights.length = _rows;

		import std.algorithm: max;

		foreach (c, column; _widgetGrid)
		{
			foreach (r, widget; column)
			{
				if (widget)
				{
					widths[c]  = max (widths[c],  widget.minSize.x);
					heights[r] = max (heights[r], widget.minSize.y);
				}
			}
		}

		foreach (w; widths)  s.x += w;
		foreach (h; heights) s.y += h;

		auto p = padding();
		s.x = min (s.x+p.x, int.max);
		s.y = min (s.y+p.x, int.max);

		return IPair(cast(int)s.x, (cast(int)s.y));
	}

	/// returns the maximum size of this widget depending on the maximum size of its subwidgets
	override IPair maxSize() const
	{
		Pair!long  s;
		int[] widths;
		int[] heights;

		widths.length = _cols;
		heights.length = _rows;

		import std.algorithm: max;

		foreach (c, column; _widgetGrid)
		{
			foreach (r, widget; column)
			{
				if (widget)
				{
					widths[c]  = max (widths[c],  widget.maxSize.x);
					heights[r] = max (heights[r], widget.maxSize.y);
				}
			}
		}

		foreach (w; widths)  s.x += w;
		foreach (h; heights) s.y += h;

		auto p = padding();
		s.x = min (s.x+p.x, int.max);
		s.y = min (s.y+p.x, int.max);

		return IPair(cast(int)s.x, (cast(int)s.y));
	}

	override IPair extSize() const
	{
		Pair!long  s;
		int[] widths;
		int[] heights;

		widths.length = _cols;
		heights.length = _rows;

		import std.algorithm: max;

		foreach (c, column; _widgetGrid)
		{
			foreach (r, widget; column)
			{
				if (widget)
				{
					widths[c]  = max (widths[c],  widget.extSize.x);
					heights[r] = max (heights[r], widget.extSize.y);
				}
			}
		}

		foreach (w; widths)  s.x += w;
		foreach (h; heights) s.y += h;

		auto p = padding();
		s.x = min (s.x+p.x, int.max);
		s.y = min (s.y+p.x, int.max);

		return IPair(cast(int)s.x, (cast(int)s.y));
	}

	/// returns the padding of this widget depending on the extended size of its subwidgets
	override IPair padding() const
	{
		return IPair (2*margin + 2*frameWidth + (_cols>1 ? cast(int)(_cols-1)*spacing : 0),
		              2*margin + 2*frameWidth + (_rows>1 ? cast(int)(_rows-1)*spacing : 0));
	}


	static IRect _fit_ (IRect area, Widget widget)
	{
		IRect rect;

		// this allows to stretch the widget when Expand is set
		int maxX = max (widget.maxSize.x, widget.extSize.x);
		int maxY = max (widget.maxSize.y, widget.extSize.y);

		rect.width  = max (widget.minSize.x, min (maxX, area.width));
		rect.height = max (widget.minSize.y, min (maxY, area.height));

		int w = area.width - rect.width;
		int h = area.height - rect.height;
		auto hm = widget.alignment & Align.HMASK;
		auto vm = widget.alignment & Align.VMASK;
		int x, y;
		if      (hm == Align.LEFT)  x = 0;
		else if (hm == Align.RIGHT) x = w;
		else                        x = w/2;
		if      (vm == Align.TOP)    y = 0;
		else if (vm == Align.BOTTOM) y = h;
		else                         y = h/2;

		rect.x0 = area.x0 + x;
		rect.y0 = area.y0 + y;

		return rect;
	}

	/// this is the function, that recalculates the size and position of all widgets of this layout
	/// (that is, all child widgets of the layout's owner widget)
	override void rearrange()
	{
		import std.algorithm: max;

		if (_widgetGrid.length == 0) return;

		enum MIN=0, NOM=1, MAX=2, EXT=3; // indexes for next 2 arrays
		int[4][] widths;
		int[4][] heights;

		widths.length = _cols;
		heights.length = _rows;

		foreach (c, column; _widgetGrid)
		{
			foreach (r, widget; column)
			{
				if (widget)
				{
					widths[c][MIN]  = max (widths[c][MIN],  widget.minSize.x);
					widths[c][NOM]  = max (widths[c][NOM],  widget.nomSize.x);
					widths[c][MAX]  = max (widths[c][MAX],  widget.maxSize.x);
					widths[c][EXT]  = max (widths[c][EXT],  widget.extSize.x);
					heights[r][MIN] = max (heights[r][MIN], widget.minSize.y);
					heights[r][NOM] = max (heights[r][NOM], widget.nomSize.y);
					heights[r][MAX] = max (heights[r][MAX], widget.maxSize.y);
					heights[r][EXT] = max (heights[r][EXT], widget.extSize.y);
				}
			}
		}

		_TmpParam[] xlist;
		_TmpParam xm, xs;
		xm.index = _MARGIN;
		xm.nom = xm.min = xm.max = margin;
		xs.index = _SPACING;
		xs.nom = xs.min = xs.max = spacing;

		xlist ~= xm;				// start templist with a margin
		foreach (i, w; widths)
		{
			_TmpParam xp;
			xp.index = i;
			xp.nom = w[NOM];
			xp.min = w[MIN];
			xp.max = w[MAX];
			xp.ext = w[EXT];

			xlist ~= xp;		// add widget
			xlist ~= xs;		// add spacing
		}
		xlist[$-1] = xm;		// replace last spacing with a margin

		_TmpParam[] ylist;
		_TmpParam ym, ys;
		ym.index = _MARGIN;
		ym.nom = ym.min = ym.max = margin + frameWidth;
		ys.index = _SPACING;
		ys.nom = ys.min = ys.max = spacing;

		ylist ~= ym;			// start templist with a margin
		foreach (i, h; heights)
		{
			_TmpParam yp;
			yp.index = i;
			yp.nom = h[NOM];
			yp.min = h[MIN];
			yp.max = h[MAX];
			yp.ext = h[EXT];

			ylist ~= yp;			// add widget
			ylist ~= ys;			// add spacing
		}
		ylist[$-1] = ym;			// replace last spacing with a margin

		_arrange_linear (xlist, size.x, owner.alignment & Align.HMASK);
		_arrange_linear (ylist, size.y, owner.alignment & Align.VMASK);

		// move and size the widgets
/+		double coordx = origin.x;
+/
		double coordx = 0.0;

		foreach (x; xlist)
		{
/+			double coordy = origin.y;
+/
			double coordy = 0.0;

			foreach (y; ylist)
			{
				if ((x.index < -2) && (y.index < -2))
				{
					auto wgt = _widgetGrid[x.index][y.index];
					if (wgt)
					{
						IPair areap, areas;

						areap.x = cast(int)coordx;
						areap.y = cast(int)coordy;

						areas.x = cast(int)x.size;
						areas.y = cast(int)y.size;

						IRect area = IRect (areap, areas.x, areas.y);
						auto loc = _fit_ (area, wgt);

						// this is the final function that resizes and relocates the widgets
						wgt.move (loc.base, loc.size);
					}
				}
				coordy += y.size;
			}
			coordx += x.size;
		}
	}
}

/** A LayoutWidget is a Widget, that can't itself be seen. It does not handle any
	events and it does not paint anything apart from the gaps between
	subwidgets. As a layout feature it is the base class
	for Spacer, HorizontalWidget and VerticalWidget
**/


class LayoutWidget : Widget
{

	/** constructor
	**/
	this (Widget parent)
	{
		super (parent);
	}

	/** overridden method; does not do anything except distributing
	    RedrawEvents to any subwidgets
	**/
	override void handleEvent (Event event)
	{
		switch (event.type)
		{
			case EventType.REDRAW:
				// distribute this event to all subwidgets
				foreach (w; layout.widgetList)
				{
					w.handleEvent (event);
				}
				break;
			default:
		}
	}
}

/** Spacer widgets are widgets that have no content (that is, they are invisible, since they don't draw anything onto the screen)
	and don't receive any events. They only have a size	and are used as a flexible space between "real" widgets.
	Since they are a layout feature, the are found in the layout module
	Spacers are the only widgets, that have a minimum size of (0, 0);
**/
class Spacer : LayoutWidget
{
	/** Constructor
	**/
	this (Widget parent)
	{
		this (IPair(0,0), parent);
	}

	/// ditto
	this (IPair min, Widget parent)
	{
		super (parent);
		_widgetMinSize =
		_widgetNomSize =
		_widgetMaxSize = IPair (0, 0);
		_widgetExtSize = IPair (int.max, int.max);
	}

	/** overridden; always returns null
	**/
	override Widget findWidget (IPair wpointer)
	{
		return null;
	}

// when spacers should be visible for debugging:
//	override void paint (CairoContext ctx)
//	{
//		import cairo.context;
//		import wcw.color;
//		ctx.set_source_color (Color.Red);
//		ctx.paint ();
//	}
}

/** Horizontal Layout

	This is a conveniance class; it only initialises its base class LinearLayout to using
	x resp. width as the major coordinate and y resp. height as the minor coordinate for calculating the layout.
**/
class HorizontalLayout : LinearLayout
{
	/** Constructor

		Params:
		widget = (Virtual-) Widget, that this Layout class will be assigned to
	**/
	this(Widget widget)
	{
		super (widget, 0, 1);
	}
}

/** Vertical Layout

	This is a conveniance class; it only initialises its base class LinearLayout to
	y resp. height as the major coordinate and y resp. width as the minor coordinate for calculating the layout.
**/
class VerticalLayout : LinearLayout
{
	/** Constructor

		Params:
		widget = (Virtual-) Widget, that this Layout class will be assigned to
	**/
	this(Widget widget)
	{
		super (widget, 1, 0);
	}
}

/** Arrange widgets in a horizontal layout

	This class is just for conveniance; The same could be achieved with the following code:

	Code:
	auto widget = new Widget(...);
	new2 HorizontalLayout (widget);
**/
class HorizontalWidget : LayoutWidget
{
	/** Constructor

		Params:
		parent = (Virtual-) Widget, that this Layout class will be assigned to
	**/
	this (Widget parent)
	{
		super (parent);
		new HorizontalLayout (this);
	}
}

/** Arrange widgets in a vertical layout

	This class is just for conveniance; The same could be achieved with the following code:

	Code:
	auto widget = new Widget(...);
	new2 VerticalLayout (widget);
**/
class VerticalWidget : LayoutWidget
{
	/** Constructor

		Params:
		parent = (Virtual-) Widget, that this Layout class will be assigned to
	**/
	this (Widget parent)
	{
		super (parent);
		new VerticalLayout (this);
	}
}

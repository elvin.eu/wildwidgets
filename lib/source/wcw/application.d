module wcw.application;

import std.exception;
import std.path;
import std.algorithm;

import wcw.auxiliary;
import wcw.enums;
import wcw.window;
import wcw.design;
import wcw.config;
import wcw.menu;
import wcw.clipboard;

version (X11) import wcw.x11.application;

package Application globalApplication;

Application App(string [] args_ = null)
{
	if (!globalApplication)
	{
		new Application(args_);
	}
	return globalApplication;
}

class Application
{
private:
	Application_Backend _backend;

package:
	string   _title;
	string[] _parameters;

	bool _quit;

	IPair _resolution;
	Design _design;
	Config _config;
	Clipboard _clipboard;

	Window[] _windowList;
	Window[] _modalWindowList;

	MenuPanel _oldMenu;		// the old - or current - Menu
	MenuPanel _newMenu;		// if another Menu shall be opened, this is it

	/++ the first function call in each wcw program should be a call to
		new Application (args), where args is the args parameter of main (string[] args)

		The constructor saves the main() parameters and connects to the X server
	++/
	this (string [] arguments = null)
	{
		enforce (globalApplication is null, "only one global Application object is allowed");

		version (X11) _backend = new Application_X11 (this);

		if (arguments.length >= 1) _title = baseName (arguments[0], ".exe");
		_parameters = arguments;

		// after this line it is save to call functions that need a running Application
		globalApplication = cast(Application)this;

		// those might need a running Backend e.g. for the allocation of colors
		_config = new Config ("wcw");
		_design = new Design (_config);
		_clipboard = new Clipboard ();
	}

	~this()
	{
		debug (shutdown) whereami ("~this Application");

		while (_windowList.length > 0)
		{
			destroy (_windowList[0]);
		}

		destroy (_clipboard); _clipboard = null;
		destroy (_design);    _design    = null;
		destroy (_config);    _config    = null;
		destroy (_backend);   _backend   = null;
	}

public:
	Application_Backend backend () {return _backend;}

	string title () const {return _title;}
	const (string[]) parameters () const {return _parameters;}

	IPair resolution () const {return _resolution;}
	inout(Design) design () inout  {return _design;}
	inout(Config) config () inout  {return _config;}
	inout(Clipboard) clipboard () inout  {return _clipboard;}

	/// quit the application
	final void quit ()
	{
		// quitting is done by run()
		_quit = true;
	}

	/// add a window to the internal window list
	void addWindow (Window window)
	{
		_windowList ~= window;
	}

	/// add a window to the internal window list
	void removeWindow (Window window)
	{
		_windowList = _windowList.remove!(a => a==window);
	}


	void pushModalWindow (Window m)
	{
		_modalWindowList.push (m);
	}

	void dropModalWindow (Window m)
	{
		assert (_modalWindowList.tos == m);
		_modalWindowList.pop();
	}

	inout (MenuPanel) menu () inout {return _oldMenu;}

	/** enter the main event loop. read in events from the X server,
		convert them to wcw events and send them to the appropriate window.
		Exits, when no more events are pending
	**/
	int run (Window w=null)
	{
		return backend.run (w);
	}

	void beep ()
	{
		backend.beep();
	}

	/// call the menu
	void showMenu (MenuPanel menu)
	{
		// The menu is called indirectly by setting the _newMenu variable.
		// run() will then show the menu by calling app_showMenu()
		_newMenu = menu;
	}



package:
	void app_showMenu ()
	{
		if (_oldMenu == _newMenu) return;

		MenuPanel [] tree_old, tree_new, tree_hide, tree_show;

		// previous menu tree
		for (auto m = _oldMenu; m; m = m.parentMenuPanel) tree_old ~= m;
		// new menu tree
		for (auto m = _newMenu; m; m = m.parentMenuPanel) tree_new ~= m;
		// menus to hide
		foreach (t; tree_old) if (!canFind (tree_new, t)) tree_hide ~= t;
		// menus to show
		foreach (t; tree_new)   if (!canFind (tree_old, t)) tree_show ~= t;

		foreach (t; tree_hide) t.hide;
		foreach (t; tree_show) t.show;
/*
		if (_newMenu !is null && _newMenu.isMenuRoot) _oldMenu = null;
		else                                          _oldMenu = _newMenu;
*/

		_oldMenu = _newMenu;
	}
}

interface Application_Backend
{
	DialogResult run (Window);
	void beep  ();
}

module wcw.event;

import wcw.auxiliary;
import wcw.enums;

class Event
{
	EventType type;
	bool broadcast;
//	bool prioritise;

	this ()            {type = EventType.INVALID;}
	this (EventType t) {type = t;}

	override const istring toString ()  {return format ("Event: %s", type);}
}

class MouseInEvent : Event
{
	this () {super (EventType.MOUSEIN);}
}

class MouseOutEvent : Event
{
	this () {super (EventType.MOUSEOUT);}
}

class CloseEvent : Event
{
	this()
	{
		debug (shutdown) whereami ("CloseEvent");
		super (EventType.CLOSE);
	}
}

/** This event is generated every 100 milliseconds. There is no guarantee, that this event is
	generated. The clock stops, when the application is busy while dispatching an event (for
	example a drawing event takes longer to be processed). After the event has been dispatched,
	the clock continues to tick.
**/

class ClockEvent : Event
{
	this() {super (EventType.CLOCK);}
}

class FocusInEvent : Event
{
	this() {super (EventType.FOCUSIN);}
}

class FocusOutEvent : Event
{
	this() {super (EventType.FOCUSOUT);}
}

class MouseEvent : Event
{
	IPair pointer;
	MouseButton button;
	ModifierKey modifier;
	ModifierKey buttonModifier () const {return modifier&ModifierKey.BUTTONMASK;}
	ModifierKey keyModifier () const {return modifier&ModifierKey.KEYMASK;}

	this (EventType t)
	{
		super (t);
	}


}

class MouseMoveEvent : MouseEvent
{
	this (IPair pos)
	{
		super (EventType.MOUSEMOVE);
		pointer = pos;
	}
}

class ButtonPressEvent : MouseEvent
{
	this (IPair pos)
	{
		super (EventType.BUTTONPRESSED);
		pointer = pos;
	}
}

class ButtonReleaseEvent : MouseEvent
{
	this (IPair pos)
	{
		super (EventType.BUTTONRELEASED);
		pointer = pos;
	}
}

class KeyEvent : Event
{
	uint        keycode;	// the number of the key on the keyboard
	KeySymbol   symbol;		// a key symbol like Backspace, F11, Home...
	ModifierKey modifier;	// Shift, Control, Alt, AltGr... or'd together
	ModifierKey buttonModifier () const {return modifier&ModifierKey.BUTTONMASK;}
	ModifierKey keyModifier () const {return modifier&ModifierKey.KEYMASK;}
	string      text;		// the generated character (usually) or istring (seldom)

	this (EventType t)
	{
		super (t);

		keycode  = 0;
		symbol   = KeySymbol.NONE;
		modifier = ModifierKey.NONE;
		text     = "";
	}
}

class KeyPressEvent : KeyEvent
{
	this ()
	{
		super (EventType.KEYPRESSED);
	}
}

class KeyReleaseEvent : KeyEvent
{
	this () {super (EventType.KEYRELEASED);}
}

// this event is only handled by Windows
class TransformEvent : Event
{
	IPair position;
	IPair size;

	this ()
	{
		super (EventType.TRANSFORM);
	}
}

class RedrawEvent : Event
{
	this () {super (EventType.REDRAW);}
}

class ShowEvent : Event
{
	this () {super (EventType.SHOW);}
}

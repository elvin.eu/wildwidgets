module wcw.image;

import std.stdio;
import std.exception;

import cairo;

import wcw.auxiliary;
import wcw.cairo;

class Image
{
private:
	CairoImageSurface _surface;

public:
	CairoImageSurface surface() {return _surface;}

	// currently only png data supported
	this (const ubyte[] data)
	{
		_surface = CairoImageSurface.create_from_png_data (data);
	}

	this (CairoImageSurface surface)
	{
		_surface = surface;
	}

	Image scaledTo (IPair size)
	{
		double w = _surface.get_width();
		double h = _surface.get_height;


		CairoMatrix matrix = CairoMatrix (w/size.width, 0.0, 0.0, h/size.height, 0.0, 0.0);

		auto pattern = new CairoSurfacePattern (surface);
		pattern.set_matrix (matrix);
		pattern.set_filter (CairoFilter.BEST);

		auto image = new CairoImageSurface (CairoFormat.ARGB32, size.width, size.height);
		auto context = new CairoContext (image);

		context.set_source (pattern);
		context.paint ();

		return new Image (image);
	}
}
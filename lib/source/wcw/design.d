module wcw.design;

import wcw.color;
import wcw.auxiliary;
import wcw.config;
import wcw.font;

//import std.stdio;
//import std.path;

/// collection of design colors, consisting of:
/// windowColor: the window's background color
/// editColor: the background color of a text widget
/// textColor: foreground color of text
/// frameHiColor, frameLoColor: color of frames
/// buttonHiColor,buttonLoColor: color of the inner area of buttons
/// boxHiColor, boxLoColor: the inner area of boxes, e.g. toolboxes
struct Palette
{
	Color passive,				// inactive
		  active,				// normal state
		  hilight,				// e.g. mouse over button
		  select;				// e.g. text is selected
	double gradient = 1.0;		// optional. normal widgets, text and edit fields don't use it

	this (Color p, Color a, Color h, Color s, double g=1.0)
	{
		passive = p;
		active  = a;
		hilight = h;
		select  = s;
		gradient= g;
	}

	this (Color c)
	{
		passive = active = hilight = select = c;
		gradient = 1.0;
	}
}

/// all relevant design parameters
class Design
{
	// text font, default is "Sans:Regular:10"
	Font font;

	// edge diameters and frame sizes:
	// button e.g. for push buttons,
	// box e.g. for toolboxes,
	// widget for other framed widgets
	uint widgetMargin, widgetSpacing, widgetFrame, widgetEdge;
	uint windowMargin, windowSpacing;
	uint buttonFrame, buttonEdge;
	uint toolboxFrame, toolboxEdge;
	uint textMargin, textFrame;

	// gradient types of buttons, boxes and frames:
	uint frameGradient, buttonGradient, boxGradient;

	Palette widgetPalette,
			 framePalette,
			 buttonPalette,
			 textPalette,
			 toolboxPalette,
			 editPalette;

	Color cursorColor;

	this (Config config)
	{
		font       = config.value!Font("font", "Sans:Regular:10");

		windowMargin  = config.value!int("windowMargin",  "5");
		windowSpacing = config.value!int("windowSpacing", "5");

		widgetMargin  = config.value!int("widgetMargin",  0);
		widgetSpacing = config.value!int("widgetSpacing", 5);
		widgetFrame   = config.value!int("widgetFrame",   1);
		widgetEdge    = config.value!int("widgetEdge",    0);

		textMargin    = config.value!int("textMargin", 3);
		textFrame     = config.value!int("textFrame",  1);

		buttonFrame   = config.value!int("buttonFrame",  1);
		buttonEdge    = config.value!int("buttonEdge",   0);

		toolboxFrame  = config.value!int("toolboxFrame",     1);
		toolboxEdge   = config.value!int("toolboxEdge",      0);

		widgetPalette.passive  = config.value!Color ("widgetPalette.passive",  "aaaaaa");	// widget is inactive
		widgetPalette.active   = config.value!Color ("widgetPalette.active",   "cccccc");	// widget is active
		widgetPalette.hilight  = config.value!Color ("widgetPalette.hilight",  "cccccc");
		widgetPalette.select   = config.value!Color ("widgetPalette.select",   "cccccc");

		framePalette.passive   = config.value!Color ("framePalette.passive",   "888888");
		framePalette.active    = config.value!Color ("framePalette.active",    "222222");
		framePalette.hilight   = config.value!Color ("framePalette.hilight",   "222222");
		framePalette.select    = config.value!Color ("framePalette.select",    "0088ee");	// widget has focus
		framePalette.gradient  = config.value!double("framePalette.gradient",   1.00);

		buttonPalette.passive  = config.value!Color ("buttonPalette.passive",  "aaaaaa");	// button is passive
		buttonPalette.active   = config.value!Color ("buttonPalette.active",   "bbbbbb");	// button is active
		buttonPalette.hilight  = config.value!Color ("buttonPalette.hilight",  "aabbcc");	// mouse is over button
		buttonPalette.select   = config.value!Color ("buttonPalette.select",   "7799dd");	// button is pressed
		buttonPalette.gradient = config.value!double("buttonPalette.gradient",  1.00);

		toolboxPalette.passive = config.value!Color ("boxPalette.passive",     "aaaaaa");	// widget is passive
		toolboxPalette.active  = config.value!Color ("boxPalette.active",      "bbbbbb");	// widget is active
		toolboxPalette.hilight = config.value!Color ("boxPalette.hilight",     "aabbcc");
		toolboxPalette.select  = config.value!Color ("boxPalette.select",      "7799dd");
		toolboxPalette.gradient= config.value!double("boxPalette.gradient",     1.00);

		editPalette.passive    = config.value!Color ("editPalette.passive",    "bbbbbb");	// widget is passive
		editPalette.active     = config.value!Color ("editPalette.active",     "ffffff");	// widget is active
		editPalette.hilight    = config.value!Color ("editPalette.hilight",    "ffffff");
		editPalette.select     = config.value!Color ("editPalette.select",     "4080c0");	// background of selected text

		textPalette.passive    = config.value!Color ("textPalette.passive",    "666666");	// widget is passive
		textPalette.active     = config.value!Color ("textPalette.active",     "000000");	// widget is active
		textPalette.hilight    = config.value!Color ("textPalette.hilight",    "aaaa00");
		textPalette.select     = config.value!Color ("textPalette.select",     "e0e0e0");	// text is selected

		cursorColor            = config.value!Color ("cursorColor",            "800000");
	};

	~this ()
	{
		debug (shutdown) whereami ("~this Design");
	}
}

module wcw.config;

import wcw.auxiliary;

import std.stdio;
import std.path;
import std.regex;
import std.string;
import std.algorithm;
import std.conv;

/* Config files:

key = value
key : value

key:   any combination of characters with the exception of whitespaces, ':' and '='
value: an integer, float, color or string

integer: 123
float:   123e-4
string:  characters enclosed in double colons (")
		 replace " with \x22
		 replace any control character (00 to 31) \x..
*/

class Config
{
	File _file;
	string _path;
	string[string]_entries;

	this (string name)
	{
		_path = expandTilde ("~/.config/"~name~".conf");
		try
		{
			auto file = File (_path, "r");
			auto lines = file.byLine();

			auto sn = regex (r"^\s*\[\s*(\w+)\s*]");    // matches sections
			auto ct = regex (r"^\s*;(.*)");             // matches comments
			auto kv = regex (r"^\s*(\w+)\s*=\s*(.+)");   // matches key/value pairs

			string section;

			foreach (char[] line; lines)
			{
				// check, if it is a section entry
				auto hit = matchFirst (line, sn);
				if (hit.length)
				{
					section = hit[1].idup;
					continue;
				}

				// check, if it is a comment
				hit = matchFirst (line, ct);
				if (hit.length)
				{
					continue;
				}

				hit = matchFirst (line, kv);
				if (hit.length)
				{
					char[] key   = strip(hit[1]);       // remove leading and trailing whitespaces
					char[] value = strip(hit[2]);       // if you want them, enclose values in double quotes (")

					// add section.key/value pair to associative array
					// always add a dot, even when section is not specified
					key = section ~ '.' ~ key;
					_entries[key.idup] = value.idup;
				}
			}
		}
		catch (Throwable)
		{
			stderr.writefln("Can't read the configuration from %s", _path);
		}
	}

	~this ()
	{
		debug (shutdown) whereami ("~this Config");
	}

	/// return value from config database
	T value (T)(string key, T def)
	{
		T val;

		// keys without section internally start with '.'
		if (indexOf (key, '.') == -1) key = "." ~ key;

		if (key in _entries)
		{
			try
			{
				return to!T(_entries[key]);
			}
			catch (Throwable)
			{
				// do nothing; instead return def
				whereami();
			}
		}

		// write value back to database; this could repair a possibly wrong entry
		store (key, def);
		return def;
	}

	/// ditto
	T value (T)(string key, istring def)
	{
		T val;
		bool done = false;

		// keys without section internally start with '.'
		if (indexOf (key, '.') == -1) key = "." ~ key;

		if (key in _entries)
		{

			try
			{
				val = to!T(_entries[key]);
				done=true;
			}
			catch (Throwable)
			{
			}
		}

		if (!done)
		{
			try
			{
				val = to!(T)(def);     // this crashes, when T is a base type and def makes no sense
			}
			catch (Throwable)
			{
			}
		}

		// write value back to database; this could repair a possibly wrong entry
		store (key, val);
		return val;
	}


	/// clear database
	void clear ()
	{
		_entries = _entries.init;   // wipe out all settings
	}

	/// write a key/value into database
	void store(T)(string key, T value)
	{
		// keys without section internally start with '.'
		if (indexOf (key, '.') == -1) key = "." ~ key;

		_entries[key] = to!string(value);
	}

	/// save configuration to file
	void save ()
	{
		try
		{
			auto file = File (_path, "w");
			auto sn = regex (r"^(\w*)\.(\w+)$");    // matches key.section
			string section;

			foreach (key; _entries.keys.sort)
			{
				string subkey = key;

				auto hit = matchFirst(key, sn);
				if (hit.length)
				{
					if (hit[1] != section)
					{
						file.writeln ("\n[", hit[1], "]");
						section = hit[1];
					}

					file.writeln (hit[2], " = ", _entries[key]);
				}
			}
		}

		catch (Throwable)
		{
			stderr.writefln("Can't write the configuration to %s", _path);
		}
	}

}

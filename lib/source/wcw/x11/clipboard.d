module wcw.x11.clipboard;


version (X11)
{
import core.stdc.config;
import std.algorithm;
import std.concurrency;
import std.conv;
import std.datetime;
import std.exception;
import std.string;
import std.typecons;

import wcw.application;
import wcw.clipboard;
import wcw.auxiliary;
import wcw.x11.application;
import wcw.enums;

private Application_X11 x11_backend (Application app) {return cast(Application_X11)app.backend;}

static import X11;

private long bytesForFormat (int format)
{
	switch (format)
	{
	case 8: return  byte.sizeof;		// obvious
	case 16: return short.sizeof;		// also obvious
	case 32: return c_long.sizeof;		// not so obvious: 32 means 8 bytes for 64 bit software
	default: assert (0);
	}
}

private void writeProperty (X11.Display* disp,
	ref X11.XSelectionRequestEvent sr,
	X11.Atom type,
	int format,
	ubyte* buffer,
	int length)
{
	if (sr.property != X11.None)
	{
		X11.XChangeProperty(disp,
			sr.requestor,
			sr.property,
			type, format,
			X11.PropModeReplace,
			buffer, length);
	}

	X11.XSelectionEvent sevent;
	sevent.type      = X11.SelectionNotify;
	sevent.requestor = sr.requestor;
	sevent.selection = sr.selection;
	sevent.target    = sr.target;
	sevent.property  = sr.property;
	sevent.time      = sr.time;

	X11.XSendEvent (disp, sr.requestor, true, X11.NoEventMask, cast (X11.XEvent *) &sevent);
}

private struct ReadContext
{
	X11.Atom property;
	bool incr;
}

private class WriteContext
{
	X11.Window window;
	X11.Atom property;
	size_t from;
	size_t to;
	bool incr;
	SysTime time;
}

private struct WriteContextArray
{
	WriteContext[] array;

	WriteContext get (X11.Window window, bool clear_if_exists)
	{
		WriteContext context;

		foreach (element; array)
		{
			if (element.window == window)
			{
				context = element;
				break;
			}
		}

		if (context is null)
		{
			context = new WriteContext;
			context.window = window;
			context.time = Clock.currTime;
			array ~= context;
			clear_if_exists = true;
		}

		if (clear_if_exists)
		{
			context.property = 0;
			context.from     = 0;
			context.to       = 0;
			context.time = Clock.currTime;
			context.incr     = false;
		}

		return context;
	}

	// return context; if context does not exist, create a new one
	WriteContext find (X11.Window window) {return get (window, false);}

	// create and return a new context; if context existsalready, clear it
	WriteContext create (X11.Window window) {return get (window, true);}

	void cleanup (X11.Window w)
	{
		auto past = Clock.currTime - 5.seconds;
		array = remove!(a => a.time < past) (array);		// remove items that are too old
		array = remove!(a => a.window == w) (array);		// remove items for window w
	}
}

private struct Environment
{
	string name;				// name of class object; only for bug hunting
	int verbose;

	X11.Display* display;

	X11.Window read_window;		// one invisible window for reading, one for writing
	ubyte[]    read_buffer;		// the data buffer
	int        read_format;		// format of property: 8, 16 or 32

	X11.Window write_window;	// one invisible window for reading, one for writing
	ubyte[]    write_buffer;	// the data buffer
	int        write_format;	// format of property: 8, 16 or 32
	X11.Atom   write_type;		// targtet type of the  property (UTF8_STRING, TARGETS, ...)

	X11.Atom property, primary, clipboard, incr, sel, utf8, targets;

	ReadContext read_context;
	WriteContextArray write_contexts;
	int maxlen;
}

struct Verbosity
{
	bool shutdown;
	bool events;
	bool selection;
}

shared static  Verbosity vy;

// XSelectionRequestEvent
// .selection == selection type (PRIMARY or CLIPBOARD)
// .target    == target format  (UTF8_STRING, STRING, TIMESTAMP, TARGETS, ...)
// .property  == any property of receiving or sending window

class Clipboard_X11 : Clipboard_Backend
{
	Environment *_env;
	X11.Display* _display;
	bool _internaldisplay;
	Tid closeTid;

	this ()
	{
		_display = App.x11_backend.x11_Display ();

		spawn (&_run);
		_env = cast (Environment*) receiveOnly!(shared (Environment)*);
		spawn (&_close, cast (shared) _env);

		debug (events)    vy.events = true;
		debug (selection) vy.selection = true;
		debug (shutdown)  vy.shutdown = true;
	}

	~this()
	{
		if (vy.shutdown) whereami ("~this Clipboard");
// this seems to be nonsense, since _close continues running after receiveOnly, when the program
// is terminated. I wonder, from where I got that idea in the first place... :-(
//		closeTid.send (true);
		if (_internaldisplay) X11.XCloseDisplay (_display);
	}

	// since run() doesn't end when the owner ends, we need a dedicated task that tell run() to exit

	private static void _close (shared Environment* shared_env)
	{
		auto env = cast (Environment*) shared_env;

		try
		{
			// nobody ever sends us anything - but when the application terminates, this call
			// throws an exception and we can send a termination message to _run
			receiveOnly!bool;
		}
		catch (Throwable)
		{
			// write a client message to write_window (could also be read_window)
			// this will end the "run" task
			X11.XClientMessageEvent event;
			event.type         = X11.ClientMessage;
			event.window       = env.write_window;
			event.message_type = 0;
			event.format       = 32;
			event.data.l       = [0, 0, 0, 0, 0]; // data is a union, need to initialize it manually
			X11.XSendEvent (env.display, env.write_window, 0, 0, cast(X11.XEvent*)&event);

			// immensely important to flush, otherwiese the event gets delayed and will never be
			// sent because the main Application ends as well and closes x11Display
			X11.XFlush (env.display);
		}

		if (vy.shutdown) whereami (env.name ~ "._close:    ends");
	}

	private static void _run ()
	{
		Environment env;

		env.name = "Clipboard";
		auto name = env.name ~ "._run:      ";

		env.display = X11.XOpenDisplay (null);
		enforce (env.display, name ~ "cannot connect to X server");

		env.property  = X11.XInternAtom (env.display, "WCW_CLIPBOARD", false);
		env.primary   = X11.XInternAtom (env.display, "PRIMARY", false);
		env.clipboard = X11.XInternAtom (env.display, "CLIPBOARD", false);
		env.incr      = X11.XInternAtom (env.display, "INCR", false);
		env.utf8      = X11.XInternAtom (env.display, "UTF8_STRING", false);
		env.targets   = X11.XInternAtom (env.display, "TARGETS", false);

		env.write_window = X11.XCreateWindow (env.display,
			X11.XDefaultRootWindow (env.display),
			0, 0, 1, 1, 0, 0, X11.InputOnly, null, 0, null);

		if (vy.events) whereami (format(name ~ "write window: %x", env.write_window));

		X11.XSelectInput (env.display, env.write_window, X11.PropertyChangeMask);

		env.read_window = X11.XCreateWindow (env.display,
			X11.XDefaultRootWindow (env.display),
			0, 0, 1, 1, 0, 0, X11.InputOnly, null, 0, null);

		if (vy.events) whereami (format(name ~ "read window: %x", env.read_window));

		X11.XSelectInput (env.display, env.read_window, X11.PropertyChangeMask);

		env.maxlen = cast(int) X11.XMaxRequestSize(env.display)*4;
		if (vy.selection) env.maxlen = 4*4;

		ownerTid.send (cast (shared Environment*) (&env));

		for (;;)
		{
			X11.XEvent xevent;
			X11.XNextEvent (env.display, &xevent);

			if (vy.events) whereami (format (name ~ "received event %s: %s for window %x",
				xevent.type, eventName(xevent.type), xevent.xany.window));

			if (xevent.type == X11.ClientMessage)
			{
				break;
			}
			else if (xevent.xany.window == env.read_window)
			{
				_read (xevent, &env);
			}
			else if (xevent.xany.window == env.write_window)
			{
				_write (xevent, &env);
			}
			else
			{
				_incr (xevent, &env);
			}
		} // for(;;)

		X11.XCloseDisplay (env.display);
		if (vy.shutdown) whereami (name ~ "ends");
	}

	private static void _read (X11.XEvent xevent, Environment* env)
	{
		auto name = env.name ~ "._read:     ";

		if (xevent.type == X11.SelectionNotify)
		{
			auto sn = xevent.xselection;
			if (sn.property == X11.None)
			{
				if (vy.selection) whereami (name ~ "ignoring invalid request for property  \"None\"");
				ownerTid().send (false);	// invalid request
			}
			else
			{
				// XGetWindowProperty's return values:

				X11.Atom type;		// type returned: INCR, UTF8_STRING...
				int format;			// item size: 8, 16 or 32
				ulong items;        // number of items in buffer
				ulong remain;       // bytes remaining
				ubyte* buffer;      // returned properties as array of

				X11.XGetWindowProperty(env.display, env.read_window,
					sn.property,
					0,						// offset
					0,						// length
					false,					//	delete
					X11.AnyPropertyType,
					&type, &format, &items, &remain, &buffer);

				if (vy.selection)
				{
					auto propname = X11.XGetAtomName (env.display, sn.property);
					whereami (text(name ~ "got size for property ", sn.property, ": ", propname));
					X11.XFree (propname);
				}

				env.read_buffer.length = 0; 		// empty buffer before receiving a new property

				if (type == env.incr)
				{
					// acc. to ICCCM V2.0: a lower bound on the number of bytes of data
					c_long* size;

					X11.XGetWindowProperty(env.display, env.read_window,
						sn.property,
						0,						// offset in 32 bit words
						1,						// length in 32 bit words
						false,					// delete flag
						X11.AnyPropertyType,	// required type
						&type, &format,	&items,	&remain, cast(ubyte**)&size);

					if (vy.selection) whereami (text(name ~ "switching to INCR protocol for ", *size, " bytes"));

					if (*size > 0) env.read_buffer.reserve (*size);
					env.read_buffer.length = 0;

					X11.XDeleteProperty(env.display, env.read_window, sn.property);
					X11.XFree (size);

					env.read_context.property = sn.property;
					env.read_context.incr = true;
				}
				else
				{
					X11.XGetWindowProperty (env.display, env.read_window,
						sn.property,
						0,		// offset in 32 bit multiples
						remain,	// length in 32 bit multiples
						false,
						X11.AnyPropertyType,
						&type, &format, &items,	&remain, &buffer);

					if (vy.selection) whereami (text(name ~ "received ", items, " items of format ", format));

					env.read_buffer ~= buffer[0..items*bytesForFormat(format)];
					env.read_format = format;
					X11.XFree (buffer);

					X11.XDeleteProperty(env.display, env.read_window, sn.property);

					ownerTid().send (true);
				}
			}
		} // SelectionNotify
		else if (xevent.type == X11.PropertyNotify)
		{
			if (xevent.xproperty.state == X11.PropertyNewValue && env.read_context.incr == true)
			{
				X11.Atom type; int format; ulong items, remain; ubyte* buffer;

				X11.XGetWindowProperty(env.display, env.read_window,
					env.read_context.property,
					0,						// offset
					0,						// length
					false,					//	delete
					X11.AnyPropertyType,
					&type, &format, &items, &remain, &buffer);

				if (vy.selection) whereami (text(name ~ "got size for property ", env.read_context.property));

/* maybe check for correct type from ConvertSelection?
*/
				if (type != env.incr)
				{
					X11.XGetWindowProperty(env.display, env.read_window,
						env.read_context.property,
						0,
						remain,
						false,
						X11.AnyPropertyType,
						&type, &format,	&items,	&remain, &buffer);

					if (vy.selection) whereami (text(name ~ "received ", items, " items of format ", format));

					env.read_format = format;
					env.read_buffer ~= (cast(char*)buffer)[0..items*bytesForFormat(format)];
					X11.XFree (buffer);

					X11.XDeleteProperty(env.display, env.read_window, env.read_context.property);
					if (vy.selection) whereami (text(name ~ "deleted property ", env.read_context.property));

					if (items == 0)
					{
						if (vy.selection) whereami (name ~ "INCR protocol finished");

						env.read_context.incr = false;
						ownerTid().send (true);
					}
				}
			}
			else
			{
				if (vy.selection) whereami (name ~ "ignored PropertyNotify");
			}
		} // PropertyNotify
	}

	private static void _write (X11.XEvent xevent, Environment* env)
	{
		auto name = env.name ~ "._write:    ";

		if (xevent.type == X11.SelectionRequest)
		{
			auto sr = xevent.xselectionrequest;

			if (vy.selection)
			{
				auto propertyname = X11.XGetAtomName (env.display, sr.property);
				auto targetname   = X11.XGetAtomName (env.display, sr.target);

				whereami (format (name ~ "property is %s: %s, target is %s: %s, requestor is %x",
					sr.property, propertyname.fromStringz, sr.target, targetname.fromStringz, sr.requestor));

				X11.XFree (propertyname);
				X11.XFree (targetname);
			}

			if (sr.property == X11.None)
			{
				if (vy.shutdown) whereami (env.name ~ ": ignoring property \"None\"");
			}
			else
			{
				if (sr.target == env.write_type)
				{
					if (vy.selection) whereami (name ~ "property target matches buffer type");

					auto buflen = cast (int) env.write_buffer.length;

					// do nothin, if length of buffer does not match formet
					if (env.write_buffer.length % bytesForFormat (env.write_format) != 0)
					{
						if (vy.shutdown) whereami (text(env.name, ": byte length is ", buflen, " but format is ", env.write_format));
					}

					int length = buflen / bytesForFormat (env.write_format);

					if (buflen <= env.maxlen)
					{
						if (vy.selection) whereami (text(name ~ ": sending ", length, " items of format at once", env.write_format));
						writeProperty (env.display, sr, env.write_type, env.write_format, env.write_buffer.ptr, length);
					}

					else // switch to INCR protocol
					{

						X11.XSelectInput (env.display, sr.requestor, X11.PropertyChangeMask);

						auto context = env.write_contexts.create (sr.requestor);
						context.property = sr.property;
						context.incr = true;

						if (vy.selection) whereami (text(name ~ "sending type INCR and length ", buflen));

						writeProperty (env.display, sr, env.incr, 32, cast (ubyte*)&buflen, 1);
					}
				}
				else if (sr.target == env.targets)
				{
					X11.Atom[] targets = [env.targets];
					if (env.write_type != 0) targets ~= env.write_type;

					if (vy.selection) whereami (text(name ~ "sending ", targets.length, " targets"));

					writeProperty (env.display, sr, X11.XA_ATOM, 32, cast(ubyte*)targets.ptr , cast(int)targets.length);
				}
				else // write back nothing
				{
					if (vy.selection) whereami (env.name, ": sending nothing");
					writeProperty (env.display, sr, env.write_type, env.write_format, null, 0);
				}
			}
		}
	}

	private static void _incr (X11.XEvent xevent, Environment* env)
	{
		auto name = env.name ~ "._incr:     ";

		if (xevent.type == X11.PropertyNotify) // beware of incr protocol!
		{
			auto pn = xevent.xproperty;

			// since more than one write request can be running simultaneously,
			// we need the correct context fotrr this request
			auto context = env.write_contexts.find (xevent.xproperty.window);

			if (context.incr == true)
			{
				if (pn.atom == context.property && pn.state == X11.PropertyDelete)
				{

					context.to = min (context.from+env.maxlen, env.write_buffer.length);
					auto slice = env.write_buffer[context.from..context.to];

					auto length = cast(int) (slice.length / bytesForFormat (env.write_format));

					if (vy.selection) whereami (text(name ~ "sending ", length, " items of format ", env.write_format));

					X11.XChangeProperty(env.display,
						context.window,
						context.property,
						env.write_type, env.write_format, X11.PropModeReplace,
						slice.ptr, length);

					if (slice.length == 0) // finished?
					{
						if (vy.selection) whereami (name ~ "INCR protocol finished");
						X11.XSelectInput (env.display, context.window, X11.NoEventMask);
					}
					else
					{
						context.from = context.to;
					}
				}
				else
				{
					if (vy.selection) whereami (text (name ~ "ignoring PropertyNotify (atom ",
						 pn.atom, " and state ", pn.state, ")"));
				}
			}
			else
			{
				if (vy.selection) whereami (text (name ~ "ignoring PropertyNotify (atom ", pn.atom,
					" and state ", pn.state, ") because INCR protocol is not active"));

			}
		}
	}

	Tuple!(int, ubyte[]) x11_read (X11.Atom t_atom, X11.Atom s_atom)
	{
		_env.read_buffer.length = 0;
		_env.read_format = 8;

		auto name = _env.name ~ ".x11_read:  ";

		if (t_atom == 0)
		{
			if (vy.selection) whereami (name ~ "illegal target 0");
			return Tuple!(int, ubyte[])(_env.read_format, _env.read_buffer);
		}

		if (X11.XGetSelectionOwner(_display, s_atom) == 0)
		{
			if (vy.selection) whereami (name ~ "no active selection");
			return Tuple!(int, ubyte[])(_env.read_format, _env.read_buffer);
		}

		// acc. to ICCC it is mandatory to delete the property first
		X11.XDeleteProperty (_display, _env.read_window, _env.property);

		// write a conversion request to X11
		X11.XConvertSelection (_display,
							   s_atom,
							   t_atom,
							   _env.property,
							   _env.read_window, X11.CurrentTime);
		X11.XFlush(_display);

		// and wait for a signal from task
		bool result;
		if (receiveTimeout (1.seconds, (bool r){result = r;}))
		{
			if (result == false)
			{
				_env.read_buffer.length = 0;
				if (vy.selection) whereami (text (name ~ "could not retrieve data for atom ", t_atom));
			}
		}
		else
		{
			_env.read_buffer.length = 0;
			if (vy.selection) whereami (text (name ~ "received timeout in retrieving clipboard content"));
		}
		return Tuple!(int, ubyte[])(_env.read_format, _env.read_buffer);
	}

	/** return a copy of the current clipboard content;
		the result is a tuple with the values
		tuple[0]: format
		tuple[1]: a ubyte[] array containing the data

		if the format is  8 the data will be an array of bytes
		if the format is 16 the data will be an array of shorts
		if the format is 32 the data will be an array of c_long, which has a length of 4 bytes
		on 32 bit applications and 8 bytes with the upper 4 bytes set to zero on 64 bit applications
	**/

	Tuple!(int, ubyte[]) read (string type, SelectionType selection=SelectionType.CLIPBOARD)
	{
		auto t_atom = X11.XInternAtom (_display, type.toStringz, true);
		auto s_atom = selection == SelectionType.PRIMARY ?
					  _env.primary :
					  _env.clipboard;

		return x11_read (t_atom, s_atom);
	}

	override byte[] readData (string type, SelectionType selection=SelectionType.CLIPBOARD)
	{
		auto result = read (type, selection);
		return cast(byte[])result[1];

	}

	override char[] readText (SelectionType selection=SelectionType.CLIPBOARD)
	{
		char[] none;
		auto result = read ("UTF8_STRING", selection);

//		if (result[0] == 8)
			return cast(char[])result[1];
//		else return none;
	}

	override string[] readTargets (SelectionType selection=SelectionType.CLIPBOARD)
	{
		string[] none;

		auto result = read ("TARGETS", selection);

		if (result[0] != 32) return none;

		// convert atoms into strings
		string[] targets;
		foreach (atom; cast(c_long[])result[1])
		{
			auto name = X11.XGetAtomName (_display, atom);
			targets ~= name.fromStringz.dup;
			X11.XFree (name);
		}

		return targets;
	}

	void x11_write (ubyte[] data, int format, X11.Atom atype, X11.Atom asel)
	{
		auto name = _env.name ~ ".x11_write: ";

		if (atype == 0)
		{
			if (vy.selection) whereami (name ~ "illegal type atom 0");
			return;
		}

		if (asel == 0)
		{
			if (vy.selection) whereami (name ~ "illegal selection atom 0");
			return;
		}

		if (format != 8 && format != 16 && format != 32)
		{
			if (vy.selection) whereami (text(name ~ "illegal format ", format));
			return;
		}

		_env.write_buffer = data;
		_env.write_type   = atype;
		_env.write_format = format;

		X11.XSetSelectionOwner (_env.display, asel, _env.write_window, X11.CurrentTime);
		X11.XFlush (_env.display); // flushing is absolutely neccessary here
	}

	void write (ubyte[] data, int fmt, string type, SelectionType selection)
	{
		auto name = _env.name ~ ".write:     ";

		auto atype = X11.XInternAtom (_display, type.toStringz, false);
		if (atype == 0)
		{
			if (vy.selection) whereami (text(name ~ "illegal atom ", type, " for ", _env.name));
			return;
		}
		if (vy.selection) whereami (text(name ~ "atom nr. for atom name ", type, " is ", atype));

		X11.Atom asel;
		if (selection == SelectionType.PRIMARY) asel = _env.primary;
		else                                    asel = _env.clipboard;

		x11_write (data, fmt, atype, asel);
	}

	void write(T) (T[] data, string type, SelectionType selection)
	{
		static if      (T.sizeof == byte.sizeof)  write (cast(ubyte[])data, 8, type, selection);
		else static if (T.sizeof == short.sizeof) write (cast(ubyte[])data, 16, type, selection);
		else static if (T.sizeof == c_long.sizeof) write (cast(ubyte[])data, 32, type, selection);
	}

	override void writeData (in byte[] data, string type, SelectionType selection)
	{
		write (data, type, selection);
	}

	override void writeText (in char[] txt, SelectionType selection=SelectionType.CLIPBOARD)
	{
		write (cast(ubyte[]) txt, "UTF8_STRING", selection);
	}
} // Clipboard_X11
} // version (X11)

module wcw.x11.window;

/*########################################################################################
#
# X11 backend for Window
#
########################################################################################*/

version (X11)
{

import std.algorithm;
import std.string;

static import X11;
import cairo;

import wcw.auxiliary;
import wcw.enums;
import wcw.application;
import wcw.cairo;
import wcw.event;
import wcw.window;
import wcw.image;
import wcw.x11.application;

class Window_X11 : Window_Backend
{
private:
	Window      _frontend;
	X11.Window  _xwindow;
	bool _existent;
	bool _moved;
	cstring _caption;

public:
	this (Window w)
	{
		_frontend = w;
		_caption = _frontend.caption;

		X11.Window x11parent;
		x11parent = X11.XDefaultRootWindow (xdisplay);
		ulong mask =
			X11.CWBackingStore     |
			X11.CWOverrideRedirect |
			X11.CWEventMask        |
			0;

		X11.XSetWindowAttributes  attrs;
		attrs.backing_store = X11.NotUseful;
		attrs.event_mask =
			X11.KeyPressMask             |
			X11.KeyReleaseMask           |
			X11.ButtonPressMask          |
			X11.ButtonReleaseMask        |
			X11.EnterWindowMask          |
			X11.LeaveWindowMask          |
			X11.PointerMotionMask        |
			X11.ExposureMask             |
			X11.StructureNotifyMask      |
			X11.FocusChangeMask          |
			0;

		if (frontend.attributes & WindowAttribute.UNMANAGED)
		{
			// no borders drawn and position not changed by WM
			attrs.override_redirect = true;
		}

		auto visual = X11.XDefaultVisual (xdisplay, X11.DefaultScreen(xdisplay));
		auto root   = X11.XDefaultRootWindow (xdisplay);

		_xwindow = X11.XCreateWindow (xdisplay,
			root,
			// X11 window manager places dialog over the parent window only,
			// when initial position != 0,0
			1, 1,
			// a minimum size of 1,1 is needed for CreateWindow to succeed
			1, 1,
			0,
			X11.CopyFromParent,
			X11.InputOutput,
			visual,
			mask, &attrs);

		// tell X11 to send a close message when close button in frame is pressed
		x11_setProperty (xapp.x11_Atom_Protocol, xapp.x11_Atom_DeleteWindow);

		if (frontend.attributes & WindowAttribute.TRANSIENT)
		{
			if (frontend.transitionParent)
			{
				auto window = xwindow;
				auto parent = (cast(Window_X11)frontend.transitionParent.backend).xwindow;
				X11.XSetTransientForHint (xdisplay, window, parent);
			}
		}

		if (frontend.attributes & WindowAttribute.MODAL)
		{
			// make window modal
			auto state = xapp.x11_Atom_State;
			auto modal = xapp.x11_Atom_StateModal;
			x11_setProperty (state, modal);
		}

		if (frontend.role == WindowRole.DIALOG)
		{
			// inform window manager about window's role
			auto type   = xapp.x11_Atom_WindowType;
			auto dialog = xapp.x11_Atom_WindowTypeDialog;
			x11_setProperty (type, dialog);
		}
		else if (frontend.role == WindowRole.MENU)
		{
			// inform window manager about window's role
			auto type   = xapp.x11_Atom_WindowType;
			auto normal = xapp.x11_Atom_WindowTypeMenu;
			x11_setProperty (type, normal);
		}
		else
		{
			// inform window manager about window's role
			auto type   = xapp.x11_Atom_WindowType;
			auto normal = xapp.x11_Atom_WindowTypeNormal;
			x11_setProperty (type, normal);
		}

		if (frontend._caption != "") setCaption (frontend._caption);

		// create a surface and set _created to true
		frontend._surface = new CairoXlibSurface (xdisplay, xwindow, visual, frontend.size.x, frontend.size.y);
	}

	~this ()
	{
		debug (shutdown) whereami ("~this Window_X11 " ~ _caption);
		if (xwindow) X11.XDestroyWindow (xdisplay, xwindow);
	}

	Window frontend () {return _frontend;}
	Application_X11 xapp () {return cast(Application_X11)App.backend;}
	X11.Display* xdisplay () {return xapp.x11_Display;}
	X11.Window xwindow () {return _xwindow;}

	CairoXlibSurface surface ()
	{
		assert (frontend.surface.get_type == CairoSurfaceType.XLIB);
		return cast(CairoXlibSurface) frontend._surface;
	}

	X11.XSizeHints _sizehints;

	override void resize (IPair newsize, IPair newpos=IPair(int.min,int.min))
	{
		move (newpos, newsize);
	}

	override void move (IPair newpos, IPair newsize=IPair(int.min,int.min))
	{
		bool m = true;
		bool r = true;

		if (newpos.x == int.min || newpos.y == int.min) m=false;
		if (newsize.x == int.min || newsize.y == int.min) r=false;

		if (!m && !r) return;

		uint vmask;
		X11.XWindowChanges values;

		if (m)
		{
			_moved = true;

			vmask = X11.CWX | X11.CWY;
			values.x = min (short.max, newpos.x);
			values.y = min (short.max, newpos.y);

			if (frontend.isVisible == false) frontend.setPosition (newpos);
		}

		if (r)
		{
			vmask |= X11.CWWidth | X11.CWHeight;
			values.width  = min (short.max, newsize.x);
			values.height = min (short.max, newsize.y);

			surface.set_size (newsize.x, newsize.y);
			if (frontend.isVisible == false) frontend.setSize (newsize);
		}

		X11.XConfigureWindow (xdisplay, xwindow, vmask, &values);
	}

	void setSurfaceSize (IPair size)
	{
		surface.set_size (size.x, size.y);
	}

	void x11_setProperty (X11.Atom prop, X11.Atom atom)
	{
		auto r = X11.XChangeProperty (xdisplay,
			xwindow,
			prop,
			X11.XA_ATOM,
			32,
			X11.PropModeReplace,
			cast(const ubyte*)&atom,
			1);

		if (r == 0) whereami ("error in writing property");
	}

	void x11_setProperty (X11.Atom prop, cstring text)
	{
		auto r = X11.XChangeProperty (xdisplay,
			xwindow,
			prop,
			xapp.x11_Atom_Utf8String,
			8,
			X11.PropModeReplace,
			cast(const ubyte*)toStringz(text),
			cast(int)text.length);

		if (r == 0) whereami ("error in writing property");
	}

	override void show ()
	{
		X11.XSizeHints hints;
		hints.flags = X11.PMinSize | X11.PMaxSize;
		hints.min_width      = min (short.max, frontend.minSize.x);
		hints.min_height     = min (short.max, frontend.minSize.y);
		hints.max_width      = min (short.max, frontend.maxSize.x);
		hints.max_height     = min (short.max, frontend.maxSize.y);

		// if window's position was changed by move(), this will tell the WM to use
		// x and y from the last XConfigureWindow call or x and y from the XCreateWindow call
		if (_moved) hints.flags |= X11.PPosition;

		X11.XSetWMNormalHints (xdisplay, xwindow, &hints);
		X11.XMapWindow (xdisplay, xwindow);
	}

	override void hide ()
	{
		X11.XUnmapWindow (xdisplay, xwindow);
		X11.XFlush (xdisplay);
	}

	override void setCaption (cstring title)
	{
		frontend._caption = title;

		if (xwindow == 0) return;

		// window and icon name with utf8 support
		x11_setProperty (xapp.x11_Atom_Name, title);
		x11_setProperty (xapp.x11_Atom_IconName, title);
	}

	override int[4] frameSizes ()
	{
		X11.Atom eAtom = X11.XInternAtom (xdisplay,"_NET_FRAME_EXTENTS", true);
		X11.Atom typ;
		int fmt;
		ulong num, aft;
		ulong *data;

		int[4] sizes;

		if (X11.Success == X11.XGetWindowProperty (xdisplay, xwindow,
			eAtom, 0, 4, 0, X11.AnyPropertyType,
			&typ, &fmt, &num, &aft, cast(ubyte**)&data))
		{
			assert (num == 4);
			sizes = [cast(int) data[0],
					 cast(int) data[2],
					 cast(int) data[1],
					 cast(int) data[3]];
			X11.XFree(data);
		}
		return sizes;
	}

	override void setApplicationIcon (Image image)
	{
	    int width  = image.surface.get_width();
	    int height = image.surface.get_height();
	    int stride = image.surface.get_stride()/4;

	    int* data = cast(int*) image.surface.get_data();

		// Prepare icon data for X11
		int length = 2 + width * height;

		import core.stdc.config;
		c_long[] icon_data;			// c_long is 32 bit or 64 bit depending on CPU
		icon_data ~= width;
		icon_data ~= height;

		int i=2;
		for (int h=0; h < height; h++) for (int w=0; w < width; w++)
		{
			uint pixel = data [h*stride+w];
			icon_data ~= cast(c_long)pixel;
		}

		X11.Atom atom = X11.XInternAtom (xdisplay, "_NET_WM_ICON", false);
		X11.XChangeProperty (xdisplay, xwindow, atom, X11.XA_CARDINAL, 32, X11.PropModeReplace, cast(ubyte*)icon_data.ptr, length);
	}
} // Window_X11
} // version (X11)

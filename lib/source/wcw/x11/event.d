module wcw.x11.event;

/*########################################################################################
#
# X11 backend for Event
#
########################################################################################*/

version (X11)
{
import std.string;

import wcw.event;
import wcw.auxiliary;
import wcw.enums;
import wcw.application;
import wcw.x11.application;

static import X11;

private Application_X11 x11_backend (Application app) {return cast(Application_X11)app.backend;}

/** if x11CreateEvent can't create an event for the X11 event type,
	an event with EventType.INVALID will be created
**/
Event x11CreateEvent (ref X11.XEvent xevent)
{
	switch (xevent.type)
	{

	case X11.KeyPress:
	{
		X11.KeySym symbol;
		X11.Status status;	// not used
		int count;
		char [256] buffer;

		// translate keycode to utf8 string
		if (App.x11_backend.x11_Xic)
			count = X11.Xutf8LookupString(App.x11_backend.x11_Xic, &xevent.xkey, buffer.ptr, 255, &symbol, &status);
		else
			count = X11.XLookupString(&xevent.xkey, buffer.ptr, 255, &symbol, null); // fallback into Latin1

		buffer[count] = '\0';

		// now this is dirty: modify the original event to make XFilterEvent believe
		// that the event came from the hidden proxyWindow; otherwise it would fail
		// since proxyWindow is the only window, that has a XIC (check out the
		// constructor of Application). Don't forget to reset this value upon return

		auto w = xevent.xkey.window;
		xevent.xkey.window = App.x11_backend.x11_ProxyWindow;

		// check, if string is ready (if composed, e.g from "`" and "a" key -> "à"
		bool   ready  = X11.XFilterEvent(&xevent, App.x11_backend.x11_ProxyWindow) ? false : true;
		string text = fromStringz (buffer.ptr).dup;
		char c = buffer[0];

		xevent.xkey.window = w; // undo the previous change

		auto event = new KeyPressEvent ();
		if (count == 1 && symbol != 0 && (c <= 0x1f || 0x7f <= c && c <= 0x9f))
		{
			event.keycode  = xevent.xkey.keycode;
			event.symbol   = cast(KeySymbol)symbol;
			event.modifier = cast(ModifierKey)xevent.xkey.state;
			event.text     = "";
		}
		else if (ready == true)
		{
			event.keycode  = xevent.xkey.keycode;
			event.symbol   = cast(KeySymbol)symbol;
			event.modifier = cast(ModifierKey)xevent.xkey.state;
			event.text     = text;
		}
		return event;
	}

	case X11.KeyRelease:
	{
		auto event = new KeyReleaseEvent ();
		return event;
	}

	// MouseEvents

	case X11.MotionNotify:
	{
		auto event = new MouseMoveEvent (IPair (&xevent.xmotion.x));
//			event.button = xevent.xmotion.button;
		event.modifier = cast(ModifierKey) xevent.xmotion.state;
//		event.prioritise = true;
		return event;
	}

	case X11.ButtonPress:
	{
		auto  position = IPair (&xevent.xbutton.x);
		auto state = xevent.xbutton.state;
		MouseButton button;

		if      (xevent.xbutton.button == 1) button = MouseButton.LEFT;
		else if (xevent.xbutton.button == 2) button = MouseButton.MIDDLE;
		else if (xevent.xbutton.button == 3) button = MouseButton.RIGHT;
		else if (true)                       button = cast (MouseButton) xevent.xbutton.button;
		else                         button = MouseButton.NONE;

		auto event = new ButtonPressEvent (IPair (&xevent.xbutton.x));
		event.button = button;
		event.modifier = cast(ModifierKey) state;

		return event;
	}

	case X11.ButtonRelease:
	{
		auto  position = IPair (&xevent.xbutton.x);
		auto state = xevent.xbutton.state;
		MouseButton button;

		if      (xevent.xbutton.button == 1) button = MouseButton.LEFT;
		else if (xevent.xbutton.button == 2) button = MouseButton.MIDDLE;
		else if (xevent.xbutton.button == 3) button = MouseButton.RIGHT;
		else if (xevent.xbutton.button == 6) button = MouseButton.TILT_LEFT;
		else if (xevent.xbutton.button == 7) button = MouseButton.TILT_RIGHT;
		else                         button = MouseButton.NONE;

		auto event = new ButtonReleaseEvent (IPair (&xevent.xbutton.x));
		event.button = button;
		event.modifier = cast(ModifierKey) state;

		return event;
	}

	case X11.ClientMessage:
	{
		/*  currently 3 different events are converted:
			- Close event, created by x11, when the close-window-button
			  in the upper right corner of a window was pressed
			  (data == Protocol and type == DeleteWindow)
			- Close event, when CTRL-C was pressed (type == SigInt)
			- Clock event (type == SigClock)
		*/
		if (xevent.xclient.message_type == App.x11_backend.x11_Atom_Protocol)
		{
			if (xevent.xclient.data.l[0] == App.x11_backend.x11_Atom_DeleteWindow)
			{
				auto event = new CloseEvent ();
				return event;
			}
		}
		else if (xevent.xclient.message_type == App.x11_backend.x11_Atom_SigInt)
		{
			auto event = new CloseEvent ();
			event.broadcast = true;
			return event;
		}
		else if (xevent.xclient.message_type == App.x11_backend.x11_Atom_SigClock)
		{
			auto event = new ClockEvent ();
			event.broadcast = true;
			return event;
		}
		return new Event();
	}

	case X11.Expose:
	{
		auto event = new RedrawEvent ();
//		event.prioritise = true;
		return event;
	}

	case X11.ConfigureNotify:
	{
		auto event = new TransformEvent ();

		// (xevent.xconfigure.above == 0) works on xfce but not on some other WMs, therefore 
		// replaced with (xevent.xconfigure.send_event == 1). That seems to do the trick

		if ((xevent.xconfigure.send_event == 1) || (xevent.xconfigure.override_redirect == 1))
			event.position = IPair (&xevent.xconfigure.x);
		else
			event.position = IPair(int.min,int.min);

		event.size = IPair (&xevent.xconfigure.width);
//		event.prioritise   = true;
		return event;
	}

	case X11.LeaveNotify:
	{
		auto event = new MouseOutEvent ();
//		event.prioritise = true;
		return event;
	}

	case X11.EnterNotify:
	{
		auto event = new MouseInEvent ();
		return event;
	}

	case X11.FocusIn:
	{
		auto event = new FocusInEvent ();
//		event.prioritise = true;
		return event;
	}

	case X11.FocusOut:
	{
		auto event = new FocusOutEvent ();
//		event.prioritise = true;
		return event;
	}

	case X11.MapNotify:
	{
		auto event = new ShowEvent ();
//		event.prioritise = true;
		return event;
	}

	default:
		debug (events) whereami(format("unknown X11 event %s for x11 window %x", xevent.type, xevent.xany.window));
		return new Event ();
	}
} // CreateEvent_X11
} // version X11
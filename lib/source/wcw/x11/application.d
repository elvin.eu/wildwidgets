module wcw.x11.application;

/*########################################################################################
#
# X11 Backend
#
########################################################################################*/

version (X11)
{
static import X11;

import std.exception;
import std.stdio;
import std.concurrency;
import std.datetime;
import std.string;
import core.stdc.signal;
import core.atomic;

import wcw.application;
import wcw.event;
import wcw.auxiliary;
import wcw.enums;
import wcw.x11.event;
import wcw.x11.window;
import wcw.window;

private Window_X11 x11_backend (Window w) {return cast(Window_X11)w.backend;}

// need one valid but hidden window, so that custom made events can be sent that
// don't target a specific X11 widget
shared private X11.Window _x11_proxyWindow;
shared private bool s_quit;
shared private bool s_tick;

extern (C) void signalHandler (int signal) nothrow @nogc @system
{
	if (globalApplication is null) return;

	// the X11 event loop is triggered every 100ms from the timer interrupt, which is generated
	// by clock_task; so, setting the shared variable s_quit to true is enough to tell
	// run() to call quit();
	if (signal == SIGINT) atomicStore(s_quit, true);
}


debug (events) enum tick = 5000; // 100 msec would be a bit much for debugging
else           enum tick = 100;

void clock_task (shared X11.Display* disp, X11.Atom atom, X11.Window win)
{
	for (;;)
	{
		try
		{
			receiveTimeout (tick.msecs); // this is our clock

			if (disp && atomicLoad(s_tick))
			{
				// disable ticking until it is allowed again at the end of the event loop
				atomicStore(s_tick, false);

				X11.XClientMessageEvent event;
				event.type         = X11.ClientMessage;
				event.window       = win;
				event.message_type = atom;
				event.format       = 32;
				event.data.l       = [0, 0, 0, 0, 0]; // data is a union, need to initialize it manually

				X11.XSendEvent (cast (X11.Display*) disp, win, 0, 0,  cast(X11.XEvent*)&event);
				X11.XFlush (cast (X11.Display*) disp);
			}
		}
		catch (Throwable)
		{
			// probably the main task terminated. let's terminate as well, otherwise
			// the process never ends
			debug (shutdown) whereami ("clock_task ends");
			return;
		}
	}
}

class Application_X11 : Application_Backend
{
private:
	Application _frontend;

	X11.Display* _x11_display;
	X11.XIM  _x11_xim;
	X11.XIC  _x11_xic;

public:
	immutable X11.Atom x11_Atom_Protocol;
	immutable X11.Atom x11_Atom_DeleteWindow;
	immutable X11.Atom x11_Atom_SigInt;
	immutable X11.Atom x11_Atom_SigClock;

	immutable X11.Atom x11_Atom_Name;
	immutable X11.Atom x11_Atom_IconName;
	immutable X11.Atom x11_Atom_Utf8String;

	immutable X11.Atom x11_Atom_WindowType;
	immutable X11.Atom x11_Atom_WindowTypeDesktop;
	immutable X11.Atom x11_Atom_WindowTypeDock;
	immutable X11.Atom x11_Atom_WindowTypeMenu;
	immutable X11.Atom x11_Atom_WindowTypeUtility;
	immutable X11.Atom x11_Atom_WindowTypeSplash;
	immutable X11.Atom x11_Atom_WindowTypeDialog;
	immutable X11.Atom x11_Atom_WindowTypeNormal;

	immutable X11.Atom x11_Atom_State;
	immutable X11.Atom x11_Atom_StateModal;
	immutable X11.Atom x11_Atom_StateSticky;
	immutable X11.Atom x11_Atom_StateAbove;
	immutable X11.Atom x11_Atom_StateBelow;

	X11.Display* x11_Display ()      {return _x11_display;}
	X11.XIM      x11_Xim ()          {return _x11_xim;}
	X11.XIC      x11_Xic ()          {return _x11_xic;}
	X11.Window   x11_ProxyWindow ()  {return _x11_proxyWindow;}

 	this (Application a)
	{
		assert (a);
		_frontend = a;

		enforce (X11.XInitThreads, "cannot initialize Xlib thread support");

		// connect to X server
		_x11_display = X11.XOpenDisplay (null);
		enforce (x11_Display, "cannot connect to X server");

		// find current screen
		int screen = X11.DefaultScreen(x11_Display);

		frontend._resolution.x = X11.XDisplayWidth(x11_Display, screen)*254/X11.XDisplayWidthMM(x11_Display, screen)/10;
		frontend._resolution.y = X11.XDisplayHeight(x11_Display, screen)*254/X11.XDisplayHeightMM(x11_Display, screen)/10;

		// 2 newly created atoms
		x11_Atom_SigInt       = x11_GetAtom ("WCW_SIGINT", true);
		x11_Atom_SigClock     = x11_GetAtom ("WCW_SIGCLOCK", true);

		x11_Atom_Protocol     = x11_GetAtom ("WM_PROTOCOLS");
		x11_Atom_DeleteWindow = x11_GetAtom ("WM_DELETE_WINDOW");
		x11_Atom_Name         = x11_GetAtom ("_NET_WM_NAME");
		x11_Atom_IconName     = x11_GetAtom ("_NET_WM_ICON_NAME");
		x11_Atom_Utf8String   = x11_GetAtom ("UTF8_STRING");

		x11_Atom_WindowType        = x11_GetAtom ("_NET_WM_WINDOW_TYPE");
		x11_Atom_WindowTypeDesktop = x11_GetAtom ("_NET_WM_WINDOW_TYPE_DESKTOP");
		x11_Atom_WindowTypeDock    = x11_GetAtom ("_NET_WM_WINDOW_TYPE_DOCK");
		x11_Atom_WindowTypeMenu    = x11_GetAtom ("_NET_WM_WINDOW_TYPE_MENU");
		x11_Atom_WindowTypeUtility = x11_GetAtom ("_NET_WM_WINDOW_TYPE_UTILITY");
		x11_Atom_WindowTypeSplash  = x11_GetAtom ("_NET_WM_WINDOW_TYPE_SPLASH");
		x11_Atom_WindowTypeDialog  = x11_GetAtom ("_NET_WM_WINDOW_TYPE_DIALOG");
		x11_Atom_WindowTypeNormal  = x11_GetAtom ("_NET_WM_WINDOW_TYPE_NORMAL");

		x11_Atom_State       = x11_GetAtom ("_NET_WM_STATE");
		x11_Atom_StateModal  = x11_GetAtom ("_NET_WM_STATE_MODAL");
		x11_Atom_StateSticky = x11_GetAtom ("_NET_WM_STATE_STICKY");
		x11_Atom_StateAbove  = x11_GetAtom ("_NET_WM_STATE_ABOVE");
		x11_Atom_StateBelow  = x11_GetAtom ("_NET_WM_STATE_BELOW");

		// create an invisible window which is always present so that clockTask()
		// has a guaranteed existing window to send events to
		_x11_proxyWindow = X11.XCreateWindow (_x11_display, X11.XDefaultRootWindow (_x11_display),
			0, 0, 1, 1, 0, X11.CopyFromParent, X11.InputOnly,
			X11.XDefaultVisual (_x11_display, X11.DefaultScreen(_x11_display)), 0, null);

		debug (events) whereami(format("created proxy window %x", _x11_proxyWindow));

		/*  the next nonse now only for utf8 keyboard input:
			xim: input method protocol
			xic: input context

			the man pages for XopenIM and XCreateIC are just a bad joke.
			There's some example code at https://gist.github.com/Determinant (xim_example.c)
		*/

		_x11_xim = X11.XOpenIM (_x11_display, null, null, null);
		if (_x11_xim is null) whereami(format("XOpenIM failed"));

		_x11_xic = X11.XCreateIC (x11_Xim, X11.XNInputStyle.ptr,
			// if XIMPreeditNothing and X11.XIMStatusNothing are not supported,
			// we can't create a xic and keyboard input falls back to Latin1.
			X11.XIMPreeditNothing | X11.XIMStatusNothing,
			X11.XNClientWindow.ptr, _x11_proxyWindow, null);
		if (_x11_xic is null) stderr.writefln ("Warning: file %s, line %s, XCreateIC failed", __FILE__, __LINE__);

		// sigIntHandler needs globalApplication
		if (signal (SIGINT, &signalHandler) == SIG_ERR)
		{
			whereami(format("Could not install signal handler for SIGINT"));
		}
		// start a timer in a separate thread
		spawn (&clock_task, cast(shared)x11_Display, x11_Atom_SigClock, _x11_proxyWindow);
	}


	~this ()
	{
		debug (shutdown) whereami("~this Application_X11");
		X11.XCloseDisplay (_x11_display);
	}

	Application frontend () {return _frontend;}

	X11.Atom x11_GetAtom (cstring name, bool create=false)
	{
		auto atom = X11.XInternAtom (x11_Display, name.toStringz, !create);
		if (atom == X11.None ) stderr.writeln ("Error: cannot find Atom " ~ name);
		return atom;
	}


	/** enter the main event loop. read in events from the X server,
		convert them to wcw events and send them to the appropriate Window.
		Exits, when _quit becomes true
	**/
	DialogResult run (Window self)
	{
		while ((frontend._quit == false))
		{
			// only paint windows, when no events are anymore pending
			int pending = X11.XPending(_x11_display);
			if (pending == 0)
			{
				foreach (ww; frontend._windowList)
				{
					ww.paintTaintedWidgets();
				}
			}

			debug (events) if (pending > 0) whereami(format("still %s %s pending", pending, pending==1?"event":"events"));

			// event processing starts here: read in X11 event and convert it to a wcw Event
			X11.XEvent x11_event;
			X11.XNextEvent (_x11_display, &x11_event);

			// swallow all remaining MotionNotify Events in the queue
			if (x11_event.type == X11.MotionNotify)
			{
				while (X11.XCheckTypedWindowEvent (_x11_display,
					x11_event.xmotion.window,
					X11.MotionNotify,
					&x11_event)) {}
			}

			auto event = x11CreateEvent (x11_event);

			debug (events) whereami(format("X11 event %s converted to %s", x11_event.type, event));

			bool dispatched;
			if (event.broadcast)
			{
				// dispatch to all windows
				foreach (ww; frontend._windowList)
				{
					debug (events) whereamif ("dispatching event %s to %s", event, ww);

					ww.dispatchEvent (event);
					dispatched = true;
				}
			}
			else
			{
				// dispatch to single window
				foreach (ww; frontend._windowList)
				{
					if (ww.x11_backend.xwindow == x11_event.xany.window)
					{
						debug (events) whereamif ("dispatching event %s to %s", event, ww);

						ww.dispatchEvent (event);
						dispatched = true;

						// this is how modal dialogs end
						if (ww.result != DialogResult.NONE && ww == self) return ww.result;
						break;
					}
				}
			}

			debug (events) if (dispatched == false) whereami(format("can't dispatch event"));

			// after all current events are processed check if there's a menu open
			frontend.app_showMenu();

			X11.XFlush (_x11_display);

			// allow the clock to tick
			atomicStore(s_tick, true);

			// greetings from signalHandler()
			if (atomicLoad(s_quit)) frontend.quit();
		} // while
		return DialogResult.NONE;
	} // run()

	void beep ()
	{
		X11.XBell (x11_Display, 100);
	}

} // Application_X11
} // version (X11)

module wcw.cairo;

import wcw.color;
import wcw.font;
import wcw.auxiliary;

import cairo;

void set_source_color (CairoContext context, Color color)
{
	context.set_source_rgb (color.red/255.0, color.green/255.0, color.blue/255.0);
}

void add_color_stop_color (CairoLinearPattern pattern, double offset, Color c)
{
	pattern.add_color_stop_rgb (offset, c.red/255.0, c.green/255.0, c.blue/255.0);
}

void add_color_stop_color (CairoRadialPattern pattern, double offset, Color c)
{
	pattern.add_color_stop_rgb (offset, c.red/255.0, c.green/255.0, c.blue/255.0);
}

/** copy a rectangle area on a surface to another location on the same surface
**/
void copy (CairoContext context, double x, double y, double dx, double dy, double w, double h)
{
	if (dx == 0 && dy == 0) return;

	auto sf = context.get_target;
	auto st = CairoSurface.create_for_rectangle (sf, x+dx, y+dy, w, h);
	auto ct = new CairoContext (st);
	ct.set_source_surface (sf, -x, -y);
	ct.paint ();
}

/** print text
**/
void write (CairoContext context, double x, double y, cstring text)
{
	auto font = context.get_scaled_font ();
	auto extents = font.extents;
	auto glyphs = font.text_to_glyphs (x, y+extents.ascent, text);
	context.show_glyphs (glyphs);
}

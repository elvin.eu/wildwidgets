module wcw.toolbar;

import cairo;

import wcw.application;
import wcw.widget;
import wcw.layout;
import wcw.button;
import wcw.image;
import wcw.auxiliary;
import wcw.cairo;
import wcw.event;
import wcw.enums;

/** ToolBar
**/

class ToolBar : Widget
{
	this (Widget parent, int[] i...)
	{
		super (parent, i);
		new HorizontalLayout (this);
		setExtSize (-1, 0);
		// setAlignment (Align.LEFT);
		setWidgetPalette (App.design.toolboxPalette);
		setSpacing (0);
		setEdgeRadius (App.design.toolboxEdge);
		addFrame();

	}

	override void addFrame () {setFrameWidth (App.design.toolboxFrame);}
}


/** ToolButton
**/
class ToolButton : BaseButton
{
	Image _image;
	Image _pressed_image;

	bool _pressable;
	bool _pressed;

public:
	this (Image image, Widget parent, int[] i...)
	{
		super(parent, i);

		// hmm -- hard coded sizes?
		setFixSize (32, 32);

		setWidgetPalette (App.design.buttonPalette);
		setEdgeRadius (0);
		setFrameWidth (0);
		_image = image;
	}

	this (Image image, Image pressed, Widget parent, int[] i...)
	{
		this(image, parent, i);
		_pressed_image = pressed;
		_pressable = true;
	}

	override void addFrame () {setFrameWidth (App.design.buttonFrame);}

	bool isPressed () const {return _pressed;}

	void setPressed (bool set)
	{
		if (!_pressable || isPressed == set) return;

		_pressed = set;
		clicked.emit ();
		setHilight();
		redraw();
	}

	override void paint (CairoContext ctx)
	{
		if (!ctx) return;
		super.paint (ctx);

		// setting the image surface as source for the drawing surface and paint it

		auto width  = _image.surface.get_width ();
		auto height = _image.surface.get_height ();

		if (_pressed) ctx.set_source_surface (_pressed_image.surface, (32-width)/2, (32-height)/2);
		else          ctx.set_source_surface (_image.surface, (32-width)/2, (32-height)/2);
		ctx.paint ();
	}

	override void buttonReleaseHandler (in ButtonReleaseEvent event)
	{
		if (isPassive) return;
		if (!mapToWindow(rectangle).contains(event.pointer)) return;

		if (event.button == MouseButton.LEFT)
		{
			if (_pressable) _pressed = !_pressed;
			clicked.emit ();
			setHilight();
		}
		else
		{
			setActive ();
		}

		redraw();
	}
}

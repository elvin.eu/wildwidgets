module wcw.color;

import std.format;
import std.string;
import std.algorithm: min, max;
import std.exception;

import wcw.auxiliary;


struct Color
{
	union
	{
		uint c;	// the color as an unsigned integer. In Hex: aarrggbb

		version (BigEndian) {struct {ubyte a, r, g, b;}}
		version (LittleEndian) {struct {ubyte b, g, r, a;}}
	}

	alias argb this;

	this (int color, ubyte alpha=255)
	{
		c = color&0x00ffffff;
		a = alpha;
	}

	this (int red, int green, int blue, int alpha=255)
	{
		a = cast(ubyte)alpha;
		r = cast(ubyte)red;
		g = cast(ubyte)green;
		b = cast(ubyte)blue;
	}

	this (double red, double green, double blue, double alpha=1.0)
	{
		a = cast(ubyte)(min(1.0, max (0.0, alpha))*255.0);
		r = cast(ubyte)(min(1.0, max (0.0, red))*255.0);
		g = cast(ubyte)(min(1.0, max (0.0, green))*255.0);
		b = cast(ubyte)(min(1.0, max (0.0, blue))*255.0);
	}

	this (const(char)[] str)
	{
		c = 0xff000000;

		try
		{
			 c =  argb_from_string (str);
		}
		catch (Throwable)
		{
			// do nothing
		}
	}

	static Color argb (uint c) {return Color (c);}

	static Color rgb (uint c) {return Color (c|0xff000000);}

	uint rgb () {return c&0x00ffffff;}

	uint argb () {return c;}

	/// return color as istring "#AARRGGBB"
	string toString () const
	{
		if (a == 255) return  format!"#%02X%02X%02X"(r, g, b);
		else          return  format!"#%02X%02X%02X%02X"(a, r, g, b);
	}

	void opAssign (Color color) {c = color.c;}

	void opAssign (uint color) {c = color;}

	uint rgb () const {return c & 0xffffff;}

	ubyte red () const {return r;}
	ubyte green () const {return g;}
	ubyte blue () const {return b;}
	ubyte alpha () const {return a;}

	Color dark (double factor=0.25)
	{
		factor = min (1.0, (max (0, factor)));

		double r = red/255.0   * (1.0-factor);
		double g = green/255.0 * (1.0-factor);
		double b = blue/255.0  * (1.0-factor);

		return Color (r, g, b);
	}

	Color light (double factor=0.25)
	{
		factor = min (1.0, (max (0, factor)));

		double dr = red/255.0;
		double dg = green/255.0;
		double db = blue/255.0;

		double r = dr + (1.0-dr)*factor;
		double g = dg + (1.0-dg)*factor;
		double b = db + (1.0-db)*factor;

		return Color (r, g, b);
	}

	Color mix (Color other, double other_factor=0.5)
	{
		other_factor = min (1.0, (max (0, other_factor)));
		double this_factor = 1.0 - other_factor;

		double r = other.red/255.0   * other_factor + red/255.0   * this_factor;
		double g = other.green/255.0 * other_factor + green/255.0 * this_factor;
		double b = other.blue/255.0  * other_factor + blue/255.0  * this_factor;

		return Color (r, g, b);
	}

	static Color White()   {return Color(255,255,255);}
	static Color Grey()    {return Color(128,128,128);}
	static Color Black()   {return Color(0,0,0);}

	static Color Red()     {return Color(255,0,0);}
	static Color Lime()    {return Color(0,255,0);}
	static Color Blue()    {return Color(0,0,255);}

	static Color Maroon()  {return Color(128,0,0);}
	static Color Green()   {return Color(0,128,0);}
	static Color Navi()    {return Color(0,0,128);}

	static Color Magenta() {return Color(255,0,255);}
	static Color Cyan()    {return Color(0,255,255);}
	static Color Yellow()  {return Color(255,255,0);}

	static Color Purple()  {return Color(128,0,128);}
	static Color Teal()    {return Color(0,128,128);}
	static Color Olive()   {return Color(128,128,0);}
}

uint int_from_xchar (char c)
{
	uint b = (cast (uint) c);

	if      (b >= 97) b -= 87;
	else if (b >= 65) b -= 55;
	else              b -= 48;

	if (b > 15) throw new Exception ("not a hex digit");

	return b;
}


uint argb_from_string (const (char)[] str)
{
	const (char)[]  s = str;
	uint a=255;
	uint r, g, b;

	try
	{
		enforce (s.length >= 6);

		if (s[0] == '#') s = s[1..$];

		if (s.length == 8)
		{
			a = (int_from_xchar(s[0]) << 4) + int_from_xchar(s[1]);
			s = s[2..$];
		}

		enforce (s.length == 6);

		r = int_from_xchar (s[0])*16 + int_from_xchar (s[1]);
		g = int_from_xchar (s[2])*16 + int_from_xchar (s[3]);
		b = int_from_xchar (s[4])*16 + int_from_xchar (s[5]);

	}
	catch (Throwable)
	{
		throw new Exception (str.idup ~ " does not describe a color");
	}
	return (a<<24) + (r<<16) + (g<<8) + b;
}
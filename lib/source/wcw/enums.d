module wcw.enums;

/** button types

**/
enum MouseButton
{
	NONE           = 0,
	LEFT           = 1, BUTTON1 = 1,
	MIDDLE         = 2, BUTTON2 = 2,
	RIGHT          = 3, BUTTON3 = 3,
	SCROLL_FORWARD = 4, BUTTON4 = 4,
	SCROLL_REVERSE = 5, BUTTON5 = 5,
	TILT_LEFT      = 6, BUTTON6 = 6,
	TILT_RIGHT     = 7, BUTTON7 = 7,
}

enum SelectionType
{
	CLIPBOARD = 0,
	PRIMARY   = 1
}

enum GradientType
{
    Flat       = 0x000001,
	Linear     = 0x000002,
	Cylinder   = 0x000003,
	Horizontal = 0x000100,
	Vertical   = 0x000200
};

enum EventType
{
	INVALID, CLOSE, CLOCK, MOUSEMOVE, BUTTONPRESSED, BUTTONRELEASED,
	KEYPRESSED, KEYRELEASED, TRANSFORM, REDRAW, MOUSEIN, MOUSEOUT,
	FOCUSIN, FOCUSOUT, SHOW,
//	SELECTION,
//	RESIZE, EXPOSE, CONFIGURE, REPARENT, MAP,
//	UNMAP, VISIBILITY, HIDE
}

enum WindowRole
{
	NONE,
	WINDOW,
	MENU,
	DIALOG
}

enum WindowAttribute
{
	NONE,
	UNMANAGED = 1<<0,	// unmanaged by the window manager: no frame and no automatoc placement by wm
	TRANSIENT = 1<<1,	// window will be always above parent
	MODAL     = 1<<2,	// window will grab focus from parent
	ABOVE     = 1<<3,	// window should be above all others
	BELOW     = 1<<4,	// window should be below all others
	STICKY    = 1<<5,	// window should stay where it is on screen when desktop moves
}

/** widget states. Values are:
	PASSIVE (widget is not reacting to input)
	ACTIVE (normal state)
	HILIGHT (when mouse hovers over the widget)
	SELECT (when a widget or button has the input focus)
**/
enum State
{
	PASSIVE,
	ACTIVE,
	HILIGHT,
	SELECT
};

/** How to align a widget.
	Values are NONE, LEFT, RIGHT, UP, DOWN.
**/
enum Align
{
	NONE = 0, DEFAULT = 0, CENTER = 0x33,
	LEFT = 0x01, RIGHT  = 0x02, HCENTER = 0x03, HMASK = 0x0f,
	TOP  = 0x10, BOTTOM = 0x20, VCENTER = 0x30, VMASK = 0xf0,
}

/** How to expand a widget.
	Values are NONE, VERTICAL, HORIZONTAL
**/
enum Expand
{
	NONE       = 0,
	VERTICAL   = 1<<0,
	HORIZONTAL = 1<<1
}

enum ButtonRole
{
	ACCEPT = 1,
	CANCEL
}

enum DialogResult
{
	NONE = -1,		// Dialog still open
	CLOSED,			// close button in window frame pressed (usually not used)
	YES,			// button pressed
	NO,
	OK,
	ACCEPT,
	DENY,
	CANCEL
}

/** key modifiers

    they coincidally resemble X11's key and button masks ☺
**/
enum ModifierKey
{
	NONE        = 0,
	SHIFT       = 1<<0,
	CAPSLOCK    = 1<<1,
	CONTROL     = 1<<2,
	ALT         = 1<<3,  MOD1     = 1<<3,
	NUMLOCK     = 1<<4,  MOD2     = 1<<4,
	                     MOD3     = 1<<5,
	WINDOWS     = 1<<6,  MOD4     = 1<<6,
	ALTGR       = 1<<7,  MOD5     = 1<<7,

    LEFT        = 1<<8,  BUTTON1  = 1<<8,
	MIDDLE      = 1<<9,  BUTTON2  = 1<<9,
	RIGHT       = 1<<10, BUTTON3  = 1<<10,
	                     BUTTON4  = 1<<11,
	                     BUTTON5  = 1<<12,
    KEYMASK     = 0xFF,
    BUTTONMASK  = 0xFF00,
}

/** key symbols

    they coincidally resemble X11's KeySymbols ☺
**/
enum KeySymbol
{
  NONE          = 0,
  BACKSPACE     = 0xff08,
  TAB           = 0xff09,
  LINEFEED      = 0xff0a,
  CLEAR         = 0xff0b,
  RETURN        = 0xff0d,
  PAUSE         = 0xff13,
  SCROLL_LOCK   = 0xff14,
  SYS_REQ       = 0xff15,
  ESCAPE        = 0xff1b,
  DELETE        = 0xffff,

  HOME          = 0xff50,
  LEFT          = 0xff51,
  UP            = 0xff52,
  RIGHT         = 0xff53,
  DOWN          = 0xff54,
  PRIOR         = 0xff55,
  PAGE_UP       = 0xff55,
  NEXT          = 0xff56,
  PAGE_DOWN     = 0xff56,
  END           = 0xff57,
  BEGIN         = 0xff58,

  SELECT        = 0xff60,
  PRINT         = 0xff61,
  EXECUTE       = 0xff62,
  INSERT        = 0xff63,
  UNDO          = 0xff65,
  REDO          = 0xff66,
  MENU          = 0xff67,
  FIND          = 0xff68,
  CANCEL        = 0xff69,
  HELP          = 0xff6a,
  BREAK         = 0xff6b,
  MODE_SWITCH   = 0xff7e,
  NUM_LOCK      = 0xff7f,

  KP_TAB        = 0xff89,
  KP_ENTER      = 0xff8d,
  KP_F1         = 0xff91,
  KP_F2         = 0xff92,
  KP_F3         = 0xff93,
  KP_F4         = 0xff94,
  KP_HOME       = 0xff95,
  KP_LEFT       = 0xff96,
  KP_UP         = 0xff97,
  KP_RIGHT      = 0xff98,
  KP_DOWN       = 0xff99,
  KP_PRIOR      = 0xff9a,
  KP_PAGE_UP    = 0xff9a,
  KP_NEXT       = 0xff9b,
  KP_PAGE_DOWN  = 0xff9b,
  KP_END        = 0xff9c,
  KP_BEGIN      = 0xff9d,
  KP_INSERT     = 0xff9e,
  KP_DELETE     = 0xff9f,

  F1            = 0xffbe,
  F2            = 0xffbf,
  F3            = 0xffc0,
  F4            = 0xffc1,
  F5            = 0xffc2,
  F6            = 0xffc3,
  F7            = 0xffc4,
  F8            = 0xffc5,
  F9            = 0xffc6,
  F10           = 0xffc7,
  F11           = 0xffc8,
  F12           = 0xffc9,

  SHIFT_L       = 0xffe1,
  SHIFT_R       = 0xffe2,
  CONTROL_L     = 0xffe3,
  CONTROL_R     = 0xffe4,
  CAPS_LOCK     = 0xffe5,
  SHIFT_LOCK    = 0xffe6,

  META_L        = 0xffe7,
  META_R        = 0xffe8,
  ALT_L         = 0xffe9,
  ALT_R         = 0xffea,
  SUPER_L       = 0xffeb,
  SUPER_R       = 0xffec,
  HYPER_L       = 0xffed,
  HYPER_R       = 0xffee,

  ALT_GR        = 0xfe03	// in X11: XK_ISO_Level3_Shift
}

module wcw.font;

import std.format;
import std.string;
import std.conv;
import std.utf;

import fontconfig;
import cairo;

import wcw.application;
import wcw.auxiliary;

/// opens a font scaled to the display resolution

class Font
{
private:
	string _family;
	string _style;
	string _path;
	double _size;
	CairoFontFace _fontFace;
	CairoScaledFont _scaledFont;

public:
	/// internal font variables
	string family () const {return _family;}
	/// ditto
	string style () const {return _style;}
	/// ditto
	string path () const {return _path;}
	/// ditto
	double size () const {return _size;}
	/// ditto
	inout(CairoScaledFont) scaledFont () inout {return _scaledFont;}

	/** find the best matching font for family and style
		and open it with size scaled to the current display resolution
		Examples:
		auto font = new Font ("Liberation Sans", "Regular", 10.0)
	**/
	this (in istring family, in istring style, in float size)
	{
		FcResult result;
		FcPattern *pattern = null;

		pattern = FcPatternCreate ();
		if (family.length > 0) pattern = FcPatternBuild (pattern, FC_FAMILY.toStringz, FcType.FcTypeString, family.toStringz, null);
		if (style.length > 0)  pattern = FcPatternBuild (pattern, FC_STYLE.toStringz, FcType.FcTypeString, style.toStringz, null);
		FcConfigSubstitute (null, pattern, FcMatchKind.FcMatchPattern);
		FcDefaultSubstitute (pattern);

		FcPattern *match = FcFontMatch (null, pattern, &result);

		char *s1 = FcPatternFormat (match, "%{family}");
		char *s2 = FcPatternFormat (match, "%{style}");
		char *s3 = FcPatternFormat (match, "%{file}");

		_family = fromStringz(s1).idup;
		_style  = fromStringz(s2).idup;
		_path   = fromStringz(s3).idup;
		_size   = size;

		FcStrFree (s1);
		FcStrFree (s2);
		FcStrFree (s3);

		_fontFace = new CairoFtFontFace (match);

		FcPatternDestroy (match);
		FcPatternDestroy (pattern);

		// scale the font to display resolution
		auto options = new CairoFontOptions;
		auto fu = CairoMatrix (_size*App.resolution.x/72.0, 0.0, 0.0, _size*App.resolution.y/72.0, 0.0, 0.0);
		// cairo seems to do weird things with this matrix:
		// so, what is it good for?
		auto ud = CairoMatrix (1.0, 0.0, 0.0, 1.0, 0.0, 0.0);
		_scaledFont = new CairoScaledFont (_fontFace, fu, ud, options);
	}

	/** find the best matching font using a description istring in the format
		"Family:Style:Size", scaled to the current display resolution
		Examples:
		auto font = new Font ("Liberation Sans:Regular:10");
	**/
	this (in istring description)
	{
		string[] parts = description.split(":");

		if (parts.length <= 0) parts ~= "Sans";
		if (parts.length <= 1) parts ~= "Regular";
		if (parts.length <= 2) parts ~= "10";

		this(parts[0], parts[1], to!float(parts[2]));
	}

	/// font description as string
	override istring toString () const
	{
		return format("%s:%s:%s", _family, _style, _size);
	}

	CairoFontExtents fontExtents ()
	{
		return _scaledFont.extents;
	}

	CairoTextExtents textExtents (cstring text)
	{
		return _scaledFont.text_extents (text);
	}
}

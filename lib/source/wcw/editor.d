module wcw.editor;


import std.signals;
import std.string;
import std.conv;
import std.utf;
import std.utf: utf8_count = count;
import std.array;
import std.algorithm;
import std.math;
import std.stdio;
import std.typecons;
import std.exception;

import core.stdc.string;
import core.stdc.stdio;

import cairo;

import wcw.auxiliary;
import wcw.application;
import wcw.design;
import wcw.color;
import wcw.widget;
import wcw.cairo;
import wcw.event;
import wcw.font;
import wcw.image;
import wcw.layout;
import wcw.enums;
import wcw.clipboard;

/* Unicode Begriffe

Code Unit:
	8 bit für UTF8,
	16 bit für UTF16 und
	32 bit für UTF32

Code Point:
	der Character Code für ein Unicode-Zeichen,
	z.B.:
	Zeichen Code      UTF8 (hex)     UTF8 (dec)         UTF16(BE)    UTF32(BE)
	'A' 	U+0041    41             65                 0041         00000041
	'Ω' 	U+03A9    CE A9          206 169            03A9         000003A9
	'😃'	U+1F603   F0 9F 98 83    240 159 152 131    D83D DE03    0001F603

Grapheme:
	Ein komplettes Zeichen, das auch aus mehreren Zeichen zusammengesetzt sein kann
	z.B.	Code Points      UTF8
	'â'		U+00E2           C3 A2
	'â'     U+0061,U+0302    61, CC 82

Glyph:
	graphische Repräsentation eines Zeichens
*/

/*------------------------------------------------------------------------------------------------*/
/*                                                                                                */
/*                                                                                                */
/*	Editor, 2. Versuch                                                                            */
/*                                                                                                */
/*                                                                                                */
/*------------------------------------------------------------------------------------------------*/


class LineEditor : SimpleEditor
{
	this (Widget parent, int[] i...)
	{
		this ("", parent, i);
	}

	this (cstring txt, Widget parent, int[] i...)
	{
		_singleline = true;
		super (txt, parent, i);
	}
}

struct Line
{
	Text *_textptr;

	mstring _chars;
	alias _chars this;

	CairoGlyph[] _glyphs;
	double[] _glyph_widths;

	struct Wrap
	{
		long bi;
		long gi;
		double x;
	}
	Wrap [] _wraps;

	long rows () const {return _wraps.length+1;}

	long row (long y)
	{
		auto i = _chars[0..y].utf8_count();
		return cast (long) (glyphY(i)/_textptr.lineHeight);
	}

	long startColumnOfRow (long r)
	{
		if (r > _wraps.length) r = _wraps.length;
		if (r <= 0) return 0;
		else        return _wraps[r-1].bi;
	}

	int _border=0;					// column at which wrapping occurs

	bool _prepared = false;
	CairoTextExtents _textex;
	CairoFontExtents _fontex;

	this (ref Text text, mstring str)
	{
		_textptr = &text;
		_chars = str;
	}

	this (ref Text text, cstring str)
	{
		_textptr = &text;
		_chars = str.dup;
	}

	void insert (long pos, cstring str)
	{
		insertInPlace (_chars, pos, str);
		delete_glyphs ();
	}

	void remove (long from, long to=long.max)
	{
		long length = _chars.length;

		if (from > to) swap (from, to);
		if (from > length) from = length;
		if (to   > length) to = length;
		if (from == to) return;

		_chars = _chars[0..from]~_chars[to..$];
		delete_glyphs ();

	}


	// prepare line for drawing: initialize glyphs and perform wrapping
	void prepare (CairoContext context, int col)
	{
		if (!_prepared)
		{
			context.save();
			context.identity_matrix ();
			auto font = context.get_scaled_font();
			_textex = font.text_extents (_chars);
			_fontex = font.extents ();
			_glyphs = font.text_to_glyphs (0, 0, _chars);
			_glyph_widths.length = _glyphs.length;
			// no wrapping yet, so _glyph.x values are still unmodified: quickly save the glyph widths
			// into a separate array
			auto x1 = _textex.x_advance;
			foreach_reverse (i, g; _glyphs)
			{
				auto x0 = _glyphs[i].x;
				_glyph_widths[i] = x1-x0;
				x1 = x0;
			}

			context.restore ();
			_prepared = true;
		}

		wrap (col);
	}

	// make sure, that a font has been allocated for context
	private void wrap (int col)
	{
		assert (_prepared);

		if (col == _border) return;
		unwrap();
		if (col == 0) return;

		_border = col;

		double x=0;
		double y=0;
		long bi=0;

		foreach  (gi, ref glyph; _glyphs)
		{
			if (glyph.x+glyphWidth(gi)-x >= col)
			{
				x = glyph.x;
				y += _textptr.lineHeight;

				_wraps ~= Wrap(bi, gi, x);
			}
			glyph.x -= x;
			glyph.y = y;

			bi += stride(_chars,bi);
		}
	}

	private void delete_glyphs ()
	{
		_wraps.length = 0;
		_glyphs.length = 0;
		_glyph_widths.length = 0;
		_border = 0;

		_prepared = false;
	}

	private void unwrap ()
	{
		long w=0;
		while (w<_wraps.length)
		{
			long i = _wraps[w].gi;
			long j = w+1 < _wraps.length ? _wraps[w+1].gi : _glyphs.length;

			while (i<j)
			{
				_glyphs[i].x += _wraps[w].x;
				_glyphs[i].y = 0;
				++i;
			}
			++w;
		}

		_wraps.length = 0;
		_border = 0;
	}

	// paints the line at 0,0 as it's upper left corner
	// this is different from the combination of textToGlyphs + showGlyphs(),
	// which paints text with it's baseline at 0,0
	void paint (CairoContext context, long from, long to, Color bgnd, Color fgnd)
	{
		assert (from <= to, "left marker right of right marker");
		assert (from <= _chars.length, "left marker outside text bounds");
		assert (to   <= _chars.length, "right marker outside text bounds");
		assert (_prepared);

		if (from == to) return;

		auto m = context.get_matrix();
		long g0 = _chars[0..from].utf8_count;
		long g1 = _chars[0..to].utf8_count;

		{
			context.set_source_color (bgnd);

			for (auto i=g0; i<g1; ++i)
			{
				context.rectangle (glyphX(i), glyphY(i), glyphWidth(i), glyphHeight());
				context.fill();
			}
		}

		context.set_source_color (fgnd);
		context.translate (0, _textptr.ascent);
		context.show_glyphs (_glyphs[g0..g1]);
		context.set_matrix(m);
	}

	// returns index of corresponding glyph for _chars[i]
	long glyphIndex (long i)
	{
		assert (i <= _chars.length, "index outside text bounds");
		assert (_prepared);

		return _chars[0..i].utf8_count;
	}

	// returns position of glyph; if index is out of bounds, returns the imaginary position
	// after the last character;
	// i is the index of the character's glyph
	double glyphX (long i)
	{
		assert (_prepared);
		if (i<_glyphs.length)        return  (_glyphs[i].x);
		else if (_glyphs.length > 0) return (_glyphs[$-1].x + _glyph_widths[$-1]);
		else                         return 0;
	}

	// returns the y position of the character; when the line is not wrapped, this is always 0
	// i is the index of the character's glyph
	double glyphY (long i)
	{
		assert (_prepared);
		if (i<_glyphs.length)      return (_glyphs[i].y);
		else if (_glyphs.length>0) return (_glyphs[$-1].y);
		else                       return 0;
	}

	//  returns glyph width; this information comes from an internally allocated array
	// i is the index of the character's glyph
	double glyphWidth (long i)
	{
		assert (_prepared);
		if (i<_glyphs.length) return (_glyph_widths[i]);
		else                  return (0);
	}

	// since all characters have the same height, this returns the font's height
	// i is the index of the character's glyph
	double glyphHeight ()
	{
		assert (_prepared);
		return _textptr._fontExtents.height;
	}
}

struct Text
{
	Line[] _lines;
	alias _lines this;

	CairoFontExtents _fontExtents;

	double lineHeight () const {return _fontExtents.height;}
	double ascent () const {return _fontExtents.ascent;}

	long lineRow  (LPair csr) {return _lines[csr.line].row(csr.column);}
	long lineRows (long ln) {return _lines[ln].rows;}
	long lineLength (long ln) {return _lines[ln].length;}

	long linesCount () {return _lines.length;}

	void newLine (long ln) {insertInPlace (_lines, ln, Line(this, ""));}

	void remove (LPair from, LPair to)
	{
		if (from > to) swap (from, to);

		assert (from >= LPair(0,0));
		assert (to   <= LPair(_lines.length-1, _lines[$-1].length));

		if (from.line == to.line)
		{
			_lines[from.line].remove (from.column, to.column);
		}
		else
		{
			// combine first and last line
			_lines[from.line]._chars = _lines[from.line][0..from.column] ~
			                           _lines[to.line][to.column..$];
			_lines[from.line].delete_glyphs();

			// delete all lines except the first
			_lines = _lines[0..from.line+1] ~ _lines[to.line+1..$];
		}
	}
}

class SimpleEditor : Widget
{
protected:
	LPair homeCursor () const {return LPair (0, 0);}
	LPair endCursor () const {return LPair (_text.length-1, _text[$-1].length);}

	Palette _textPalette;
	Color   _selectColor;
	Font    _textFont;

	bool _overwrite;


	Text _text;

	bool _singleline;			// just a line input?
	LPair _cursor;				// current line/column (x is the byte index in line y)
	LPair _marker;				// selection start/end (x is the byte index in line y)

	LPair  _top;
	double _left;

	bool _selectionMode;		// true if selection is active

	Rect!double _cursorRect;		// position in pixels of cursor relative to text
	CairoSurface _cursorOn;		// shape of on cursor
	CairoSurface _cursorOff;	// shape of off cursor
	int   _countCursor;				// internal tick counter; will reset every time it reaches 8
	bool  _showCursor;
	bool _wrapping;

	struct KeyAction
	{
		int  modifier;
		int  symbol;
		void delegate () func;
	}
	KeyAction[] _actions;

public:
	// signals
	mixin Signal!() returnPressed;
	mixin Signal!() escapePressed;
	mixin Signal!() textChanged;

	const LPair  cursor ()     {return _cursor;}
	const long   lineNumber () {return _cursor.line;}
	const long   lineCount ()  {return _text.length;}
	const double lineHeight () {return _text.lineHeight;}
	bool onLastLine () {return lineNumber == lineCount-1;}

	this (cstring txt, Widget parent, int[] i...)
	{
		this (parent, i);
		insertString (txt);
		_cursor = LPair (0, 0);
	}

	this (Widget parent, int[] i...)
	{
		super (parent, i);

		setFocus(true);

		enum NONE        = ModifierKey.NONE;
		enum SHIFT       = ModifierKey.SHIFT;
		enum CTRL        = ModifierKey.CONTROL;
		enum SHIFT_CTRL  = ModifierKey.SHIFT|ModifierKey.CONTROL;

		_actions ~= KeyAction (NONE,  KeySymbol.LEFT,      {moveLeft(false);});
		_actions ~= KeyAction (NONE,  KeySymbol.RIGHT,     {moveRight(false);});
		_actions ~= KeyAction (NONE,  KeySymbol.UP,        {moveUp(false);});
		_actions ~= KeyAction (NONE,  KeySymbol.DOWN,      {moveDown(false);});
		_actions ~= KeyAction (NONE,  KeySymbol.HOME,      {moveFarLeft(false);});
		_actions ~= KeyAction (NONE,  KeySymbol.END,       {moveFarRight(false);});
		_actions ~= KeyAction (NONE,  KeySymbol.PAGE_UP,   {movePageUp(false);});
		_actions ~= KeyAction (NONE,  KeySymbol.PAGE_DOWN, {movePageDown(false);});
		_actions ~= KeyAction (NONE,  KeySymbol.RETURN,    {typeString("\n");});
		_actions ~= KeyAction (NONE,  KeySymbol.KP_ENTER,  {typeString("\n");});
		_actions ~= KeyAction (NONE,  KeySymbol.INSERT,    {setOverwrite (!_overwrite);});
		_actions ~= KeyAction (NONE,  KeySymbol.ESCAPE,    {escapePressed.emit();});

		_actions ~= KeyAction (NONE,  KeySymbol.BACKSPACE, {deleteChar(true);});
		_actions ~= KeyAction (NONE,  KeySymbol.DELETE,    {deleteChar(false);});

		_actions ~= KeyAction (SHIFT, KeySymbol.LEFT,      {moveLeft(true);});
		_actions ~= KeyAction (SHIFT, KeySymbol.RIGHT,     {moveRight(true);});
		_actions ~= KeyAction (SHIFT, KeySymbol.UP,        {moveUp(true);});
		_actions ~= KeyAction (SHIFT, KeySymbol.DOWN,      {moveDown(true);});
		_actions ~= KeyAction (SHIFT, KeySymbol.HOME,      {moveFarLeft(true);});
		_actions ~= KeyAction (SHIFT, KeySymbol.END,       {moveFarRight(true);});
		_actions ~= KeyAction (SHIFT, KeySymbol.PAGE_UP,   {movePageUp(true);});
		_actions ~= KeyAction (SHIFT, KeySymbol.PAGE_DOWN, {movePageDown(true);});
		_actions ~= KeyAction (SHIFT, KeySymbol.RETURN,    {typeString("\n");});
		_actions ~= KeyAction (SHIFT, KeySymbol.KP_ENTER,  {typeString("\n");});

		_actions ~= KeyAction (CTRL,  KeySymbol.HOME, {moveStart(false);});
		_actions ~= KeyAction (CTRL,  KeySymbol.END,  {moveEnd(false);});

		_actions ~= KeyAction (CTRL,  'x', &cutToClipboard);
		_actions ~= KeyAction (CTRL,  'c', &copyToClipboard);
		_actions ~= KeyAction (CTRL,  'v', &pasteFromClipboard);

		_actions ~= KeyAction (SHIFT_CTRL, KeySymbol.HOME, {moveStart(true);});
		_actions ~= KeyAction (SHIFT_CTRL, KeySymbol.END,  {moveEnd(true);});

		// for testing only
		_actions ~= KeyAction (CTRL, 'w',  {setWrapping (!_wrapping);});

		Palette cs;
		cs.passive = App.design.editPalette.passive;
		cs.active  = App.design.editPalette.active;
		cs.hilight = cs.active;
		cs.select  = cs.active;

		setWidgetPalette (cs);
		setEdgeRadius (0);
		setFrameWidth (App.design.textFrame);
		setMargin     (App.design.textMargin);

		_textPalette  = App.design.textPalette;
		_selectColor  = App.design.editPalette.select;
		_textFont     = App.design.font;

		_text._fontExtents = _textFont.fontExtents;

		_left   = 0; // is a double

		_text ~= Line (_text, "");	// one empty line

		// set the sizes
		if (_singleline)
		{
			int h = to!int (lineHeight + 2*margin);
			int w = em(10);
			setFixSize (IPair (w, h));
			setExtSize (IPair(-1, 0));
		}
		else
		{
			int hmin = 1;		// 1, 10 or 1000 lines
			int hnom = 10;
			int hmax = 1000;

			hmin = to!int (hmin*lineHeight + 2*margin);
			hnom = to!int (hnom*lineHeight + 2*margin);
			hmax = to!int (min (maxWidgetHeight(), hmax*lineHeight + 2*margin));

			setMinSize (IPair(em(10), hmin));
			setNomSize (IPair(em(10), hnom));
			setMaxSize (IPair(maxWidgetWidth, hmax));
		}
	}

	~this ()
	{
		destroy (_cursorOn);  _cursorOn = null;
		destroy (_cursorOff); _cursorOff = null;
	}

	override void addFrame () {setFrameWidth (App.design.textFrame);}

	void addAction (KeyAction a) {_actions ~= a;}

	LPair walkRows (LPair csr, long walk)
	{
		long l = csr.line;
		long r;

		if (walk > 0)
		{
			r = walk - (_text.lineRows(csr.line)-1-_text.lineRow(csr));

			while (r > 0)
			{
				if (++l >= _text.length)
				{
					l = _text.length-1;
					r = 0;
					break;
				}
				r -= _text.lineRows(l);
			}
			r += _text.lineRows(l)-1;
		}
		else
		{
			r = walk + _text.lineRow(csr);

			while (r < 0)
			{
				if (--l < 0)
				{
					l = 0;
					r = 0;
					break;
				}
				r += _text.lineRows(l);
			}
		}
		return LPair (l, _text[l].startColumnOfRow (r));
	}

	private void scroll (int wrap)
	{
		int sy = margin;								// screen upper edge
		int sh = size.height - 2*margin;				// screen height
		int pr = to!int (ceil(sh / lineHeight));		// count of partially visible text rows
		int fr = to!int (floor(sh / lineHeight));		// count of fully visible text rows

		auto br = min (max ((fr/2)-1, 0), 5);

		LPair upper = walkRows (_top, br);
		LPair lower = walkRows (_top, fr-br-1);

		auto r = _text.lineRow(_cursor);

		if (_cursor.line < upper.line ||
		    _cursor.line == upper.line && cursor.column < upper.column)
		{
			LPair up = walkRows (_cursor, -br);
			_top = up;
		}

		if (_cursor.line > lower.line ||
		    _cursor.line == lower.line && _cursor.column > lower.column)
		{
			LPair dn = walkRows (_cursor, br);
			LPair nt = walkRows (dn, 1-fr);
			_top = nt;
		}

		int sx = margin;						// screen left edge
		int sw = size.width - 2*margin;		// screen width
		auto ln = _text[_cursor.line];
		auto gi = ln.glyphIndex (_cursor.column);
		auto cx0 = ln.glyphX (gi);
		auto cx1 = cx0 + max(ln.glyphWidth (gi), em(0.5));

		auto bw = min(em(5), sw/5);

		if (cx0 < _left+bw)
		{
			_left = max(0, floor(cx0/bw)-1) * bw;
		}
		else if (cx1 > _left+sw-bw && wrap==false)	// wrapping menas, that text always fits to screen
		{
			_left = ((ceil(cx1/bw)+1) * bw) - sw;
		}
	}

	override void paint (CairoContext ctx)
	{
		Widget.paint(ctx);

		// make cursor invisible; new values will be calculated later
		_cursorRect = Rect!double (DPair(0, 0), 0, 0);

		// for the text we create a new canvas that is a little bit smaller than
		// the widget's drawing area
		int sx = margin;
		int sw = size.width - 2*margin;
		int sy = margin;
		int sh = size.height - 2*margin;

		auto surface = ctx.get_target;	// tris creates a new object; need to destroy it!
		scope (exit) destroy (surface);

		auto canvas  = CairoSurface.create_for_rectangle (surface, sx, sy, sw, sh);
		scope (exit) destroy (canvas);

		auto context = new CairoContext (canvas);
		scope (exit) destroy (context);

		context.set_line_width (1.0);
		context.set_line_cap (CAIRO_LINE_CAP_SQUARE);
		context.set_scaled_font (_textFont.scaledFont);

		// prepare  lines
		// for now, the whole text is prepared, could be optimized to prepare only text
		// that's needed for scrolling and drawing
		auto w = _wrapping ? sw : 0;
		foreach (ref l; _text) l.prepare (context, w);

		// adjust upper left
		scroll (w);

		// selected area is between position and selection
		LPair from = _cursor;
		LPair to   = _marker;
		if (from > to) swap (from, to);

		double ox = -_left;
		double oy = -(_text.lineRow(_top)*lineHeight);

		auto matrix = CairoMatrix (1.0, 0.0, 0.0, 1.0, ox, oy);
		context.set_matrix (matrix);

		for (long l = _top.line; l < _text.length; l++)
		{
			long m0, m1;

			if (_selectionMode)
			{
				if      (l == from.line && l == to.line) {m0 = from.column; m1 = to.column;}
				else if (l == from.line)                 {m0 = from.column; m1 = _text[l].length;}
				else if (l == to.line)                   {m0 = 0;           m1 = to.column;}
				else if (from.line < l && l < to.line)   {m0 = 0;           m1 = _text[l].length;}
			}

			paintLine (context, l, m0, m1);
			auto ls = lineHeight*(_text.lineRows(l));
			oy += ls;
			context.translate (0, ls);
			if (oy > sh) break;
		}

		paintCursor (ctx);
	} // paint

	// paint <line> with selection between <from> and <to>
	private void paintLine (CairoContext context, long ln, long m0, long m1)
	{
		// marker set?
		if (_selectionMode)
		{
			_text[ln].paint (context, 0, m0, currentWidgetColor (), _textPalette.active);
			_text[ln].paint (context, m1, _text[ln].length, currentWidgetColor (), _textPalette.active);
			_text[ln].paint (context, m0, m1, _selectColor, _textPalette.select);
		}
		else
		{
			_text[ln].paint (context, 0, _text[ln].length, currentWidgetColor (), _textPalette.active);
		}

		if (ln == _cursor.line)
		{
			createCursor (context);
		}
	}


	// assumes, that the current device matrix has ist's origin at the upper left corner of the
	// line the cursor points to
	private void createCursor (CairoContext context)
	{
		auto matrix = context.get_matrix;

		auto ln = _text[_cursor.line];

		auto gi = ln.glyphIndex (_cursor.column);
		auto x = ln.glyphX (gi);
		auto y = ln.glyphY (gi);
		auto w = ln.glyphWidth (gi);
		auto h = ln.glyphHeight ();

		_cursorRect.x0     = matrix.x0+x;
		_cursorRect.y0     = matrix.y0+y;
		_cursorRect.width  = max (em(0.5), w);	// with of overwrite cursor not narrower than 1/2 em
		_cursorRect.height = h;

		destroy (_cursorOn);
		destroy (_cursorOff);
		_cursorOn = _cursorOff = null;

		auto surface = context.get_target;
		scope (exit) destroy (surface);

		_cursorOff = CairoSurface.create_similar (surface,
			CAIRO_CONTENT_COLOR,
			cast(int)_cursorRect.width,
			cast(int)_cursorRect.height);

		_cursorOn = CairoSurface.create_similar (surface,
			CAIRO_CONTENT_COLOR,
			cast(int)_cursorRect.width,
			cast(int)_cursorRect.height);

		auto coff = new CairoContext (_cursorOff);
		scope (exit) destroy (coff);

		auto con = new CairoContext (_cursorOn);
		scope (exit) destroy (con);

		coff.set_source_surface (surface, -_cursorRect.x0, -_cursorRect.y0);
		coff.paint ();

		con.set_source_surface (surface, -_cursorRect.x0, -_cursorRect.y0);
		con.paint ();

		auto color = App.design.cursorColor;
		con.set_source_rgb (color.red/255.0, color.green/255.0, color.blue/255.0);
		con.set_line_width (2.0);

		if (_overwrite)
		{
			con.move_to (0, lineHeight-1);
			con.line_to (_cursorRect.width, lineHeight-1);
		}
		else
		{
			con.move_to (1, 0);
			con.line_to (1, _cursorRect.height);
		}
		con.stroke ();
	}

	private void paintCursor (CairoContext context)
	{
		int sx = margin;
		int sw = size.width - 2*margin;
		int sy = margin;
		int sh = size.height - 2*margin;

		auto sf = context.get_target();
		scope (exit) destroy (sf);

		auto cs = CairoSurface.create_for_rectangle (sf, sx, sy, sw, sh);
		scope (exit) destroy (cs);

		auto cc = new CairoContext (cs);
		scope (exit) destroy (cc);

		if (_showCursor)
		{
			if (_cursorOn)
			{
				cc.set_source_surface (_cursorOn, _cursorRect.x0, _cursorRect.y0);
				cc.paint ();
			}
		}
		else
		{
			if (_cursorOff)
			{
				cc.set_source_surface (_cursorOff, _cursorRect.x0, _cursorRect.y0);
				cc.paint ();
			}
		}
	}

	override void clockHandler (in ClockEvent event)
	{
		if (isPassive()) return;

		if (++_countCursor >= 8)
		{
			// hard coded 800 ms (clock events occur every 100 ms, this is also hard coded :-(
			_countCursor = 0;
			_showCursor = !_showCursor;

			if (canDraw())
			{
				IPair w = mapToWindow (position);
				auto sf = CairoSurface.create_for_rectangle (window.surface, w.x, w.y, _size.width, _size.height);
				scope (exit) sf.destroy;
				auto ct = new CairoContext (sf);
				scope (exit) ct.destroy;
				paintCursor (ct);
			}
		}
	}

	override void keyPressHandler (in KeyPressEvent event)
	{
		if (event.text.length > 0)
		{
			typeString (event.text.dup);
		}
		else
		{
			foreach_reverse (s; _actions)
			{
				if (s.modifier == (event.modifier & (ModifierKey.SHIFT|ModifierKey.CONTROL)) &&
				    s.symbol   == event.symbol)
				{
					s.func();
					break;
				}
			}
		}
	}

	void setWrapping (bool on=true)
	{
		if (_wrapping == on) return;

		_wrapping = on;
		redraw();
	}

	// set or delete a marker; if a marker is already set (_selectionmode is true), do nothing
	void markText (bool mark)
	{
		if (mark == false)                _marker = LPair(0,0); // just a precaution
		else if (_selectionMode == false) _marker = _cursor;    // mode changes, set marker to cursor
		_selectionMode = mark;
	}

	void cutToNowhere ()
	{
		if (_selectionMode == false || _cursor == _marker) return;

		_text.remove (_cursor, _marker);
		_cursor = _cursor < _marker? _cursor : _marker;
		markText (false);
		redraw();
	}

	void cutToClipboard ()
	{
		if (_selectionMode == false || _cursor == _marker) return;

		copyToClipboard();
		cutToNowhere ();
	}

	void copyToClipboard ()
	{
		if (_selectionMode == false || _cursor == _marker) return;

		mstring temp;

		LPair from = _cursor;
		LPair to   = _marker;
		if (from > to) swap (from, to);
		assert (_cursor >= homeCursor);
		assert (_marker <= endCursor);

		if (from.line == to.line)
		{
			temp ~= _text[from.line][from.column..to.column];
		}
		else
		{
			temp ~= _text[from.line][from.column..$];

			for (long l=from.line+1; l<to.line; l++)
			{
				temp ~= "\n" ~ _text[l];
			}

			temp ~= "\n" ~ _text[to.line][0..to.column];
		}

		App.clipboard.writeText (temp, SelectionType.CLIPBOARD);
	}

	void pasteFromClipboard ()
	{
		insertString (App.clipboard.readText (SelectionType.CLIPBOARD));
	}

	void deleteChar (bool backspace=false)
	{
		if (_selectionMode == false)
		{
			if (backspace) moveLeft (true);
			else           moveRight (true);
		}
		cutToNowhere ();
		textChanged.emit();
		redraw();
	}

	void moveRight (bool mark=false)
	{
		markText (mark);

		if (_cursor.column < _text[_cursor.line].length)
		{
			_cursor.column = utf8_next (_text[cursor.line], cursor.column);
		}
		else if (_cursor.line < _text.length-1)
		{
			_cursor.column = 0;
			_cursor.line++;
		}
		redraw();
	}

	void moveLeft (bool mark=false)
	{
		markText (mark);

		if (_cursor.column > 0)
		{
			_cursor.column = utf8_prev (_text[_cursor.line], _cursor.column);
		}
		else if (_cursor.line > 0)
		{
			_cursor.line--;
			_cursor.column = _text[_cursor.line].length;
		}
		redraw();
	}

	void moveUp (bool mark=false)
	{
		if (cursor.line <= 0) return;

		markText (mark);
		long col = _text[_cursor.line][0.._cursor.column].utf8_count;
		_cursor.line--;
		_cursor.column=0;
		for (int i=0; i<col; i++) _cursor.column = utf8_next (_text[cursor.line]._chars, _cursor.column);
		redraw();
	}

	void moveDown (bool mark=false)
	{
		if (_cursor.line >= _text.length-1) return;

		markText (mark);
		long col = _text[_cursor.line][0.._cursor.column].utf8_count;
		_cursor.line++;
		_cursor.column=0;
		for (int i=0; i<col; i++) _cursor.column = utf8_next (_text[cursor.line]._chars, _cursor.column);
		redraw();
	}

	void moveStart (bool mark=false)
	{
		markText (mark);
		_cursor = homeCursor();
		redraw();
	}

	void moveEnd (bool mark=false)
	{
		markText (mark);
		_cursor = endCursor();
		redraw();
	}

	void moveFarLeft (bool mark=false)
	{
		markText (mark);
		_cursor.column = 0;
		redraw();
	}

	void moveFarRight (bool mark=false)
	{
		markText (mark);
		_cursor.column = _text.lineLength(_cursor.line);
		redraw();
	}

	void movePageUp (bool mark=false)
	{
		int sh = size.height - 2*margin;				// screen height
		int fr = to!int (floor(sh / lineHeight));		// count of fully visible text rows
		auto br = min (max ((fr/2)-1, 0), 5);

		markText (mark);
		long col = _text[_cursor.line][0.._cursor.column].utf8_count;
		_cursor = walkRows (_cursor, br-fr);
		for (int i=0; i<col; i++) _cursor.column = utf8_next (_text[cursor.line]._chars, _cursor.column);
		redraw();
	}

	void movePageDown (bool mark=false)
	{
		int sh = size.height - 2*margin;	// screen height
		int fr = cast (int) floor(sh / lineHeight);		// count of fully visible text rows
		auto br = min (max ((fr/2)-1, 0), 5);

		markText (mark);
		long col = _text[_cursor.line][0.._cursor.column].utf8_count;
		_cursor = walkRows (_cursor, fr-br);
		for (int i=0; i<col; i++) _cursor.column = utf8_next (_text[cursor.line]._chars, _cursor.column);
		redraw();
	}

	void setOverwrite (bool on=true)
	{
		_overwrite = !_overwrite;
		redraw();
	}

	void assertText ()
	{
		if (_text.length == 0)
		{
			_text ~= Line (_text, "");
			whereami ("added an empty line");
		}

		if (_cursor.line >= _text.length)
		{
			_cursor.line = _text.length-1;
			_cursor.column = _text[_cursor.line].length;
			whereami ("corrected cursor");
		}
		else if (_cursor.column > _text[_cursor.line].length)
		{
			_cursor.column = _text.lineLength (_cursor.line);
			whereami ("corrected cursor");

		}

		if (_marker.line >= _text.length)
		{
			_marker.line = _text.length-1;
			_marker.column = _text[_marker.line].length;
			whereami ("corrected marker");
		}
		else if (_marker.column > _text[_marker.line].length)
		{
			_marker.column = _text[_marker.line].length;
			whereami ("corrected marker");
		}
	}

	// internal only: called from buttonPressHandler
	// differs from insertString in assuming that the istring comes from a keyboard event, which
	// represents usually only one character. If that character is a newline, it is inserted
	// and additionally a returenPress signal is emitted. Otherwise, the character istring is
	// inserted at the current cursor position or when overwriting mode is active, instead of
	// the current character
	void typeString (cstring str)
	{
		if (str == "\n")
		{
			auto l = _cursor.line;
			insertString ("\n");
			returnPressed.emit();
		}
		else
		{
			if (_selectionMode == false && _overwrite == true)
			{
				markText (true);
				_cursor.column = utf8_next (_text[cursor.line], cursor.column);
			}
			insertString (str);
		}
	}

	void insertString (cstring str)
	{
		assertText();
		if (str == "") return;

		if (_selectionMode == true && _cursor != _marker)
		{
			_text.remove (_cursor, _marker);
			_cursor = _cursor < _marker? _cursor : _marker;
		}

		auto lines = lineSplitter!(Yes.keepTerminator) (str);	// keep the eol in each line

		foreach (ln; lines)
		{
			auto eol  = utf8_hasEOL (ln);		// true if eol
			auto stripped = ln[0..$-eol];		// line without eol


			if (eol && !_singleline)
			{

				// create a new line and move right sided text into it
				_text.newLine (_cursor.line+1);
				_text[cursor.line+1].insert  (0, _text[_cursor.line][_cursor.column..$]);

				// and remove that text from the old line
				_text[_cursor.line].remove (_cursor.column);
				_text[_cursor.line].insert (_cursor.column, stripped);
				_cursor.line+=1;
				_cursor.column=0;
			}
			else
			{
				_text[_cursor.line].insert (_cursor.column, stripped);
				_cursor.column += stripped.length;
			}

		}
		markText (false);
		textChanged.emit();
		redraw();
	}

	cstring getLine (long l)
	{
		if (0 <= l && l < _text.length) return _text[l].idup;
		else                            return "";
	}

	cstring currentLine ()
	{
		return getLine(lineNumber);
	}

}

/// test, if last char(s) are either \r, \n, \f, \v, \u2085, \u2028 or \u2029
/// if yes, returns the character length of the code point, else returns 0
/// the selection of return codes equals the codes that are used by std.strings.lineSplitter
int utf8_hasEOL (cstring l)
{
	// test if last char is \r, \n, \f or \v
	if (l.length < 1) return 0;
	if (l[$-1] == '\r' || l[$-1] == '\n' || l[$-1] == '\f' || l[$-1] == '\v') return 1;

	// test if last chars are \u2085
	if (l.length < 2) return 0;
	if (l[$-2] == 0xc2 && l[$-1] == 0x85) return 2;

	// test if last chars are \u2028 or \u2029
	if (l.length < 3) return 0;
	if (l[$-3] == 0xe2 && l[$-2] == 0x80 && ((l[$-1] == 0xa8) || (l[$-1] == 0xa9))) return 3;

	// all tests failed
	return 0;
}

long utf8_next (inout char[] str, long index)
{
	if (index < str.length) return index+stride(str,index);
	else                    return index;
}

long utf8_prev (inout char[] str, long index)
{
	if (index > 0) return index-strideBack(str,index);
	else           return 0;
}

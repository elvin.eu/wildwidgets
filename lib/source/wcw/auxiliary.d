module wcw.auxiliary;

import std.conv;
import std.stdio;
import std.string;
import std.typecons;
import std.algorithm;
import std.exception;
import std.math;
import std.traits;

import wcw.application;

public import std.string : format; // for whereami
import std.stdio: writefln;

// istring aliases
alias cstring=const(char)[];
alias istring=immutable(char)[];
alias mstring=char[];

int maxWidgetWidth()  {return 32767;}
int maxWidgetHeight() {return 32767;}

/** A template struct holding 2 values of type T.

	Internally, the values are stored in an array of length 2:
**/

struct Pair(T)
{
	T x;
	T y;

	/** Constructors

		the 3nd constructor takes a pointer to
		an address of an C-style array of 2 values of type P.
	    When the type P of pointer is not the same type T of the template class,
		conversion is performed using the to! template from std.conf

		the 4rd constructor takes a string of the format "(a, b)" where a, b
		are valid numbers of type T that can be converted by d's to!T routines
	**/

	this (Pair!T p)
	{
		x = p.x;
		y = p.y;
	}

	this (T x, T y)
	{
		this.x = x;
		this.y = y;
	}

	/// ditto
	this(P) (P* pointer)
	{
		x = to!T(pointer[0]);
		y = to!T(pointer[1]);
	}

	/// ditto
	this (string parameters)
	{
		try
		{
			if (parameters.length>=2 && parameters[0] == '(' && parameters[$-1] == ')')
			{
				string str = parameters[1..$-1];

				int b;
				foreach (i, letter; str)
				{
					if (letter == '(') b++;
					else if (letter == ')') b--;
					else if (letter == ',' && b==0)
					{
						x = to!T(str[0..i].strip);
						y = to!T(str[i+1..$].strip);

						return;
					}
				}
			}
		}
		catch (Throwable)
		{
			// bah, we failed
		}
		stderr.writefln ("Failed to initialize Pair(%s) from %s", typeid(T), parameters);
	}

	alias line=x;
	alias column=y;

	alias width =x;
	alias height=y;

	/** return a istring "(x, y)"
	**/
	string toString () const
	{
		return format ("(%s, %s)", x, y);
	}

	T opIndex (size_t index) inout
	{
		if      (index==0) return x;
		else if (index==1) return y;
		else               throw new Exception ("index [" ~ index.to!string ~ "] out of range");
	}

	T opIndexAssign (T value, size_t index)
	{
		if      (index == 0) return x = value;
		else if (index == 1) return y = value;
		else                 throw new Exception ("index [" ~ index.to!string ~ "] out of range");
	}



	/** the + - * and / operators
	**/
	Pair!T opBinary(string op) (Pair!T p) const
	if (op == "+")
	{
		return Pair!T (x+p.x, y+p.y);
	}

	/// ditto
	Pair!T opBinary(string op) (Pair!T p) const
	if (op == "-")
	{
		return Pair!T (x-p.x, y-p.y);
	}

	/// ditto
	Pair!T opBinary(string op) (T m) const
	if (op == "*")
	{
		return Pair!T (x*m, y*m);
	}

	/// ditto
	Pair!T opBinary(string op) (T m) const
	if (op == "/")
	{
		return Pair!T (x/m, y/m);
	}

	/// ditto
	Pair!T opBinaryRight(string op) (T m) const
	if (op == "*")
	{
		return Pair!T (x*m, y*m);
	}

	/// ditto
	Pair!T opBinaryRight(string op) (T m) const
	if (op == "/")
	{
		return Pair!T (m/x, m/y);
	}

	/** the = operator
	**/
	ref Pair!T opAssign (Pair!T p)
	{
		x = p.x;
		y = p.y;
		return this;
	}


	/** the += and -= operators
	**/
	ref Pair!T opOpAssign(string op) (Pair!T p) if (op == "+")
	{
		x += p.x;
		y += p.y;
		return this;
	}

	/// ditto
	ref Pair!T opOpAssign(string op) (Pair!T p)
	if (op == "-")
	{
		x -= p.x;
		y -= p.y;
		return this;
	}

	/// compare operator
    T opCmp (Pair!T r) const
    {
        return (x == r.x ? y - r.y : x - r.x);
    }

	// opEquals not neccessary; created automatically by compiler
}

/// aliases
alias IPair = Pair!(int);
alias LPair = Pair!(long);
alias DPair = Pair!(double);

unittest
{
	auto p1 = IPair (1, 4);
	auto p2 = IPair (3, 2);

	assert (p1 + p2 == IPair(4,6));
	assert (p1 - p2 == IPair(-2,2));

	assert (p1 * 5 == IPair(5, 20));
	assert (p1 / 2 == IPair(0, 2));

	p1 += p2;
	p2 -= p1;
	assert (p1 == IPair(4, 6));
	assert (p2 == IPair(-1, -4));

	p1.x = 1;
	p1.y = 4;
	assert (p1 == IPair(1,4));
}

/** A template struct holding 4 values of type T.
	Internally, the values are stored in an array of length 2:
	array[0] is x, array[1] is y, array[2] is w and array[3] is h.
**/
struct Rect(T)
{
	T x0, y0, width, height;

	/** the 4th constructor takes a pointer to an address of a C-style array of 4 values of type P

	    When the type P of pointer is not the same type T of the template class,
		conversion is performed using the to! template from std.conf
	**/

	this (Rect!T r)
	{
		x0 = r.x0;
		y0 = r.y0;
		width = r.width;
		height = r.height;
	}

	/// ditto
	this (Pair!T p0, Pair!T p1)
	{
		x0 = p0.x;
		y0 = p0.y;
		width = p1.x-p0.x;
		height = p1.y-p0.y;

		validate();
	}

	/// ditto
	this (Pair!T p, T wi, T he)
	{
		x0 = p.x;
		y0 = p.y;
		width = wi;
		height = he;

		validate();
	}

	void validate ()
	{
		if (width<0)
		{
			x0 += width;
			width = -width;
		}

		if (height<0)
		{
			y0 += height;
			height = -height;
		}
	}

	Pair!T base () const
	{
		return Pair!T(x0, y0);
	}

	void base (Pair!T t)
	{
		x0 = t.x;
		y0 = t.y;
	}

	Pair!T size () const
	{
		return Pair!T(width, height);
	}

	void size (Pair!T t)
	{
		width = t.x;
		height = t.y;
		validate();
	}

	T x1 () const {return x0+width;}
	T y1 () const {return y0+height;}

	/** return a istring "(x, y, w, h)"
	**/
	string toString () const
	{
		return format ("(%s, %s, %s, %s)", x0, y0, width, height);
	}
}

alias IRect = Rect!(int);
alias LRect = Rect!(long);
alias DRect = Rect!(double);

unittest
{
	auto r = IRect(IPair (0, 0), IPair(20, 30));
	assert (IRect (0, 0, 20, 30) == r);

	r.base += IPair(2, 3);
	r.size += IPair(4, 5);
	assert (IRect (2, 3, 24, 35) == r);

	r.x0 += 1;
	r.y0 += 2;
	r.width += 3;
	r.heigth += 4;
	assert (IRect (3, 5, 27, 39) == r);

	double[4] d = [1.1, 2.2, 3.3, 4.4];
	r = IRect(d.ptr);
	assert (IRect (1, 2, 3, 4) == r);
}




/** Push a variable onto a lifo stack

	This and the next few functions allow using dynamic arrays as LIFO stack
**/
void push(T) (ref T[] l, T i)
{
	l ~= i;
}

/** Pop a variable from a lifo stack
**/
T pop(T) (ref T[] l)
{
	enforce (l.length > 0);
	T i = l[$-1];
	l.length--;
	return i;
}

/** copy the first variable on the stack (TOS, Top-Of-Stack)
**/
T tos(T) (ref T[] l)
{
	enforce (l.length > 0);
	return l[$-1];
}

/* some functions for debugging
*/

private int wmicount;

/** whereami()

	EXAMPLE: whereami();
			 whereami("Five is %s", 5);
**/

void whereami(T...) (cstring txt="", string f = __FILE__, int l = __LINE__)
{
	if (txt == "")
	{
		stderr.writefln ("%3s %s %s", ++wmicount, f, l);
	}
	else
	{
		stderr.writefln ("%3s %s %s: %s", ++wmicount, f, l, txt);
	}
}

/** whereami()

	EXAMPLE: whereamif("shutting down %s", this);
**/

void whereamif(T...) (auto ref T m, string f = __FILE__, int l = __LINE__)
{
	stderr.writefln ("%3s %s %s: "~m[0], ++wmicount, f, l, m[1..$]);
}


/* helper functions that don't fit anywhere else
*/

/// function for converting em into pixel
int em (double d) {return cast(int) (d * App.resolution.x * App.design.font.size / 72.0);}

//int mm2px (double d) {return cast(int) (d * App.resolution.x / 25.4);}
//int mil2px (double d) {return cast(int) (d * App.resolution.x / 1000.0);}
//int pt2px (double d) {return cast(int) (d * App.resolution.x / 72.0);}

version (X11)
{
	string eventName (int type)
	{
		switch (type)
		{
		case 2 : return "KeyPress";
		case 3 : return "KeyRelease";
		case 4 : return "ButtonPress";
		case 5 : return "ButtonRelease";
		case 6 : return "MotionNotify";
		case 7 : return "EnterNotify";
		case 8 : return "LeaveNotify";
		case 9 : return "FocusIn";
		case 10: return "FocusOut";
		case 11: return "KeymapNotify";
		case 12: return "Expose";
		case 13: return "GraphicsExpose";
		case 14: return "NoExpose";
		case 15: return "VisibilityNotify";
		case 16: return "CreateNotify";
		case 17: return "DestroyNotify";
		case 18: return "UnmapNotify";
		case 19: return "MapNotify";
		case 20: return "MapRequest";
		case 21: return "ReparentNotify";
		case 22: return "ConfigureNotify";
		case 23: return "ConfigureRequest";
		case 24: return "GravityNotify";
		case 25: return "ResizeRequest";
		case 26: return "CirculateNotify";
		case 27: return "CirculateRequest";
		case 28: return "PropertyNotify";
		case 29: return "SelectionClear";
		case 30: return "SelectionRequest";
		case 31: return "SelectionNotify";
		case 32: return "ColormapNotify";
		case 33: return "ClientMessage";
		case 34: return "MappingNotify";
		case 35: return "GenericEvent";
		default: return "Unknown Event";
		}
	}
}

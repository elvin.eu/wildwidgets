module wcw.widget;

import std.exception;
import std.stdio;
import std.math : abs;
import std.algorithm : max;

import wcw.auxiliary;
import wcw.color;
import wcw.layout;
import wcw.design;
import wcw.application;
import wcw.cairo;
import wcw.window;
import wcw.event;
import wcw.enums;

import cairo;


bool contains (IRect r, IPair p)
{
	if (r.x0 <= p.x && p.x < r.x1 && r.y0 <= p.y && p.y < r.y1) return true;
	return false;
}

/** Base class for all widgets
**/
class Widget
{
package:
	Widget _parent;
	Layout _layout;

	cstring _caption;

	IPair  _position;				// the Widget's position in it's parent
	IPair  _size;					// the Widget's size

	bool   _tainted;			// widget is tainted: redrawing is required
	bool   _hasTainted;			// widget has tainted subwidgets: their redrawing is required

	State  _state;
	bool   _focus;

	IPair _widgetMinSize = IPair (200, 200); // smallest possible widget size
	IPair _widgetNomSize = IPair (200, 200); // preferred widget size
	IPair _widgetMaxSize = IPair (200, 200); // maximum widget size
	IPair _widgetExtSize = IPair (200, 200); // expandable widget size

	Palette _framePalette;
	Palette _widgetPalette;
	int _frameWidth;			// some widgets want to draw a frame around them
	int _edgeRadius;
	int _spacing;
	int _margin;				// attention: margin must include frameWidth

	Align _alignment;

public:
	this (Widget parent, int[] i...)
	{
		setLayout (new SimpleLayout(this));
		setFramePalette  (App.design.framePalette);
		setWidgetPalette (App.design.widgetPalette);
		setEdgeRadius (App.design.widgetEdge);
		setSpacing (App.design.widgetSpacing);
		setMargin  (App.design.widgetMargin);

		if (parent !is null)  parent.addWidget (this, i);
		setActive();
	}

	~this ()
	{
		debug (shutdown) whereami ("~this Widget " ~ toString);
		destroy (_layout); _layout = null;
	}

	auto addTo (Widget parent, int[] i...)
	{
		assert (parent);

		_parent = parent;
		_parent.addWidget (this, i);
		return this;
	}

	/** Get the parent widget.

		Returns null, when the widget is a Window.
	**/
	inout (Widget) parent() inout {return _parent;}
	void setParent (Widget p)
	{
		enforce (_parent is null, "You can assign a widget only once!");
		_parent = p;
	}

	/** get the Layout of this widget.

		There is no public setter, because layouts are always assigned to widgets
		in the layout's constructor.
	**/
	inout (Layout) layout () inout {return _layout;}
	// maybe make this package:
	void setLayout (Layout l) {_layout=l;}

	// layout properties

//	Align alignment () const {return _alignment;}
	void  setAlignment (Align a) {_alignment = a;}

	Align alignment () const
	{
		// return default values if no alignment was secified
		Align ha = _alignment & Align.HMASK;
		Align va = _alignment & Align.VMASK;
		if (ha == Align.NONE) ha = Align.LEFT;
		if (va == Align.NONE) va = Align.TOP;
		return ha | va;
	}

	/** The widget's position; that is the coordinate of the upper left corner
		in the parent's coordinate space.
	**/
	IPair position() const {return _position;}
	package void setPosition (IPair a) {_position = a;}

	/** The widget's current size.
	**/
	IPair size() const {return _size;}
	package void setSize (IPair s) {_size = s;}

	IRect rectangle () const {return IRect (_position, _size.x, _size.y);}

	cstring caption () const {return _caption;}
	void setCaption (cstring c) {_caption = c;}

	const(Palette) framePalette () const {return _framePalette;}
	void setFramePalette (Palette cs) {_framePalette = cs;}

	const(Palette) widgetPalette () const {return _widgetPalette;}
	void setWidgetPalette (Palette cs) {_widgetPalette = cs;}

	int  frameWidth () const {return _frameWidth;}
	void setFrameWidth (int f)
	{
		_frameWidth = abs(f);
		setMargin (margin);		// increase margin, if it is too small
	}

	void addFrame () {setFrameWidth (App.design.widgetFrame);}

	int  edgeRadius () const {return _edgeRadius;}
	void setEdgeRadius (int f) {_edgeRadius = abs(f);}

	int spacing () const {return _spacing;}
	void setSpacing (int s) {_spacing = abs (s);}

	int margin () const {return _margin;}
	void setMargin (int m) {_margin = max(frameWidth, abs(m));}

	const State state () {return _state;}
	protected void setState (State s) {_state = s;}

	/** Convenience functions for setting/getting the widget's state **/
	void setPassive () {_state = State.PASSIVE;}
	/// ditto
	bool isPassive  () {return _state == State.PASSIVE;}

	/// ditto
	void setActive  () {_state = State.ACTIVE;}
	/// ditto
	bool isActive   () {return _state == State.ACTIVE;}

	/// ditto
	void setHilight () {_state = State.HILIGHT;}
	/// ditto
	bool isHilight () {return _state == State.HILIGHT;}

	/// ditto
	void setSelect  () {_state = State.SELECT;}
	/// ditto
	bool hasSelect   () {return _state == State.SELECT;}

	bool isFocus () const {return _focus;}
	void setFocus (bool b) {_focus = b;}

	IPair widgetNomSize() const {return _widgetNomSize;}
	IPair widgetMinSize() const {return _widgetMinSize;}
	IPair widgetMaxSize() const {return _widgetMaxSize;}
	IPair widgetExtSize() const {return _widgetExtSize;}

	IPair[4] widgetSizes () const
	{
		return [_widgetNomSize, _widgetMinSize, _widgetMaxSize, _widgetExtSize];
	}

	IPair[4] sizes () const {return _layout.sizes;}

	/** The widget's preferred size.

		The setter function adds some boilerplate and
		adjusts minsize and maxsize as well
	**/
	const IPair nomSize()
	{
		return _layout.nomSize;
	}
	/// ditto
	void setNomSize (int x, int y) {setNomSize (IPair(x,y));}
	void setNomSize (IPair s)
	{
		import std.algorithm: min, max;


		if (s.x > 0)
		{
			_widgetNomSize.x = s.x;
			_widgetMinSize.x = min (_widgetMinSize.x,_widgetNomSize.x);
			_widgetMaxSize.x = max (_widgetMaxSize.x,_widgetNomSize.x);
			_widgetExtSize.x = max (_widgetMaxSize.x,_widgetExtSize.x);
		}

		if (s.y > 0)
		{
			_widgetNomSize.y = s.y;
			_widgetMinSize.y = min (_widgetMinSize.y,_widgetNomSize.y);
			_widgetMaxSize.y = max (_widgetMaxSize.y,_widgetNomSize.y);
			_widgetExtSize.y = max (_widgetMaxSize.y,_widgetExtSize.y);
		}
	}

	/** The widget's smallest allowed size.
	**/
	IPair minSize() const
	{
		return _layout.minSize();
	}
	/// ditto
	void setMinSize (int x, int y) {setMinSize (IPair(x,y));}
	void setMinSize (IPair s)
	{
		import std.algorithm: max;

		if (s.x > 0)
		{
			_widgetMinSize.x = s.x;
			_widgetNomSize.x = max (_widgetMinSize.x,_widgetNomSize.x);
			_widgetMaxSize.x = max (_widgetMaxSize.x,_widgetNomSize.x);
			_widgetExtSize.x = max (_widgetMaxSize.x,_widgetExtSize.x);
		}

		if (s.y > 0)
		{
			_widgetMinSize.y = s.y;
			_widgetNomSize.y = max (_widgetMinSize.y,_widgetNomSize.y);
			_widgetMaxSize.y = max (_widgetMaxSize.y,_widgetNomSize.y);
			_widgetExtSize.y = max (_widgetMaxSize.y,_widgetExtSize.y);
		}
	}

	/** The widget's normally longst allowed size.
	**/
	IPair maxSize() const
	{
		return _layout.maxSize;
	}

	/// ditto
	void setMaxSize (int x, int y) {setMaxSize (IPair(x,y));}
	void setMaxSize (IPair s)
	{
		import std.algorithm: min, max;

		if (s.x < 0) s.x = int.max;
		if (s.y < 0) s.y = int.max;
		if (s.x > 0)
		{
			_widgetMaxSize.x =  s.x;
			_widgetExtSize.x =  max (_widgetMaxSize.x,_widgetExtSize.x);
			_widgetNomSize.x =  min (_widgetMaxSize.x,_widgetNomSize.x);
			_widgetMinSize.x =  min (_widgetMinSize.x,_widgetNomSize.x);
		}

		if (s.y > 0)
		{
			_widgetMaxSize.y =  s.y;
			_widgetExtSize.y =  max (_widgetMaxSize.x,_widgetExtSize.y);
			_widgetNomSize.y =  min (_widgetMaxSize.x,_widgetNomSize.y);
			_widgetMinSize.y =  min (_widgetMinSize.x,_widgetNomSize.y);
		}
	}

	/** The widget's extended size.
	**/
	IPair extSize() const
	{
		return _layout.extSize;
	}

	/// ditto
	void setExtSize (int x, int y) {setExtSize (IPair(x,y));}
	void setExtSize (IPair s)
	{
		import std.algorithm: min, max;

		if (s.x < 0) s.x = int.max;
		if (s.y < 0) s.y = int.max;
		if (s.x > 0)
		{
			_widgetExtSize.x =  s.x;
			_widgetMaxSize.x =  min (_widgetMaxSize.x,_widgetExtSize.x);
			_widgetNomSize.x =  min (_widgetMaxSize.x,_widgetNomSize.x);
			_widgetMinSize.x =  min (_widgetMinSize.x,_widgetNomSize.x);
		}

		if (s.y > 0)
		{
			_widgetExtSize.y =  s.y;
			_widgetMaxSize.y =  min (_widgetMaxSize.x,_widgetExtSize.y);
			_widgetNomSize.y =  min (_widgetMaxSize.x,_widgetNomSize.y);
			_widgetMinSize.y =  min (_widgetMinSize.x,_widgetNomSize.y);
		}
	}

	/** set the size of the widget to a fixed value
	**/
	void setFixSize (int x, int y) {setFixSize (IPair(x,y));}
	void setFixSize (IPair s)
	{
		if (s.x > 0)
		{
			_widgetNomSize.x = s.x;
			_widgetMinSize.x = s.x;
			_widgetMaxSize.x = s.x;
			_widgetExtSize.x = s.x;
		}

		if (s.y > 0)
		{
			_widgetNomSize.y = s.y;
			_widgetMinSize.y = s.y;
			_widgetMaxSize.y = s.y;
			_widgetExtSize.y = s.y;
		}
	}

	/** The margin around and spacing between the widgets of a Layout.
		For Widgets this is (0, 0).
	**/
	IPair padding() const
	{
		return _layout.padding;
	}

	bool addWidget (Widget widget, int[] i...)
	{
		return _layout.addWidget (widget, i);
	}

	///
	inout Window window ()
	{
		if (parent) return parent.window;
		else        assert (0);
	}

	///
	override istring toString () const
	{
		import std.string:lastIndexOf;

		with (this.classinfo)
		{
			string s = name[lastIndexOf(name, '.')+1..$] ~ " \"" ~ cast(string)_caption ~ "\"";
			return s;
		}
	}

	const Color currentWidgetColor ()
	{
		final switch (_state)
		{
		case State.ACTIVE:
			return _widgetPalette.active;
		case State.HILIGHT:
			return _widgetPalette.hilight;
		case State.SELECT:
			return _widgetPalette.select;
		case State.PASSIVE:
			return _widgetPalette.passive;
		}
	}

	const Color currentFrameColor ()
	{
		final switch (_state)
		{
		case State.ACTIVE:
			return _framePalette.active;
		case State.HILIGHT:
			return _framePalette.hilight;
		case State.SELECT:
			return _framePalette.select;
		case State.PASSIVE:
			return _framePalette.passive;
		}
	}

	bool canDraw ()
	{
		if (window && window.surface && window.isVisible) return true;
		else return false;
	}

	void paint ()
	{
		if (!canDraw()) return;

		IPair w = mapToWindow (position);
		auto sf = CairoSurface.create_for_rectangle (window.surface, w.x, w.y, size.width, size.height);
		scope (exit) destroy (sf);
		paint (sf);
	}

	void paint (CairoSurface sf)
	{
		auto ct = new CairoContext (sf);
		scope (exit) destroy (ct);
		paint (ct);
	}

	/// paint thew widgets content. Usually called from paintTaintedWidgets(), which itself is called
	/// in the main event loop [App.run()] right before pending events are handled
	void paint (CairoContext ctx)
	{
		debug (paint) whereami(format("painting %s", this));

		double width = size.x;
		double height = size.y;

		// if neccessary, create clip region
		if (edgeRadius > 0)
		{
			ctx.rectangle (0.5, 0.5, width-1.0, height-1.0, edgeRadius);
			ctx.clip ();
		}

		/* clear background */
		double gradient = _widgetPalette.gradient;

		// create the path that describes the background
		ctx.new_path ();

		foreach (w; _layout.widgetList)
		{
			auto r = w.rectangle;
			ctx.rectangle (r.x0, r.y0, r.width, r.height);
		}

		// now, build a rectangle in reverse clock direction that describes the outer border
		ctx.new_sub_path ();
		ctx.move_to (0, 0);
		ctx.line_to (0, size.height);
		ctx.line_to (size.width, size.height);
		ctx.line_to (size.width, 0);
		ctx.close_path ();

		if (gradient == 1.0)
		{
			ctx.set_source_color (currentWidgetColor);		// simply paint the whole
			ctx.fill ();									// widget with color
		}
		else // gradient != 1.0
		{
			auto loColor = currentWidgetColor.dark(gradient);
			auto hiColor = currentWidgetColor.light(gradient);

			auto pattern = new CairoLinearPattern (0, 0, 1, height);
			pattern.add_color_stop_color (0.0, loColor);
			pattern.add_color_stop_color (1.0, hiColor);

			ctx.set_source (pattern);
			ctx.fill ();
			pattern.destroy ();
		} // clear background end

		/* draw frame */
		if (frameWidth > 0)
		{
			gradient = _framePalette.gradient;
			ctx.set_line_cap (CairoLineCap.SQUARE);

			if (gradient == 1.0 || frameWidth == 1) // don't draw a too narrow frame with gradient
			{
				double lw = frameWidth;
				double ll = frameWidth * 0.5;
				double ur = frameWidth;

				ctx.set_line_width (lw);
				ctx.set_source_color (currentFrameColor);
				ctx.rectangle (ll, ll, width-ur, height-ur, edgeRadius);
				ctx.stroke ();
			}
			else // gradient != 1.0 and frame > 1
			{
				double lw  = frameWidth * 0.5;
				double llo = frameWidth * 0.25;
				double uro = frameWidth * 0.5;
				double lli = frameWidth * 0.75;
				double uri = frameWidth * 1.5;

				auto loColor = currentFrameColor.dark(gradient);
				auto hiColor = currentFrameColor.light(gradient);

				ctx.set_line_width (lw);
				ctx.set_source_color (hiColor);
				ctx.rectangle (llo, llo, width-uro, height-uro, edgeRadius-1.0);
				ctx.stroke ();
				ctx.set_source_color (loColor);
				ctx.rectangle (lli, lli, width-uri, height-uri, edgeRadius);
				ctx.stroke ();
			}
		} // draw frame end
	}

	const bool isTainted() {return _tainted;}
	const bool hasTainted() {return _hasTainted;}

	void setTainted (bool b = true)
	{
		_tainted = b;
		if (b && _parent) _parent.setHasTainted ();
	}

	void setHasTainted (bool b = true)
	{
		_hasTainted = b;
		if (b && _parent) _parent.setHasTainted ();
	}

	void redraw ()
	{
		setTainted (true);
		foreach (w; layout.widgetList) w.redraw();
	}

	void resize (IPair newsize, IPair newpos = IPair(int.min,int.min))
	{
		move (newpos, newsize);
	}

	void move (IPair newpos, IPair newsize = IPair(int.min,int.min))
	{
		if (newpos.x == int.min || newpos.y == int.min) newpos = position;
		if (newsize.x == int.min || newsize.y == int.min) newsize = size;
		if ((newsize == size) && (newpos == position)) return;

		setPosition (newpos);
		setSize   (newsize);
		layout.rearrange();

		auto e = new TransformEvent ();
		e.size = size;
		e.position = position;
		handleEvent (e.Event);
	}

	/** handler gets called, when key was pressed
	**/
	void keyPressHandler (in KeyPressEvent e)
	{
	}

	/** handler gets called, when key was released
	**/
	void keyReleaseHandler (in KeyReleaseEvent e)
	{
	}

	/** handler gets called, when mouse button  was pressed
	**/
	void buttonPressHandler (in ButtonPressEvent event)
	{
	}

	/** handler gets called, when mouse button  was released
	**/
	void buttonReleaseHandler (in ButtonReleaseEvent event)
	{
	}

	/** handler gets called, when mouse was moved
	**/
	void mouseMoveHandler (in MouseMoveEvent event)
	{
	}

/*
	bool exposeEvent (IRect area)
	{
		if (! rectangle.intersects(area)) return false;

		foreach (w; layout.widgetList)
		{
			if (w.mouseMoveHandler(pos-rectangle.position)) return true;
		}

		if (_hasMouse == false)
		{
			parentWindow.setMouseOwner (this);
		}
		return true;
	}
	*/


	/**	highlights the button when the mouse pointer enters the button's area
	**/
	void mouseInHandler (in MouseInEvent event)
	{
		if (isPassive) return;

		auto c = currentWidgetColor;
		setHilight ();
		if (c != currentWidgetColor) redraw ();
	}

	/** de-highlights the button when the mouse pointer leaves the button's area
	**/
	void mouseOutHandler (in MouseOutEvent event)
	{
		if (isPassive()) return;

		auto c = currentWidgetColor;
		setActive ();
		if (c != currentWidgetColor) redraw ();
	}

	void focusInHandler (in FocusInEvent event)
	{
	}

	void focusOutHandler (in FocusOutEvent event)
	{
		if (isPassive()) return;

		setActive ();
	}

	/** This event handler gets called, when a widget is about to be closed.
		This happens as a result to CTRL-C being entered on a controlling console,
		or when the Window's close button was pressed
	**/
	void closeHandler (in CloseEvent event) {}

	/** This event handler gets called, when a widget receives a timer event,
	    which happens every 100 ms.
	**/
	void clockHandler (in ClockEvent event) {}

	void redrawHandler (in RedrawEvent event)
	{
		redraw();
	}

	void transformHandler (in TransformEvent event)
	{
		redraw();
	}

	void showHandler (in ShowEvent event)
	{
	}

	void handleEvent (Event event)
	{
		switch (event.type)
		{
			case EventType.REDRAW:
				// distribute this event to all subwidgets
				redrawHandler (cast (RedrawEvent) event);
				foreach (w; layout.widgetList)
				{
					w.handleEvent (event);
				}
				return;
			case EventType.TRANSFORM:      return transformHandler     (cast (TransformEvent) event);
			case EventType.CLOSE:          return closeHandler         (cast (CloseEvent) event);
			case EventType.CLOCK:          return clockHandler         (cast (ClockEvent) event);
			case EventType.MOUSEMOVE:      return mouseMoveHandler     (cast (MouseMoveEvent) event);
			case EventType.BUTTONPRESSED:  return buttonPressHandler   (cast (ButtonPressEvent) event);
			case EventType.BUTTONRELEASED: return buttonReleaseHandler (cast (ButtonReleaseEvent) event);
			case EventType.KEYPRESSED:     return keyPressHandler      (cast (KeyPressEvent) event);
			case EventType.KEYRELEASED:    return keyReleaseHandler    (cast (KeyReleaseEvent) event);
			case EventType.MOUSEIN:        return mouseInHandler       (cast (MouseInEvent) event);
			case EventType.MOUSEOUT:       return mouseOutHandler      (cast (MouseOutEvent) event);
			case EventType.FOCUSIN:        return focusInHandler       (cast (FocusInEvent) event);
			case EventType.FOCUSOUT:       return focusOutHandler      (cast (FocusOutEvent) event);
			case EventType.SHOW:           return showHandler          (cast (ShowEvent) event);
//			case EventType.HIDE:           return hideHandler          (cast (HideEvent) event);
			default:
		}
	}

	bool paintTaintedWidgets()
	{
		bool rc = false;
		if (isTainted)
		{
			paint();
			setTainted (false);
			rc = true;
		}
		if (hasTainted)
		{
			foreach (w; layout.widgetList) w.paintTaintedWidgets();
			setHasTainted (false);
			rc = true;
		}
		return rc;
	}


	IPair mapFromWindow (IPair p)
	{
		return p - mapToWindow (position);
	}

	IRect mapFromWindow (IRect r)
	{
		r.base = mapFromWindow (r.base);
		return r;
	}

	/**
	    map relative coordinates based on the parent Widget
	    to absolute coordinates based on the parent Window
	**/
	IPair mapToWindow (IPair p)
	{
		if (parent)	return p + parent.mapToWindow (parent.position);
		else        return IPair (0, 0);
	}

	/**
	    map relative coordinates based on the parent Widget
	    to absolute coordinates based on the screen
	**/
	IPair mapToScreen (IPair p)
	{
		return mapToWindow(p) + window._position;
	}

	IRect mapToWindow (IRect r)
	{
		r.base = mapToWindow(r.base);
		return r;
	}

	IRect mapToScreen (IRect r)
	{
		r.base = mapToScreen(r.base);
		return r;
	}



	/** find the widget furthest down the tree, that contains pointer.
	**/
	Widget findWidget (IPair pos)
	{
		if (!mapToWindow(rectangle).contains(pos)) return null;

		Widget widget;
		foreach (w; layout.widgetList)
		{
			widget = w.findWidget (pos);
			if (widget) return widget;
		}

		return this;
	}
}

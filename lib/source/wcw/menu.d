module wcw.menu;

import std.exception: enforce;
import std.algorithm: max;

import wcw.auxiliary;
import wcw.application;
import wcw.design;
import wcw.font;
import wcw.event;
import wcw.widget;
import wcw.layout;
import wcw.window;
import wcw.enums;

import wcw.button;


/** A Menu is a Window with no borders. It is activated by its assigned MenuItem
**/

class MenuPanel : Window
{
protected:
	MenuItem _parentItem;

public:
	this (MenuItem parentItem)
	{
		setRole (WindowRole.MENU);

		super (parentItem.caption);
		new VerticalLayout (this);
		addFrame;
		setSpacing (0);
		setMargin  (0);

		setWidgetPalette (App.design.buttonPalette);

		_parentItem = parentItem;
		_parentItem._menuPanel = this;
	}

	override void addFrame () {setFrameWidth (1);}

	override istring toString () const {return ("Menu" ~ (caption.length ? " "~caption : "").idup);}

	/** the Menuitem, that is assigned to this menu
	**/
	MenuItem parentItem () {return _parentItem;}

	/** The Menu (when there is one), that contains parentItem.
		Otherwise returns null.
	**/
	MenuPanel parentMenuPanel ()
	{
		if (_parentItem) return _parentItem.parentMenuPanel;
		else             return null;
	}

	override void show ()
	{
		IPair pos;

		if (cast(MenuPanel)parentItem.parent)
			pos = parentItem.mapToScreen(parentItem.position) + IPair(_parentItem.size.x, 0);
		else
			pos = parentItem.mapToScreen(parentItem.position) + IPair(0, _parentItem.size.y);

		move (pos, minSize);
		super.show();
	}
}

class MenuBar : Widget
{
	this (Widget parent)
	{
		super (parent);
		new HorizontalLayout (this);

		setSpacing (0);
		setMargin  (0);
		setExtSize (-1, 0);
		// setAlignment (Align.LEFT); (LEFT is default)

		setWidgetPalette (App.design.buttonPalette);
	}

	override istring toString () const {return ("MenuBar" ~ (caption.length ? " "~caption : "").idup);}
}

/** A MenuItem basically is just a button that opens a Menu or (like all buttons) sends out a
	signal, when it was clicked. Menus form a tree, and MenuItems are the glue between the menus.
	The menu, that contains a MenuItem cant be retreiived with parentMenu(), the menu that
	is called by a MenuItem, is retreived with menu()
**/
class MenuItem : Button
{
protected:
	MenuPanel _menuPanel;
	MenuPanel _parentMenuPanel;

public:
	/**
	**/

	this (cstring text, MenuBar menubar)
	{
		this (text, cast(Widget)(menubar));
	}

	this (cstring text, Menu item)
	{
		this (text, cast(Widget)(item._menuPanel));
	}

	private this (cstring text, Widget parent)
	{
		assert (parent);
		super ("", parent);
		_frameWidth = 0;
		_textBorder = IPair(em(0.5), em(0.25));
		setCaption (text); // recalculates the Item's size
		setTextAlignment (Align.LEFT);

		auto pm = cast(MenuPanel)parent;	// pm = MenuPanel or null

		if (pm) // if this item is located in a Menu
		{
			setExtSize (IPair (-1, 0));
			_parentMenuPanel = pm;
		}
	}


	MenuPanel menuPanel () {return _menuPanel;}
	MenuPanel parentMenuPanel () {return _parentMenuPanel;}

	override void setCaption (cstring t)
	{
		super.setCaption (t);

		auto fonte = App.design.font.fontExtents;
		auto texte = App.design.font.textExtents (caption);
		auto txth = max (fonte.height, fonte.ascent + fonte.descent);	// text height
		auto xgap = frameWidth+_textBorder.x;							// shortcut for start of text
		auto ygap = frameWidth+_textBorder.y;

		auto s = IPair (cast(int) texte.x_advance + 2*xgap, // _frameWidth = 0!
		                cast(int) txth + 2*ygap);
		setFixSize (s);
/*
		if ((parent.layout !is null) && (cast(VerticalLayout)parent.layout !is null))
			setMaxSize (IPair(30.em, maxSize.y));
*/
		return;
	}

	override void buttonPressHandler (in ButtonPressEvent event)
	{
		super.buttonPressHandler (event);

		// if menu is not yet shown, show it
		if (menuPanel && !App.menu)
		{
			App.showMenu (menuPanel);
		}
		// otherwise hide it, but only when this item is the start of a menu tree
		else if (!cast(MenuPanel)parent)
		{
			App.showMenu (null);
		}
	}


	override void buttonReleaseHandler (in ButtonReleaseEvent event)
	{
		super.buttonReleaseHandler (event);

		if (menuPanel is null) App.showMenu (null);
	}

	override void mouseInHandler (in MouseInEvent event)
	{
		super.mouseInHandler (event);

		if (App.menu !is null)
		{
			if  (menuPanel is null) App.showMenu (parentMenuPanel);
			else                    App.showMenu (menuPanel);
		}
	}

	override istring toString () const {return ("MenuItem " ~ caption).idup;}
}


class Menu : MenuItem
{
	MenuPanel _menuPanel;

	this (cstring text, MenuBar menubar)
	{
		super (text, menubar);
		_menuPanel = new MenuPanel (this);
	}

	this (cstring text, Menu item)
	{
		super (text, item);
		_menuPanel = new MenuPanel (this);
	}

}

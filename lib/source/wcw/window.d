module wcw.window;

import cairo.surface;

import wcw.application;
import wcw.widget;
import wcw.cairo;
import wcw.auxiliary;
import wcw.event;
import wcw.menu;
import wcw.enums;
import wcw.image;

version (X11) import wcw.x11.window;

class Window : Widget
{
private:
	Window_Backend _backend;
	Window _transition_parent;	// parent for transient windows; currently only used in Dialogs
	DialogResult _result;		// result code for run(), can be set by Dialogs
	bool   _visible;			// window is shown on the screen
	bool _existent;

protected:
	bool _may_quit = true;
	bool _clock_active = true;

package:
	Widget _mouseOwner;
	bool _taintedWidgets;
	WindowRole _role;
	WindowAttribute _attributes;
	CairoSurface _surface;

public:
	Window_Backend backend ()
	{
		assert (_backend);
		return _backend;
	}

	this (cstring title = null)
	{
		super (null);

		if      (title != null)        _caption = (title);
		else if (App.title.length > 0) _caption = (App.title);
		else                           _caption = ("WildWidgets Window");

		setSpacing (App.design.windowSpacing);
		setMargin  (App.design.windowMargin);

		setRole (WindowRole.WINDOW); // this will set role only when it's not yet set

		if (role == WindowRole.MENU)
		{
			_attributes |= WindowAttribute.TRANSIENT |
			               WindowAttribute.UNMANAGED |
			               WindowAttribute.MODAL;
		}
		else if (role == WindowRole.DIALOG)
		{
			_attributes |= WindowAttribute.TRANSIENT;
//			 |
//			               WindowAttribute.MODAL;
		}

		App.addWindow (this);

		_visible = false;

		version (X11) _backend = new Window_X11 (this);
	}

	~this ()
	{
		debug (shutdown) whereami ("~this Window " ~ toString);
		App.removeWindow (this);
		destroy (_surface); _surface = null;
		destroy (_layout);  _layout  = null;
		destroy (_backend); _backend = null;
	}

	override inout Window window () {return cast(Window)this;}

	Window transitionParent () {return _transition_parent;}
	void setTransitionParent (Window parent) {_transition_parent = parent;}


	DialogResult result () const {return _result;}
	void setResult (DialogResult r) {_result= r;}

	const bool isVisible () {return _visible;}
	void setVisible (bool v) {_visible = v;}

	WindowRole role () const {return _role;}
	void setRole (WindowRole r)
	{
		// do not change role anymore after it was set
		if (_role == 0) _role = r;
	}

	WindowAttribute attributes () const {return _attributes;}
	void setAttributes (WindowAttribute a) {_attributes = a;}

	/// close event tells the application's main loop to quit
	override void closeHandler (in CloseEvent event)
	{
		if (_may_quit)
		{
			debug (shutdown) whereami ("Window.closeHandler");
			App.quit();
		}
	}

/*
	override bool mouseMoveHandler (in MouseMoveEvent event)
	{
		/*
		if (w != _mw)
		{
			if (_mw) _mw.mouseLeaveEvent (new MouseLeaveEvent);
			if (w) w.mouseEnterEvent (new MouseEnterEvent);

			_mw = w;
		}

		return true;
	}
*/

	Widget _pointerWidget;
	Widget _focusWidget;
	Widget _buttonPressWidget;

	void setFocusWidget (Widget widget)
	{
		_focusWidget = widget;
	}

	Widget cw;

	/** receive Events and send them to the appropriate widget.
	**/
	void dispatchEvent (Event event)
	{
		if (cast(MouseEvent)event)
		{
			Widget old = _pointerWidget;
			IPair pointer = (cast(MouseEvent)event).pointer;

			if (_pointerWidget)
				_pointerWidget = _pointerWidget.findWidget (pointer);

			// found a widget? if not, search from the top:
			if (!_pointerWidget)
				_pointerWidget = findWidget (pointer);

			// if still no widget found, drop this event with an error message
			if (!_pointerWidget)
			{
				debug (events) whereami(format("Coordinates for event out of bound!"));
				return;
			}

			// Widget under mouse pointer changed?
			if (old != _pointerWidget)
			{
				if (old)
				{
					Event fo = cast(Event) new MouseOutEvent ();
					old.handleEvent (fo);
				}

				Event fi = cast(Event) new MouseInEvent ();
				_pointerWidget.handleEvent (fi);
			}

			if (event.type == EventType.BUTTONPRESSED)
			{
				_buttonPressWidget = _pointerWidget;
				if (_pointerWidget.isFocus) _focusWidget = _pointerWidget;
				if (cast(MenuItem)_pointerWidget is null) App.showMenu(null);

			}
			else if (event.type == EventType.BUTTONRELEASED)
			{
				_pointerWidget = _buttonPressWidget;
			}

			if (_pointerWidget)
			{
				 _pointerWidget.handleEvent (event);
			}
		}
		else if (cast(KeyEvent)event)
		{
			if (_focusWidget) _focusWidget.handleEvent(event);
			else debug (events) whereami ("no focus widget for KeyEvent");
		}
		else if (cast(ClockEvent)event)
		{
			if (_clock_active)
			{
				if (_focusWidget) _focusWidget.handleEvent(event);
				else debug (events) whereami ("no focus widget for ClockEvent");
			}
		}
		else
		{
			handleEvent (event);
		}
	}

	override void focusInHandler (in FocusInEvent event)
	{
		_clock_active = true;
		super.focusInHandler (event);
	}

	override void focusOutHandler (in FocusOutEvent event)
	{
		_clock_active = false;
		App.showMenu (null);
		super.focusOutHandler (event);
	}

	CairoSurface surface ()
	{
		return _surface;
	}

	/** Resize and move a widget
	**/
	override void move (IPair newpos, IPair newsize = IPair(int.min,int.min))
	{
		backend.move (newpos, newsize);
	}

	override void resize (IPair newsize, IPair newpos = IPair(int.min,int.min))
	{
		backend.resize (newsize, newpos);
	}

	override void transformHandler (in TransformEvent event)
	{
		// position or size could be invalid (in.min,int.min)!
		if (event.position.x != int.min && event.position.y != int.min)
		{
			setPosition (event.position);
			// no rearrangement neccessary for position changes
		}

		if (event.size.x != int.min && event.size.y != int.min && size != event.size)
		{
			setSize (event.size);
			backend.setSurfaceSize (size);
			layout.rearrange();
		}
	}

	/** Make the widget and all its children visible on the screen.

		For XCB, this generates a x11_map_window request which in turn
		generates a MAP_NOTIFY and an EXPOSE event.
	**/
	void show ()
	{
		if (_existent==false) resize (nomSize);

		assert (layout);
		layout.rearrange();
		backend.show();

		_existent = true;
		_visible = true;
	}

	/** Make the window and all its children invisible on the screen
		without destroying it.
	**/
	void hide ()
	{
		backend.hide();
		_visible = false;
	}

	override void setCaption (cstring title)
	{
		backend.setCaption (title);
	}

	/** return the sizes of the Window's frame (which is controlled by the underlying
		window system). Returns a fixed array int[4], with the size values [left, upper, right, lower]
	**/
	int[4] frameSizes ()
	{
		return backend.frameSizes();
	}

	void setApplicationIcon (Image image)
	{
		backend.setApplicationIcon (image);
	}

}

// functions, that are overwritten by the backend

interface Window_Backend
{
	void move   (IPair newpos, IPair newsize);
	void resize (IPair newsize, IPair newpos);
	void setSurfaceSize (IPair size);
//	void focusOutHandler (in FocusOutEvent event);
	void show ();
	void hide ();
	void setCaption (cstring title);
	int[4] frameSizes ();
	void setApplicationIcon (Image image);
}
module wcw.dialog;

import std.typecons;

import wcw.window;
import wcw.layout;
import wcw.label;
import wcw.button;
import wcw.auxiliary;
import wcw.application;
import wcw.widget;
import wcw.enums;
import wcw.event;
import wcw.editor;


class Dialog : Window
{
private:

public:
	this (Window parent, cstring title = null)
	{
		setTransitionParent (parent);
		setRole (WindowRole.DIALOG);
		super (title);
		_may_quit = false;
	}

	this (Widget parent, cstring title = null)
	{
		this (parent.window(), title);
	}

	DialogResult run ()
	{
		App.run (this);
		return result;
	}

	override void mouseMoveHandler (in MouseMoveEvent event)
	{
		super.mouseMoveHandler (event);
	}

	override void closeHandler (in CloseEvent)
	{
		// ignore the window's close button (if there is one)
	}

	static DialogResult Message (Window parent, cstring title, cstring message, cstring buttons)
	{
		auto d = new MessageDialog (parent, title, message, buttons);
		scope (exit) destroy (d);

		d.show;
		return d.run ();
	}

	static cstring FileOpen (Window parent, cstring path="")
	{
		auto d = new FileDialog (parent, "Open File:", path);
		scope (exit) destroy (d);

		d.show;
		auto x = d.run ();
		auto p = d.path;

		if (x == DialogResult.ACCEPT) return p;
		else                          return "";
	}

	static cstring FileSave (Window parent, cstring path="")
	{
		auto d = new FileDialog (parent, "Save File:", path);
		scope (exit) destroy (d);

		d.show;
		auto x = d.run ();
		auto p = d.path;

		if (x == DialogResult.ACCEPT) return p;
		else                          return "";
	}

}

class MessageDialog : Dialog
{
	this (Window parent, cstring title, cstring message, cstring buttons)
	{
		import std.algorithm;

		super (parent, title);

		new VerticalLayout (this);
		this.setAlignment (Align.CENTER);

		auto lb = new Label (message, this);

		auto hl = new HorizontalWidget (this);
		auto ac = buttons.canFind ("a");
		auto ca = buttons.canFind ("c");
		auto de = buttons.canFind ("d");
		auto no = buttons.canFind ("n");
		auto ye = buttons.canFind ("y");
		auto ok = buttons.canFind ("o");

		auto l = ca;
		auto m = no || de;
		auto r = ye || ok || ac;

		if (ca)
		{
			auto b = new Button ("Cancel", hl);
			b.clicked.connect (&set_cancel);
		}

		if (no)
		{
			auto b = new Button ("No", hl);
			b.clicked.connect (&set_no);
		}

		if (de)
		{
			auto b = new Button ("Deny", hl);
			b.clicked.connect (&set_deny);
		}

		if (ye)
		{
			auto b = new Button ("Yes", hl);
			b.clicked.connect (&set_yes);
		}

		if (ac)
		{
			auto b = new Button ("ACCEPT", hl);
			b.clicked.connect (&set_accept);
		}

		if (ok)
		{
			auto b = new Button ("OK", hl);
			b.clicked.connect (&set_ok);
		}

	}

	void set_cancel () {setResult (DialogResult.CANCEL);}
	void set_no     () {setResult (DialogResult.NO);}
	void set_deny   () {setResult (DialogResult.DENY);}
	void set_yes    () {setResult (DialogResult.YES);}
	void set_accept () {setResult (DialogResult.ACCEPT);}
	void set_ok     () {setResult (DialogResult.OK);}
}

class FileDialog : Dialog
{
	LineEditor _path;

	Button _cancelButton;
	Button _acceptButton;

	this (Window parent, cstring title, cstring path="")
	{
		super (parent, title);

		auto v = new VerticalLayout (this);
		this.setAlignment (Align.CENTER);

		_path = new LineEditor (path, this);
		_path.setMinSize (IPair(20.em, 0));
		_path.setMaxSize (IPair(-1, 0));
		_path.returnPressed.connect (&accept);
		_path.escapePressed.connect (&cancel);

		auto h = new HorizontalWidget (this);
		auto c = new Button ("Cancel", h);
		auto s = new Spacer (h);
		s.setMaxSize (IPair(1.em, 0));
		auto a = new Button ("Accept", h);

		c.clicked.connect (&cancel);
		a.clicked.connect (&accept);
	}

	void accept () {setResult (DialogResult.ACCEPT);}
	void cancel () {setResult (DialogResult.CANCEL);}

	cstring path ()  {return _path.getLine (0);}
}

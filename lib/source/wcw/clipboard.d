module wcw.clipboard;

import wcw.enums;

version (X11) import wcw.x11.clipboard;

class Clipboard
{
private:
	Clipboard_Backend _backend;

public:
	this ()
	{
		version (X11) _backend = new Clipboard_X11 ();
	}

	Clipboard_Backend backend () {return _backend;}

	byte[] readData (string type, SelectionType selection=SelectionType.CLIPBOARD)
	{
		return backend.readData (type, selection);
	}

	char[] readText (SelectionType selection=SelectionType.CLIPBOARD)
	{
		return backend.readText (selection);
	}


	string[] readTargets (SelectionType selection=SelectionType.CLIPBOARD)
	{
		return backend.readTargets (selection);
	}

	void writeData (in byte[] data, string type, SelectionType selection=SelectionType.CLIPBOARD)
	{
		backend.writeData (data, type, selection);
	}

	void writeText (in char[] txt, SelectionType selection=SelectionType.CLIPBOARD)
	{
		backend.writeText (txt, selection);
	}
}

interface Clipboard_Backend
{
	byte[] readData (string type, SelectionType selection);
	char[] readText (SelectionType selection);
	string[] readTargets (SelectionType selection);
	void writeData (in byte[] data, string type, SelectionType selection);
	void writeText (in char[] txt, SelectionType selection);
}

module wcw.button;

import std.signals;
import std.string;
import std.algorithm;

import cairo;

import wcw.auxiliary;
import wcw.cairo;
import wcw.font;
import wcw.image;
import wcw.design;
import wcw.event;
import wcw.enums;


import wcw.application;
import wcw.widget;
import wcw.layout;

/** BaseButton
**/

class BaseButton : Widget
{
	// the signals
	mixin Signal!() clicked;

	this (Widget parent, int[] i...)
	{
		super (parent, i);

		setWidgetPalette (App.design.buttonPalette);
		setEdgeRadius (App.design.buttonEdge);
		addFrame();
	}

	override void addFrame () {setFrameWidth (App.design.buttonFrame);}

	/** overridden _event

		changes button's color state
	**/
	override void buttonPressHandler (in ButtonPressEvent event)
	{
		if (isPassive) return;

		setSelect ();
		redraw();
	}

	/** overridden _event

		changes button's color state
		emits clicked, when the left mouse button got pressed & released on the button
	**/
	override void buttonReleaseHandler (in ButtonReleaseEvent event)
	{
		if (isPassive) return;
		if (!mapToWindow(rectangle).contains(event.pointer)) return;

		if (event.button == MouseButton.LEFT)
		{
			clicked.emit ();
			setHilight();
		}
		else
		{
			setActive ();
		}

		redraw();
	}
}

/** Button
**/
class Button : BaseButton
{
protected:
	Palette _textPalette;
	Font     _textFont;

	Align   _textAlignment = Align.CENTER;
	IPair   _textBorder; // small space between text and frame when alignment is not centered

public:
	/** Constructor

		Params:
		caption   =    the button's text
		parent =    parent widget
		i      =    layout hints
	**/
	this (cstring caption, Widget parent, int[] i...)
	{
		setCaption (caption);
		_textPalette = App.design.textPalette;
		_textFont     = App.design.font;
		_textBorder = IPair (em(0.2), em(0.2));
		setFrameWidth (App.design.buttonFrame);
		super (parent, i);
		setFixSize (IPair (em(7), em(2)));		// maybe make size more flexible?
	}

	void setTextAlignment (Align a) {_textAlignment = a;}
	Align textAlignment () const {return _textAlignment;}

	/** overridden function

		redraws the button's area
	**/

	override void paint (CairoContext context)
	{
		super.paint (context);

		// draw text
		auto fonte = _textFont.fontExtents;
		auto texte = _textFont.textExtents (caption);

		auto txth = max (fonte.height, fonte.ascent + fonte.descent);	// text height
		auto xgap = frameWidth+_textBorder.x;							// shortcut for start of text
		auto ygap = frameWidth+_textBorder.y;

		auto h = textAlignment & Align.HMASK;
		auto v = textAlignment & Align.VMASK;
		double tx, ty;

		if      (h == Align.RIGHT) tx = xgap;
		else if (h == Align.LEFT)  tx = xgap;
		else                       tx = (size.x-texte.x_advance)/2;

		if      (v == Align.TOP)    ty = ygap+fonte.ascent;
		else if (v == Align.BOTTOM) ty = size.y-ygap-fonte.descent;
		else                        ty = (size.y-txth)/2+fonte.ascent;

		context.set_source_color (_textPalette.active);
		context.set_scaled_font (_textFont.scaledFont);
		auto glyphs = _textFont.scaledFont.text_to_glyphs (tx, ty, caption);
		context.show_glyphs (glyphs);
	} // redraw
}

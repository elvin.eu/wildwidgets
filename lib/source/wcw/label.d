module wcw.label;

import std.string: toStringz, format;
import std.conv: to;

import cairo;

import wcw.auxiliary;
import wcw.enums;
import wcw.application;
import wcw.widget;
import wcw.cairo;
import wcw.design;
import wcw.font;

import wcw.color;

class Label : Widget
{
protected:
	Palette _textPalette;
	Font     _textFont;

public:
	this (cstring caption, Widget parent, int[] i...)
	{
		super (parent, i);
		_textPalette = App.design.textPalette;
		_textFont     = App.design.font;
		setMargin  (App.design.textMargin);
		setCaption (caption);
		// use default for horizontal and center vor vertical alignment
		setAlignment (alignment | Align.VCENTER);

		// define the Label's size:
		auto fonte = _textFont.fontExtents;
		auto texte = _textFont.textExtents (caption);
		int w = to!int (texte.x_advance);
		if (frameWidth > 0) w += 2*margin;
		// add 2*margin so that height is the same as for LineEditor
		int h = to!int (fonte.height+2*margin);
		setFixSize (w, h);
	}

	~this ()
	{
		debug (shutdown) whereami ("~this Label " ~ toString);
	}

	override void addFrame () {setFrameWidth (App.design.textFrame);}

	// this will not change the Label's size
	override void setCaption (cstring c)
	{
		super.setCaption (c);
		redraw();
	}

	override void paint (CairoContext ctx)
	{
		if (!ctx) return;
		super.paint (ctx);

		ctx.set_source_color (_textPalette.active);
		ctx.set_scaled_font (_textFont.scaledFont);

		// draw text
		auto fonte = _textFont.fontExtents;
		auto texte = _textFont.textExtents (caption);

		double tx;
		switch (alignment & Align.HMASK)
		{
		case Align.RIGHT:
			tx = size.x-texte.x_advance;
			break;
		case Align.HCENTER:
			tx = (size.x-texte.x_advance)/2;
			break;
		default:
			tx = 0;
		}
		if (frameWidth > 0) tx += margin;

		double ty;
		switch (alignment & Align.VMASK)
		{
		case Align.BOTTOM:
			ty = size.y-fonte.height+fonte.ascent;
			break;
		case Align.VCENTER:
			ty = (size.y-fonte.height)/2+fonte.ascent;
			break;
		default:
			ty = fonte.ascent;
		}

		auto glyphs = _textFont.scaledFont.text_to_glyphs (tx, ty, caption);
		ctx.show_glyphs(glyphs);
	}
}

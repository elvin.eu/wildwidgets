/******************************************************************************

write dependencies of source to stdout

usage:

ddep file.d directory1 directory2 ...

*****************************************************************************/

import std.regex;
import std.getopt;
import std.stdio : write, writeln, readln;
import std.file:readText, exists;
import std.string : translate, tr;

void main (string[] args)
{
	// modules[0]: flag
	// modules[1]: module name as from import statement
	// modules[2]: full path and filename of module
	string[3][] modules_data;

	string[] directories;

	void find_imports (string filename)
	{
		string text = readText (filename);
		// remove comments
		text = replaceAll (text, regex(r"\/\*.*?\*\/", "s"), "");
		text = replaceAll (text, regex(r"\/\+.*?\+\/", "s"), "");
		text = replaceAll (text, regex(r"\/\/.*$", "m"), "");

		// find all imports : result is a string[][3]
		auto imports = matchAll (text, regex (r"(^[ |\t]*import\s*)([\w|\.]*)", "m"));
		// to list all imported modules, type:
		// foreach (im; imports) writeln (im[2]);

		// create an array of [3][], where [0] holds a flag, [1] the directory and [2] holds
		// the module's file name and module path as from the import statement

		foreach (im; imports)
		{
			bool module_exists = false;
			foreach (mod; modules_data)
			{
				if (im[2] == mod[1])
				{
					module_exists = true;
					break;
				}
			}

			if (module_exists == true) continue;

			foreach (dir; directories)
			{
				string path = dir ~ "/" ~ tr(im[2], ".", "/") ~ ".d";

				if (exists(path))
				{
					modules_data ~= ["", im[2], path];
					break;
				}
			}

		}
	}

	foreach (arg; args[2..$])
	{
		directories ~= tr(arg, "/", "/", "s");
	}

	modules_data ~= ["", "", args[1]];

	bool done;
	while (!done)
	{
		foreach (ref mod; modules_data)
		{
			done = true;
			if (mod[0] == "")
			{
				done = false;
				mod[0] = "x";
				find_imports (mod[2]);
				break;
			}
		}
	}

	foreach (mod; modules_data) write (mod[2] ~ " ");
}

/******************************************************************************

d resource precompiler
generates d source from binary files and writes the result to stdout
usage: drpc <input_file> ...

for each input file, source code for  array of ubytes is generated,
e.g.

drpc image1.png image2.jpg

generates the following code:

ubyte[] image1_png = [byte1, byte2, ... byteN];
ubyte[] image2_jpg = [byte1, byte2, ... byteN];

******************************************************************************/

import std.stdio;
import std.string;
import std.path;
import std.exception;

int main (string[] args)
{
	if (args.length < 2)
    {
        writefln ("Usage: %s <file1> <file2> .. <fileN>", args[0]);
		writeln();
		writeln ("Writes files as d source code to stdout");

        return 1;
    }

	foreach (arg; args[1..$])
	{
		string vname = arg.baseName.translate(['.' : '_']);

		writefile (arg, vname);
	}
    return 0;
}

bool writefile (string iname, string vname)
{
    File infile;
    try
    {
        infile = File (iname, "r");
    }
    catch (ErrnoException)
    {
        writefln ("Could not open file %s for reading; skipping.", iname);
        return false;
    }

	int column;
	int count;

	write ("const ubyte [] "~vname~" =\n[   ");

	foreach (ubyte[] buffer; chunks (infile, 1))
	{
        ubyte b = buffer[0];

		if (count) write (",");
		else       count=1;

		if (column > 120)
		{
			writef ("\n    %u", b);
			column=0;
		}
		else
		{
			writef ("%u", b);
		}

		if      (b < 10)  column += 2;
		else if (b < 100) column += 3;
		else              column += 4;
    }
	writeln ("\n];\n");
    return true;
}

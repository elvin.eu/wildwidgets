module X11.Xlib;

import core.stdc.config;
import core.stdc.stddef;

public import X11.Xdefs;
public import X11.X;

extern (C)
{

@nogc:
nothrow:

	enum XlibSpecificationRelease = 6;
	int _Xmblen (char* str, int len);
	alias XPointer = char*;
	struct _XExtData
	{
		int number;
		_XExtData* next;
		int function (_XExtData* extension) free_private;
		XPointer private_data;
	}
	alias XExtData = _XExtData;
	struct XExtCodes
	{
		int extension;
		int major_opcode;
		int first_event;
		int first_error;
	}
	struct XPixmapFormatValues
	{
		int depth;
		int bits_per_pixel;
		int scanline_pad;
	}
	struct XGCValues
	{
		int function_;
		c_ulong plane_mask;
		c_ulong foreground;
		c_ulong background;
		int line_width;
		int line_style;
		int cap_style; /* CapNotLast, CapButt,
						   CapRound, CapProjecting */
		int join_style;
		int fill_style; /* FillSolid, FillTiled,
						   FillStippled, FillOpaeueStippled */
		int fill_rule;
		int arc_mode;
		Pixmap tile;
		Pixmap stipple;
		int ts_x_origin;
		int ts_y_origin;
		Font font;
		int subwindow_mode;
		int graphics_exposures;
		int clip_x_origin;
		int clip_y_origin;
		Pixmap clip_mask;
		int dash_offset;
		char dashes;
	}
	struct _XGC
	{
		XExtData *ext_data;
		GContext gid;
	}

	alias GC = _XGC*;

	struct Visual
	{
		XExtData* ext_data;
		VisualID visualid;
		int class_;
		c_ulong red_mask;
		c_ulong green_mask;
		c_ulong blue_mask;
		int bits_per_rgb;
		int map_entries;
	}
	struct Depth
	{
		int depth;
		int nvisuals;
		Visual* visuals;
	}
	struct Screen
	{
		XExtData* ext_data;
		Display* display;
		Window root;
		int width;
		int height;
		int mwidth;
		int mheight;
		int ndepths;
		Depth* depths;
		int root_depth;
		Visual* root_visual;
		GC default_gc;
		Colormap cmap;
		c_ulong white_pixel;
		c_ulong black_pixel;
		int max_maps;
		int min_maps;
		int backing_store;
		int save_unders;
		c_long root_input_mask;
	}
	struct ScreenFormat
	{
		XExtData* ext_data;
		int depth;
		int bits_per_pixel;
		int scanline_pad;
	}
	struct XSetWindowAttributes
	{
		Pixmap background_pixmap;
		c_ulong background_pixel;
		Pixmap border_pixmap;
		c_ulong border_pixel;
		int bit_gravity;
		int win_gravity;
		int backing_store;
		c_ulong backing_planes;
		c_ulong backing_pixel;
		int save_under;
		c_long event_mask;
		c_long do_not_propagate_mask;
		int override_redirect;
		Colormap colormap;
		Cursor cursor;
	}
	struct XWindowAttributes
	{
		int x;
		int y;
		int width;
		int height;
		int border_width;
		int depth;
		Visual* visual;
		Window root;
		int class_;
		int bit_gravity;
		int win_gravity;
		int backing_store;
		c_ulong backing_planes;
		c_ulong backing_pixel;
		int save_under;
		Colormap colormap;
		int map_installed;
		int map_state;
		c_long all_event_masks;
		c_long your_event_mask;
		c_long do_not_propagate_mask;
		int override_redirect;
		Screen* screen;
	}
	struct XHostAddress
	{
		int family;
		int length;
		char* address;
	}
	struct XServerInterpretedAddress
	{
		int typelength;
		int valuelength;
		char* type;
		char* value;
	}
	struct _XImage
	{
		int width;
		int height;
		int xoffset;
		int format;
		char* data;
		int byte_order;
		int bitmap_unit;
		int bitmap_bit_order;
		int bitmap_pad;
		int depth;
		int bytes_per_line;
		int bits_per_pixel;
		c_ulong red_mask;
		c_ulong green_mask;
		c_ulong blue_mask;
		XPointer obdata;
		struct funcs
		{
			_XImage* function (
				Display*,
				Visual*,
				uint,
				int,
				int,
				char*,
				uint,
				uint,
				int,
				int) create_image;
			int function (_XImage*) destroy_image;
			c_ulong function (_XImage*, int, int) get_pixel;
			int function (_XImage*, int, int, c_ulong) put_pixel;
			_XImage* function (_XImage*, int, int, uint, uint) sub_image;
			int function (_XImage*, c_long) add_pixel;
		}
		funcs f;
	}
	alias XImage = _XImage;
	struct XWindowChanges
	{
		int x;
		int y;
		int width;
		int height;
		int border_width;
		Window sibling;
		int stack_mode;
	}
	struct XColor
	{
		c_ulong pixel;
		ushort red;
		ushort green;
		ushort blue;
		char flags;
		char pad;
	}
	struct XSegment
	{
		short x1;
		short y1;
		short x2;
		short y2;
	}
	struct XPoint
	{
		short x;
		short y;
	}
	struct XRectangle
	{
		short x;
		short y;
		ushort width;
		ushort height;
	}
	struct XArc
	{
		short x;
		short y;
		ushort width;
		ushort height;
		short angle1;
		short angle2;
	}
	struct XKeyboardControl
	{
		int key_click_percent;
		int bell_percent;
		int bell_pitch;
		int bell_duration;
		int led;
		int led_mode;
		int key;
		int auto_repeat_mode;
	}
	struct XKeyboardState
	{
		int key_click_percent;
		int bell_percent;
		uint bell_pitch;
		uint bell_duration;
		c_ulong led_mask;
		int global_auto_repeat;
		char[32] auto_repeats;
	}
	struct XTimeCoord
	{
		Time time;
		short x;
		short y;
	}
	struct XModifierKeymap
	{
		int max_keypermod;
		KeyCode* modifiermap;
	}

	struct _XPrivate;
	struct _XrmHashBucketRec;
	struct Display
	{
		XExtData* ext_data;
		_XPrivate* private1;
		int fd;
		int private2;
		int proto_major_version;
		int proto_minor_version;
		char* vendor;
		XID private3;
		XID private4;
		XID private5;
		int private6;
		XID function (Display*) resource_alloc;
		int byte_order;
		int bitmap_unit;
		int bitmap_pad;
		int bitmap_bit_order;
		int nformats;
		ScreenFormat* pixmap_format;
		int private8;
		int release;
		_XPrivate* private9;
		_XPrivate* private10;
		int qlen;
		c_ulong last_request_read;
		c_ulong request;
		XPointer private11;
		XPointer private12;
		XPointer private13;
		XPointer private14;
		uint max_request_size;
		_XrmHashBucketRec* db;
		int function (Display*) private15;
		char* display_name;
		int default_screen;
		int nscreens;
		Screen* screens;
		c_ulong motion_buffer;
		c_ulong private16;
		int min_keycode;
		int max_keycode;
		XPointer private17;
		XPointer private18;
		int private19;
		char* xdefaults;
	}
	struct XKeyEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		Window root;
		Window subwindow;
		Time time;
		int x;
		int y;
		int x_root;
		int y_root;
		uint state;
		uint keycode;
		int same_screen;
	}
	alias XKeyPressedEvent = XKeyEvent;
	alias XKeyReleasedEvent = XKeyEvent;
	struct XButtonEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		Window root;
		Window subwindow;
		Time time;
		int x;
		int y;
		int x_root;
		int y_root;
		uint state;
		uint button;
		int same_screen;
	}
	alias XButtonPressedEvent = XButtonEvent;
	alias XButtonReleasedEvent = XButtonEvent;
	struct XMotionEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		Window root;
		Window subwindow;
		Time time;
		int x;
		int y;
		int x_root;
		int y_root;
		uint state;
		char is_hint;
		int same_screen;
	}
	alias XPointerMovedEvent = XMotionEvent;
	struct XCrossingEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		Window root;
		Window subwindow;
		Time time;
		int x;
		int y;
		int x_root;
		int y_root;
		int mode;
		int detail;
		/*
			 * NotifyAncestor, NotifyVirtual, NotifyInferior,
			 * NotifyNonlinear,NotifyNonlinearVirtual
			 */
		int same_screen;
		int focus;
		uint state;
	}
	alias XEnterWindowEvent = XCrossingEvent;
	alias XLeaveWindowEvent = XCrossingEvent;
	struct XFocusChangeEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		int mode; /* NotifyNormal, NotifyWhileGrabbed,
						   NotifyGrab, NotifyUngrab */
		int detail;
	}
	alias XFocusInEvent = XFocusChangeEvent;
	alias XFocusOutEvent = XFocusChangeEvent;
	struct XKeymapEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		char[32] key_vector;
	}
	struct XExposeEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		int x;
		int y;
		int width;
		int height;
		int count;
	}
	struct XGraphicsExposeEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Drawable drawable;
		int x;
		int y;
		int width;
		int height;
		int count;
		int major_code;
		int minor_code;
	}
	struct XNoExposeEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Drawable drawable;
		int major_code;
		int minor_code;
	}
	struct XVisibilityEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		int state;
	}
	struct XCreateWindowEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window parent;
		Window window;
		int x;
		int y;
		int width;
		int height;
		int border_width;
		int override_redirect;
	}
	struct XDestroyWindowEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window event;
		Window window;
	}
	struct XUnmapEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window event;
		Window window;
		int from_configure;
	}
	struct XMapEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window event;
		Window window;
		int override_redirect;
	}
	struct XMapRequestEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window parent;
		Window window;
	}
	struct XReparentEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window event;
		Window window;
		Window parent;
		int x;
		int y;
		int override_redirect;
	}
	struct XConfigureEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window event;
		Window window;
		int x;
		int y;
		int width;
		int height;
		int border_width;
		Window above;
		int override_redirect;
	}
	struct XGravityEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window event;
		Window window;
		int x;
		int y;
	}
	struct XResizeRequestEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		int width;
		int height;
	}
	struct XConfigureRequestEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window parent;
		Window window;
		int x;
		int y;
		int width;
		int height;
		int border_width;
		Window above;
		int detail;
		c_ulong value_mask;
	}
	struct XCirculateEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window event;
		Window window;
		int place;
	}
	struct XCirculateRequestEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window parent;
		Window window;
		int place;
	}
	struct XPropertyEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		Atom atom;
		Time time;
		int state;
	}
	struct XSelectionClearEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		Atom selection;
		Time time;
	}
	struct XSelectionRequestEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window owner;
		Window requestor;
		Atom selection;
		Atom target;
		Atom property;
		Time time;
	}
	struct XSelectionEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window requestor;
		Atom selection;
		Atom target;
		Atom property;
		Time time;
	}
	struct XColormapEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		Colormap colormap;
		int new_;
		int state;
	}
	struct XClientMessageEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		Atom message_type;
		int format;
		union _Anonymous_0
		{
			char[20] b;
			short[10] s;
			c_long[5] l;
		}
		_Anonymous_0 data;
	}
	struct XMappingEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
		int request; /* one of MappingModifier, MappingKeyboard,
						   MappingPointer */
		int first_keycode;
		int count;
	}
	struct XErrorEvent
	{
		int type;
		Display* display;
		XID resourceid;
		c_ulong serial;
		ubyte error_code;
		ubyte request_code;
		ubyte minor_code;
	}
	struct XAnyEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		Window window;
	}
	struct XGenericEvent
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		int extension;
		int evtype;
	}
	struct XGenericEventCookie
	{
		int type;
		c_ulong serial;
		int send_event;
		Display* display;
		int extension;
		int evtype;
		uint cookie;
		void* data;
	}
	union _XEvent
	{
		int type;
		XAnyEvent xany;
		XKeyEvent xkey;
		XButtonEvent xbutton;
		XMotionEvent xmotion;
		XCrossingEvent xcrossing;
		XFocusChangeEvent xfocus;
		XExposeEvent xexpose;
		XGraphicsExposeEvent xgraphicsexpose;
		XNoExposeEvent xnoexpose;
		XVisibilityEvent xvisibility;
		XCreateWindowEvent xcreatewindow;
		XDestroyWindowEvent xdestroywindow;
		XUnmapEvent xunmap;
		XMapEvent xmap;
		XMapRequestEvent xmaprequest;
		XReparentEvent xreparent;
		XConfigureEvent xconfigure;
		XGravityEvent xgravity;
		XResizeRequestEvent xresizerequest;
		XConfigureRequestEvent xconfigurerequest;
		XCirculateEvent xcirculate;
		XCirculateRequestEvent xcirculaterequest;
		XPropertyEvent xproperty;
		XSelectionClearEvent xselectionclear;
		XSelectionRequestEvent xselectionrequest;
		XSelectionEvent xselection;
		XColormapEvent xcolormap;
		XClientMessageEvent xclient;
		XMappingEvent xmapping;
		XErrorEvent xerror;
		XKeymapEvent xkeymap;
		XGenericEvent xgeneric;
		XGenericEventCookie xcookie;
		c_long[24] pad;
	}
	alias XEvent = _XEvent;
	struct XCharStruct
	{
		short lbearing;
		short rbearing;
		short width;
		short ascent;
		short descent;
		ushort attributes;
	}
	struct XFontProp
	{
		Atom name;
		c_ulong card32;
	}
	struct XFontStruct
	{
		XExtData* ext_data;
		Font fid;
		uint direction;
		uint min_char_or_byte2;
		uint max_char_or_byte2;
		uint min_byte1;
		uint max_byte1;
		int all_chars_exist;
		uint default_char;
		int n_properties;
		XFontProp* properties;
		XCharStruct min_bounds;
		XCharStruct max_bounds;
		XCharStruct* per_char;
		int ascent;
		int descent;
	}
	struct XTextItem
	{
		char* chars;
		int nchars;
		int delta;
		Font font;
	}
	struct XChar2b
	{
		ubyte byte1;
		ubyte byte2;
	}
	struct XTextItem16
	{
		XChar2b* chars;
		int nchars;
		int delta;
		Font font;
	}
	union XEDataObject
	{
		Display* display;
		GC gc;
		Visual* visual;
		Screen* screen;
		ScreenFormat* pixmap_format;
		XFontStruct* font;
	}
	struct XFontSetExtents
	{
		XRectangle max_ink_extent;
		XRectangle max_logical_extent;
	}
	struct _XOM;
	alias XOM = _XOM*;
	struct _XOC;
	alias XOC = _XOC*;
	alias XFontSet = _XOC*;
	struct XmbTextItem
	{
		char* chars;
		int nchars;
		int delta;
		XFontSet font_set;
	}
	struct XwcTextItem
	{
		wchar_t* chars;
		int nchars;
		int delta;
		XFontSet font_set;
	}
	struct XOMCharSetList
	{
		int charset_count;
		char** charset_list;
	}
	enum XOrientation
	{
		XOMOrientation_LTR_TTB = 0,
		XOMOrientation_RTL_TTB = 1,
		XOMOrientation_TTB_LTR = 2,
		XOMOrientation_TTB_RTL = 3,
		XOMOrientation_Context = 4
	}
	struct XOMOrientation
	{
		int num_orientation;
		XOrientation* orientation;
	}
	struct XOMFontInfo
	{
		int num_font;
		XFontStruct** font_struct_list;
		char** font_name_list;
	}
	struct _XIM;
	alias XIM = _XIM*;
	struct _XIC;
	alias XIC = _XIC*;
	alias XIMProc = void function (XIM, XPointer, XPointer);
	alias XICProc = int function (XIC, XPointer, XPointer);
	alias XIDProc = void function (Display*, XPointer, XPointer);
	alias XIMStyle = c_ulong;
	struct XIMStyles
	{
		ushort count_styles;
		XIMStyle* supported_styles;
	}
	alias XVaNestedList = void*;
	struct XIMCallback
	{
		XPointer client_data;
		XIMProc callback;
	}
	struct XICCallback
	{
		XPointer client_data;
		XICProc callback;
	}
	alias XIMFeedback = c_ulong;
	struct _XIMText
	{
		ushort length;
		XIMFeedback* feedback;
		int encoding_is_wchar;
		union _Anonymous_1
		{
			char* multi_byte;
			wchar_t* wide_char;
		}
		_Anonymous_1 string;
	}
	alias XIMText = _XIMText;
	alias XIMPreeditState = c_ulong;
	struct _XIMPreeditStateNotifyCallbackStruct
	{
		XIMPreeditState state;
	}
	alias XIMPreeditStateNotifyCallbackStruct = _XIMPreeditStateNotifyCallbackStruct;
	alias XIMResetState = c_ulong;
	alias XIMStringConversionFeedback = c_ulong;
	struct _XIMStringConversionText
	{
		ushort length;
		XIMStringConversionFeedback* feedback;
		int encoding_is_wchar;
		union _Anonymous_2
		{
			char* mbs;
			wchar_t* wcs;
		}
		_Anonymous_2 string;
	}
	alias XIMStringConversionText = _XIMStringConversionText;
	alias XIMStringConversionPosition = ushort;
	alias XIMStringConversionType = ushort;
	alias XIMStringConversionOperation = ushort;
	enum XIMCaretDirection
	{
		XIMForwardChar = 0,
		XIMBackwardChar = 1,
		XIMForwardWord = 2,
		XIMBackwardWord = 3,
		XIMCaretUp = 4,
		XIMCaretDown = 5,
		XIMNextLine = 6,
		XIMPreviousLine = 7,
		XIMLineStart = 8,
		XIMLineEnd = 9,
		XIMAbsolutePosition = 10,
		XIMDontChange = 11
	}
	struct _XIMStringConversionCallbackStruct
	{
		XIMStringConversionPosition position;
		XIMCaretDirection direction;
		XIMStringConversionOperation operation;
		ushort factor;
		XIMStringConversionText* text;
	}
	alias XIMStringConversionCallbackStruct = _XIMStringConversionCallbackStruct;
	struct _XIMPreeditDrawCallbackStruct
	{
		int caret;
		int chg_first;
		int chg_length;
		XIMText* text;
	}
	alias XIMPreeditDrawCallbackStruct = _XIMPreeditDrawCallbackStruct;
	enum XIMCaretStyle
	{
		XIMIsInvisible = 0,
		XIMIsPrimary = 1,
		XIMIsSecondary = 2
	}
	struct _XIMPreeditCaretCallbackStruct
	{
		int position;
		XIMCaretDirection direction;
		XIMCaretStyle style;
	}
	alias XIMPreeditCaretCallbackStruct = _XIMPreeditCaretCallbackStruct;
	enum XIMStatusDataType
	{
		XIMTextType = 0,
		XIMBitmapType = 1
	}
	struct _XIMStatusDrawCallbackStruct
	{
		XIMStatusDataType type;
		union _Anonymous_3
		{
			XIMText* text;
			Pixmap bitmap;
		}
		_Anonymous_3 data;
	}
	alias XIMStatusDrawCallbackStruct = _XIMStatusDrawCallbackStruct;
	struct _XIMHotKeyTrigger
	{
		KeySym keysym;
		int modifier;
		int modifier_mask;
	}
	alias XIMHotKeyTrigger = _XIMHotKeyTrigger;
	struct _XIMHotKeyTriggers
	{
		int num_hot_key;
		XIMHotKeyTrigger* key;
	}
	alias XIMHotKeyTriggers = _XIMHotKeyTriggers;
	alias XIMHotKeyState = c_ulong;
	struct XIMValuesList
	{
		ushort count_values;
		char** supported_values;
	}
	extern __gshared int _Xdebug;
	XFontStruct* XLoadQueryFont (Display*, const(char)*);
	XFontStruct* XQueryFont (Display*, XID);
	XTimeCoord* XGetMotionEvents (Display*, Window, Time, Time, int*);
	XModifierKeymap* XDeleteModifiermapEntry (XModifierKeymap*, KeyCode, int);
	XModifierKeymap* XGetModifierMapping (Display*);
	XModifierKeymap* XInsertModifiermapEntry (XModifierKeymap*, KeyCode, int);
	XModifierKeymap* XNewModifiermap (int);
	XImage* XCreateImage (
		Display*,
		Visual*,
		uint,
		int,
		int,
		char*,
		uint,
		uint,
		int,
		int);
	int XInitImage (XImage*);
	XImage* XGetImage (Display*, Drawable, int, int, uint, uint, c_ulong, int);
	XImage* XGetSubImage (
		Display*,
		Drawable,
		int,
		int,
		uint,
		uint,
		c_ulong,
		int,
		XImage*,
		int,
		int);
	Display* XOpenDisplay (const(char)*);
	void XrmInitialize ();
	char* XFetchBytes (Display*, int*);
	char* XFetchBuffer (Display*, int*, int);
	char* XGetAtomName (Display*, Atom);
	int XGetAtomNames (Display*, Atom*, int, char**);
	char* XGetDefault (Display*, const(char)*, const(char)*);
	char* XDisplayName (const(char)*);
	char* XKeysymToString (KeySym);
	int function (Display*, Display*, int) XSynchronize (Display*, Display*, int);
	int function (Display*, Display*, int function (Display*)) XSetAfterFunction (
		Display*,
		Display*,
		int function (Display*));
	Atom XInternAtom (Display*, const(char)*, int);
	int XInternAtoms (Display*, char**, int, int, Atom*);
	Colormap XCopyColormapAndFree (Display*, Colormap);
	Colormap XCreateColormap (Display*, Window, Visual*, int);
	Cursor XCreatePixmapCursor (
		Display*,
		Pixmap,
		Pixmap,
		XColor*,
		XColor*,
		uint,
		uint);
	Cursor XCreateGlyphCursor (
		Display*,
		Font,
		Font,
		uint,
		uint,
		const(XColor)*,
		const(XColor)*);
	Cursor XCreateFontCursor (Display*, uint);
	Font XLoadFont (Display*, const(char)*);
	GC XCreateGC (Display*, Drawable, c_ulong, XGCValues*);
	GContext XGContextFromGC (GC);
	void XFlushGC (Display*, GC);
	Pixmap XCreatePixmap (Display*, Drawable, uint, uint, uint);
	Pixmap XCreateBitmapFromData (Display*, Drawable, const(char)*, uint, uint);
	Pixmap XCreatePixmapFromBitmapData (
		Display*,
		Drawable,
		char*,
		uint,
		uint,
		c_ulong,
		c_ulong,
		uint);
	Window XCreateSimpleWindow (
		Display*,
		Window,
		int,
		int,
		uint,
		uint,
		uint,
		c_ulong,
		c_ulong);
	Window XGetSelectionOwner (Display*, Atom);
	Window XCreateWindow (
		Display*,
		Window,
		int,
		int,
		uint,
		uint,
		uint,
		int,
		uint,
		Visual*,
		c_ulong,
		XSetWindowAttributes*);
	Colormap* XListInstalledColormaps (Display*, Window, int*);
	char** XListFonts (Display*, const(char)*, int, int*);
	char** XListFontsWithInfo (Display*, const(char)*, int, int*, XFontStruct**);
	char** XGetFontPath (Display*, int*);
	char** XListExtensions (Display*, int*);
	Atom* XListProperties (Display*, Window, int*);
	XHostAddress* XListHosts (Display*, int*, int*);
	KeySym XKeycodeToKeysym (Display*, KeyCode, int);
	KeySym XLookupKeysym (XKeyEvent*, int);
	KeySym* XGetKeyboardMapping (Display*, KeyCode, int, int*);
	KeySym XStringToKeysym (const(char)*);
	c_long XMaxRequestSize (Display*);
	c_long XExtendedMaxRequestSize (Display*);
	char* XResourceManagerString (Display*);
	char* XScreenResourceString (Screen*);
	c_ulong XDisplayMotionBufferSize (Display*);
	VisualID XVisualIDFromVisual (Visual*);
	int XInitThreads ();
	void XLockDisplay (Display*);
	void XUnlockDisplay (Display*);
	XExtCodes* XInitExtension (Display*, const(char)*);
	XExtCodes* XAddExtension (Display*);
	XExtData* XFindOnExtensionList (XExtData**, int);
	XExtData** XEHeadOfExtensionList (XEDataObject);
	Window XRootWindow (Display*, int);
	Window XDefaultRootWindow (Display*);
	Window XRootWindowOfScreen (Screen*);
	Visual* XDefaultVisual (Display*, int);
	Visual* XDefaultVisualOfScreen (Screen*);
	GC XDefaultGC (Display*, int);
	GC XDefaultGCOfScreen (Screen*);
	c_ulong XBlackPixel (Display*, int);
	c_ulong XWhitePixel (Display*, int);
	c_ulong XAllPlanes ();
	c_ulong XBlackPixelOfScreen (Screen*);
	c_ulong XWhitePixelOfScreen (Screen*);
	c_ulong XNextRequest (Display*);
	c_ulong XLastKnownRequestProcessed (Display*);
	char* XServerVendor (Display*);
	char* XDisplayString (Display*);
	Colormap XDefaultColormap (Display*, int);
	Colormap XDefaultColormapOfScreen (Screen*);
	Display* XDisplayOfScreen (Screen*);
	Screen* XScreenOfDisplay (Display*, int);
	Screen* XDefaultScreenOfDisplay (Display*);
	c_long XEventMaskOfScreen (Screen*);
	int XScreenNumberOfScreen (Screen*);
	alias XErrorHandler = int function (Display*, XErrorEvent*);
	XErrorHandler XSetErrorHandler (XErrorHandler);
	alias XIOErrorHandler = int function (Display*);
	XIOErrorHandler XSetIOErrorHandler (XIOErrorHandler);
	XPixmapFormatValues* XListPixmapFormats (Display*, int*);
	int* XListDepths (Display*, int, int*);
	int XReconfigureWMWindow (Display*, Window, int, uint, XWindowChanges*);
	int XGetWMProtocols (Display*, Window, Atom**, int*);
	int XSetWMProtocols (Display*, Window, Atom*, int);
	int XIconifyWindow (Display*, Window, int);
	int XWithdrawWindow (Display*, Window, int);
	int XGetCommand (Display*, Window, char***, int*);
	int XGetWMColormapWindows (Display*, Window, Window**, int*);
	int XSetWMColormapWindows (Display*, Window, Window*, int);
	void XFreeStringList (char**);
	int XSetTransientForHint (Display*, Window, Window);
	int XActivateScreenSaver (Display*);
	int XAddHost (Display*, XHostAddress*);
	int XAddHosts (Display*, XHostAddress*, int);
	int XAddToExtensionList (_XExtData**, XExtData*);
	int XAddToSaveSet (Display*, Window);
	int XAllocColor (Display*, Colormap, XColor*);
	int XAllocColorCells (Display*, Colormap, int, c_ulong*, uint, c_ulong*, uint);
	int XAllocColorPlanes (
		Display*,
		Colormap,
		int,
		c_ulong*,
		int,
		int,
		int,
		int,
		c_ulong*,
		c_ulong*,
		c_ulong*);
	int XAllocNamedColor (Display*, Colormap, const(char)*, XColor*, XColor*);
	int XAllowEvents (Display*, int, Time);
	int XAutoRepeatOff (Display*);
	int XAutoRepeatOn (Display*);
	int XBell (Display*, int);
	int XBitmapBitOrder (Display*);
	int XBitmapPad (Display*);
	int XBitmapUnit (Display*);
	int XCellsOfScreen (Screen*);
	int XChangeActivePointerGrab (Display*, uint, Cursor, Time);
	int XChangeGC (Display*, GC, c_ulong, XGCValues*);
	int XChangeKeyboardControl (Display*, c_ulong, XKeyboardControl*);
	int XChangeKeyboardMapping (Display*, int, int, KeySym*, int);
	int XChangePointerControl (Display*, int, int, int, int, int);
	int XChangeProperty (
		Display*,
		Window,
		Atom,
		Atom,
		int,
		int,
		const(ubyte)*,
		int);
	int XChangeSaveSet (Display*, Window, int);
	int XChangeWindowAttributes (Display*, Window, c_ulong, XSetWindowAttributes*);
	int XCheckIfEvent (
		Display*,
		XEvent*,
		int function (Display*, XEvent*, XPointer),
		XPointer);
	int XCheckMaskEvent (Display*, c_long, XEvent*);
	int XCheckTypedEvent (Display*, int, XEvent*);
	int XCheckTypedWindowEvent (Display*, Window, int, XEvent*);
	int XCheckWindowEvent (Display*, Window, c_long, XEvent*);
	int XCirculateSubwindows (Display*, Window, int);
	int XCirculateSubwindowsDown (Display*, Window);
	int XCirculateSubwindowsUp (Display*, Window);
	int XClearArea (Display*, Window, int, int, uint, uint, int);
	int XClearWindow (Display*, Window);
	int XCloseDisplay (Display*);
	int XConfigureWindow (Display*, Window, uint, XWindowChanges*);
	int XConnectionNumber (Display*);
	int XConvertSelection (Display*, Atom, Atom, Atom, Window, Time);
	int XCopyArea (
		Display*,
		Drawable,
		Drawable,
		GC,
		int,
		int,
		uint,
		uint,
		int,
		int);
	int XCopyGC (Display*, GC, c_ulong, GC);
	int XCopyPlane (
		Display*,
		Drawable,
		Drawable,
		GC,
		int,
		int,
		uint,
		uint,
		int,
		int,
		c_ulong);
	int XDefaultDepth (Display*, int);
	int XDefaultDepthOfScreen (Screen*);
	int XDefaultScreen (Display*);
	int XDefineCursor (Display*, Window, Cursor);
	int XDeleteProperty (Display*, Window, Atom);
	int XDestroyWindow (Display*, Window);
	int XDestroySubwindows (Display*, Window);
	int XDoesBackingStore (Screen*);
	int XDoesSaveUnders (Screen*);
	int XDisableAccessControl (Display*);
	int XDisplayCells (Display*, int);
	int XDisplayHeight (Display*, int);
	int XDisplayHeightMM (Display*, int);
	int XDisplayKeycodes (Display*, int*, int*);
	int XDisplayPlanes (Display*, int);
	int XDisplayWidth (Display*, int);
	int XDisplayWidthMM (Display*, int);
	int XDrawArc (Display*, Drawable, GC, int, int, uint, uint, int, int);
	int XDrawArcs (Display*, Drawable, GC, XArc*, int);
	int XDrawImageString (Display*, Drawable, GC, int, int, const(char)*, int);
	int XDrawImageString16 (Display*, Drawable, GC, int, int, const(XChar2b)*, int);
	int XDrawLine (Display*, Drawable, GC, int, int, int, int);
	int XDrawLines (Display*, Drawable, GC, XPoint*, int, int);
	int XDrawPoint (Display*, Drawable, GC, int, int);
	int XDrawPoints (Display*, Drawable, GC, XPoint*, int, int);
	int XDrawRectangle (Display*, Drawable, GC, int, int, uint, uint);
	int XDrawRectangles (Display*, Drawable, GC, XRectangle*, int);
	int XDrawSegments (Display*, Drawable, GC, XSegment*, int);
	int XDrawString (Display*, Drawable, GC, int, int, const(char)*, int);
	int XDrawString16 (Display*, Drawable, GC, int, int, const(XChar2b)*, int);
	int XDrawText (Display*, Drawable, GC, int, int, XTextItem*, int);
	int XDrawText16 (Display*, Drawable, GC, int, int, XTextItem16*, int);
	int XEnableAccessControl (Display*);
	int XEventsQueued (Display*, int);
	int XFetchName (Display*, Window, char**);
	int XFillArc (Display*, Drawable, GC, int, int, uint, uint, int, int);
	int XFillArcs (Display*, Drawable, GC, XArc*, int);
	int XFillPolygon (Display*, Drawable, GC, XPoint*, int, int, int);
	int XFillRectangle (Display*, Drawable, GC, int, int, uint, uint);
	int XFillRectangles (Display*, Drawable, GC, XRectangle*, int);
	int XFlush (Display*);
	int XForceScreenSaver (Display*, int);
	int XFree (void*);
	int XFreeColormap (Display*, Colormap);
	int XFreeColors (Display*, Colormap, c_ulong*, int, c_ulong);
	int XFreeCursor (Display*, Cursor);
	int XFreeExtensionList (char**);
	int XFreeFont (Display*, XFontStruct*);
	int XFreeFontInfo (char**, XFontStruct*, int);
	int XFreeFontNames (char**);
	int XFreeFontPath (char**);
	int XFreeGC (Display*, GC);
	int XFreeModifiermap (XModifierKeymap*);
	int XFreePixmap (Display*, Pixmap);
	int XGeometry (
		Display*,
		int,
		const(char)*,
		const(char)*,
		uint,
		uint,
		uint,
		int,
		int,
		int*,
		int*,
		int*,
		int*);
	int XGetErrorDatabaseText (
		Display*,
		const(char)*,
		const(char)*,
		const(char)*,
		char*,
		int);
	int XGetErrorText (Display*, int, char*, int);
	int XGetFontProperty (XFontStruct*, Atom, c_ulong*);
	int XGetGCValues (Display*, GC, c_ulong, XGCValues*);
	int XGetGeometry (
		Display*,
		Drawable,
		Window*,
		int*,
		int*,
		uint*,
		uint*,
		uint*,
		uint*);
	int XGetIconName (Display*, Window, char**);
	int XGetInputFocus (Display*, Window*, int*);
	int XGetKeyboardControl (Display*, XKeyboardState*);
	int XGetPointerControl (Display*, int*, int*, int*);
	int XGetPointerMapping (Display*, ubyte*, int);
	int XGetScreenSaver (Display*, int*, int*, int*, int*);
	int XGetTransientForHint (Display*, Window, Window*);
	int XGetWindowProperty (
		Display*,
		Window,
		Atom,
		c_long,
		c_long,
		int,
		Atom,
		Atom*,
		int*,
		c_ulong*,
		c_ulong*,
		ubyte**);
	int XGetWindowAttributes (Display*, Window, XWindowAttributes*);
	int XGrabButton (
		Display*,
		uint,
		uint,
		Window,
		int,
		uint,
		int,
		int,
		Window,
		Cursor);
	int XGrabKey (Display*, int, uint, Window, int, int, int);
	int XGrabKeyboard (Display*, Window, int, int, int, Time);
	int XGrabPointer (Display*, Window, int, uint, int, int, Window, Cursor, Time);
	int XGrabServer (Display*);
	int XHeightMMOfScreen (Screen*);
	int XHeightOfScreen (Screen*);
	int XIfEvent (
		Display*,
		XEvent*,
		int function (Display*, XEvent*, XPointer),
		XPointer);
	int XImageByteOrder (Display*);
	int XInstallColormap (Display*, Colormap);
	KeyCode XKeysymToKeycode (Display*, KeySym);
	int XKillClient (Display*, XID);
	int XLookupColor (Display*, Colormap, const(char)*, XColor*, XColor*);
	int XLowerWindow (Display*, Window);
	int XMapRaised (Display*, Window);
	int XMapSubwindows (Display*, Window);
	int XMapWindow (Display*, Window);
	int XMaskEvent (Display*, c_long, XEvent*);
	int XMaxCmapsOfScreen (Screen*);
	int XMinCmapsOfScreen (Screen*);
	int XMoveResizeWindow (Display*, Window, int, int, uint, uint);
	int XMoveWindow (Display*, Window, int, int);
	int XNextEvent (Display*, XEvent*);
	int XNoOp (Display*);
	int XParseColor (Display*, Colormap, const(char)*, XColor*);
	int XParseGeometry (const(char)*, int*, int*, uint*, uint*);
	int XPeekEvent (Display*, XEvent*);
	int XPeekIfEvent (
		Display*,
		XEvent*,
		int function (Display*, XEvent*, XPointer),
		XPointer);
	int XPending (Display*);
	int XPlanesOfScreen (Screen*);
	int XProtocolRevision (Display*);
	int XProtocolVersion (Display*);
	int XPutBackEvent (Display*, XEvent*);
	int XPutImage (Display*, Drawable, GC, XImage*, int, int, int, int, uint, uint);
	int XQLength (Display*);
	int XQueryBestCursor (Display*, Drawable, uint, uint, uint*, uint*);
	int XQueryBestSize (Display*, int, Drawable, uint, uint, uint*, uint*);
	int XQueryBestStipple (Display*, Drawable, uint, uint, uint*, uint*);
	int XQueryBestTile (Display*, Drawable, uint, uint, uint*, uint*);
	int XQueryColor (Display*, Colormap, XColor*);
	int XQueryColors (Display*, Colormap, XColor*, int);
	int XQueryExtension (Display*, const(char)*, int*, int*, int*);
	int XQueryKeymap (Display*, ref char[32]);
	int XQueryPointer (
		Display*,
		Window,
		Window*,
		Window*,
		int*,
		int*,
		int*,
		int*,
		uint*);
	int XQueryTextExtents (
		Display*,
		XID,
		const(char)*,
		int,
		int*,
		int*,
		int*,
		XCharStruct*);
	int XQueryTextExtents16 (
		Display*,
		XID,
		const(XChar2b)*,
		int,
		int*,
		int*,
		int*,
		XCharStruct*);
	int XQueryTree (Display*, Window, Window*, Window*, Window**, uint*);
	int XRaiseWindow (Display*, Window);
	int XReadBitmapFile (
		Display*,
		Drawable,
		const(char)*,
		uint*,
		uint*,
		Pixmap*,
		int*,
		int*);
	int XReadBitmapFileData (const(char)*, uint*, uint*, ubyte**, int*, int*);
	int XRebindKeysym (Display*, KeySym, KeySym*, int, const(ubyte)*, int);
	int XRecolorCursor (Display*, Cursor, XColor*, XColor*);
	int XRefreshKeyboardMapping (XMappingEvent*);
	int XRemoveFromSaveSet (Display*, Window);
	int XRemoveHost (Display*, XHostAddress*);
	int XRemoveHosts (Display*, XHostAddress*, int);
	int XReparentWindow (Display*, Window, Window, int, int);
	int XResetScreenSaver (Display*);
	int XResizeWindow (Display*, Window, uint, uint);
	int XRestackWindows (Display*, Window*, int);
	int XRotateBuffers (Display*, int);
	int XRotateWindowProperties (Display*, Window, Atom*, int, int);
	int XScreenCount (Display*);
	int XSelectInput (Display*, Window, c_long);
	int XSendEvent (Display*, Window, int, c_long, XEvent*);
	int XSetAccessControl (Display*, int);
	int XSetArcMode (Display*, GC, int);
	int XSetBackground (Display*, GC, c_ulong);
	int XSetClipMask (Display*, GC, Pixmap);
	int XSetClipOrigin (Display*, GC, int, int);
	int XSetClipRectangles (Display*, GC, int, int, XRectangle*, int, int);
	int XSetCloseDownMode (Display*, int);
	int XSetCommand (Display*, Window, char**, int);
	int XSetDashes (Display*, GC, int, const(char)*, int);
	int XSetFillRule (Display*, GC, int);
	int XSetFillStyle (Display*, GC, int);
	int XSetFont (Display*, GC, Font);
	int XSetFontPath (Display*, char**, int);
	int XSetForeground (Display*, GC, c_ulong);
	int XSetFunction (Display*, GC, int);
	int XSetGraphicsExposures (Display*, GC, int);
	int XSetIconName (Display*, Window, const(char)*);
	int XSetInputFocus (Display*, Window, int, Time);
	int XSetLineAttributes (Display*, GC, uint, int, int, int);
	int XSetModifierMapping (Display*, XModifierKeymap*);
	int XSetPlaneMask (Display*, GC, c_ulong);
	int XSetPointerMapping (Display*, const(ubyte)*, int);
	int XSetScreenSaver (Display*, int, int, int, int);
	int XSetSelectionOwner (Display*, Atom, Window, Time);
	int XSetState (Display*, GC, c_ulong, c_ulong, int, c_ulong);
	int XSetStipple (Display*, GC, Pixmap);
	int XSetSubwindowMode (Display*, GC, int);
	int XSetTSOrigin (Display*, GC, int, int);
	int XSetTile (Display*, GC, Pixmap);
	int XSetWindowBackground (Display*, Window, c_ulong);
	int XSetWindowBackgroundPixmap (Display*, Window, Pixmap);
	int XSetWindowBorder (Display*, Window, c_ulong);
	int XSetWindowBorderPixmap (Display*, Window, Pixmap);
	int XSetWindowBorderWidth (Display*, Window, uint);
	int XSetWindowColormap (Display*, Window, Colormap);
	int XStoreBuffer (Display*, const(char)*, int, int);
	int XStoreBytes (Display*, const(char)*, int);
	int XStoreColor (Display*, Colormap, XColor*);
	int XStoreColors (Display*, Colormap, XColor*, int);
	int XStoreName (Display*, Window, const(char)*);
	int XStoreNamedColor (Display*, Colormap, const(char)*, c_ulong, int);
	int XSync (Display*, int);
	int XTextExtents (
		XFontStruct*,
		const(char)*,
		int,
		int*,
		int*,
		int*,
		XCharStruct*);
	int XTextExtents16 (
		XFontStruct*,
		const(XChar2b)*,
		int,
		int*,
		int*,
		int*,
		XCharStruct*);
	int XTextWidth (XFontStruct*, const(char)*, int);
	int XTextWidth16 (XFontStruct*, const(XChar2b)*, int);
	int XTranslateCoordinates (
		Display*,
		Window,
		Window,
		int,
		int,
		int*,
		int*,
		Window*);
	int XUndefineCursor (Display*, Window);
	int XUngrabButton (Display*, uint, uint, Window);
	int XUngrabKey (Display*, int, uint, Window);
	int XUngrabKeyboard (Display*, Time);
	int XUngrabPointer (Display*, Time);
	int XUngrabServer (Display*);
	int XUninstallColormap (Display*, Colormap);
	int XUnloadFont (Display*, Font);
	int XUnmapSubwindows (Display*, Window);
	int XUnmapWindow (Display*, Window);
	int XVendorRelease (Display*);
	int XWarpPointer (Display*, Window, Window, int, int, uint, uint, int, int);
	int XWidthMMOfScreen (Screen*);
	int XWidthOfScreen (Screen*);
	int XWindowEvent (Display*, Window, c_long, XEvent*);
	int XWriteBitmapFile (Display*, const(char)*, Pixmap, uint, uint, int, int);
	int XSupportsLocale ();
	char* XSetLocaleModifiers (const(char)*);
	XOM XOpenOM (Display*, _XrmHashBucketRec*, const(char)*, const(char)*);
	int XCloseOM (XOM);
	char* XSetOMValues (XOM, ...);
	char* XGetOMValues (XOM, ...);
	Display* XDisplayOfOM (XOM);
	char* XLocaleOfOM (XOM);
	XOC XCreateOC (XOM, ...);
	void XDestroyOC (XOC);
	XOM XOMOfOC (XOC);
	char* XSetOCValues (XOC, ...);
	char* XGetOCValues (XOC, ...);
	XFontSet XCreateFontSet (Display*, const(char)*, char***, int*, char**);
	void XFreeFontSet (Display*, XFontSet);
	int XFontsOfFontSet (XFontSet, XFontStruct***, char***);
	char* XBaseFontNameListOfFontSet (XFontSet);
	char* XLocaleOfFontSet (XFontSet);
	int XContextDependentDrawing (XFontSet);
	int XDirectionalDependentDrawing (XFontSet);
	int XContextualDrawing (XFontSet);
	XFontSetExtents* XExtentsOfFontSet (XFontSet);
	int XmbTextEscapement (XFontSet, const(char)*, int);
	int XwcTextEscapement (XFontSet, const(wchar_t)*, int);
	int Xutf8TextEscapement (XFontSet, const(char)*, int);
	int XmbTextExtents (XFontSet, const(char)*, int, XRectangle*, XRectangle*);
	int XwcTextExtents (XFontSet, const(wchar_t)*, int, XRectangle*, XRectangle*);
	int Xutf8TextExtents (XFontSet, const(char)*, int, XRectangle*, XRectangle*);
	int XmbTextPerCharExtents (
		XFontSet,
		const(char)*,
		int,
		XRectangle*,
		XRectangle*,
		int,
		int*,
		XRectangle*,
		XRectangle*);
	int XwcTextPerCharExtents (
		XFontSet,
		const(wchar_t)*,
		int,
		XRectangle*,
		XRectangle*,
		int,
		int*,
		XRectangle*,
		XRectangle*);
	int Xutf8TextPerCharExtents (
		XFontSet,
		const(char)*,
		int,
		XRectangle*,
		XRectangle*,
		int,
		int*,
		XRectangle*,
		XRectangle*);
	void XmbDrawText (Display*, Drawable, GC, int, int, XmbTextItem*, int);
	void XwcDrawText (Display*, Drawable, GC, int, int, XwcTextItem*, int);
	void Xutf8DrawText (Display*, Drawable, GC, int, int, XmbTextItem*, int);
	void XmbDrawString (
		Display*,
		Drawable,
		XFontSet,
		GC,
		int,
		int,
		const(char)*,
		int);
	void XwcDrawString (
		Display*,
		Drawable,
		XFontSet,
		GC,
		int,
		int,
		const(wchar_t)*,
		int);
	void Xutf8DrawString (
		Display*,
		Drawable,
		XFontSet,
		GC,
		int,
		int,
		const(char)*,
		int);
	void XmbDrawImageString (
		Display*,
		Drawable,
		XFontSet,
		GC,
		int,
		int,
		const(char)*,
		int);
	void XwcDrawImageString (
		Display*,
		Drawable,
		XFontSet,
		GC,
		int,
		int,
		const(wchar_t)*,
		int);
	void Xutf8DrawImageString (
		Display*,
		Drawable,
		XFontSet,
		GC,
		int,
		int,
		const(char)*,
		int);
	XIM XOpenIM (Display*, _XrmHashBucketRec*, char*, char*);
	int XCloseIM (XIM);
	char* XGetIMValues (XIM, ...);
	char* XSetIMValues (XIM, ...);
	Display* XDisplayOfIM (XIM);
	char* XLocaleOfIM (XIM);
	XIC XCreateIC (XIM, ...);
	void XDestroyIC (XIC);
	void XSetICFocus (XIC);
	void XUnsetICFocus (XIC);
	wchar_t* XwcResetIC (XIC);
	char* XmbResetIC (XIC);
	char* Xutf8ResetIC (XIC);
	char* XSetICValues (XIC, ...);
	char* XGetICValues (XIC, ...);
	XIM XIMOfIC (XIC);
	int XFilterEvent (XEvent*, Window);
	int XmbLookupString (XIC, XKeyPressedEvent*, char*, int, KeySym*, int*);
	int XwcLookupString (XIC, XKeyPressedEvent*, wchar_t*, int, KeySym*, int*);
	int Xutf8LookupString (XIC, XKeyPressedEvent*, char*, int, KeySym*, int*);
	XVaNestedList XVaCreateNestedList (int, ...);
	int XRegisterIMInstantiateCallback (
		Display*,
		_XrmHashBucketRec*,
		char*,
		char*,
		XIDProc,
		XPointer);
	int XUnregisterIMInstantiateCallback (
		Display*,
		_XrmHashBucketRec*,
		char*,
		char*,
		XIDProc,
		XPointer);
	alias XConnectionWatchProc = void function (
		Display*,
		XPointer,
		int,
		int,
		XPointer*);
	int XInternalConnectionNumbers (Display*, int**, int*);
	void XProcessInternalConnection (Display*, int);
	int XAddConnectionWatch (Display*, XConnectionWatchProc, XPointer);
	void XRemoveConnectionWatch (Display*, XConnectionWatchProc, XPointer);
	void XSetAuthorization (char*, int, char*, int);
	int _Xmbtowc (wchar_t*, char*, int);
	int _Xwctomb (char*, wchar_t);
	int XGetEventData (Display*, XGenericEventCookie*);
	void XFreeEventData (Display*, XGenericEventCookie*);
	enum X_HAVE_UTF8_STRING = 1;
	alias Bool = int;
	alias Status = int;
	enum True = 1;
	enum False = 0;
	enum QueuedAlready = 0;
	enum QueuedAfterReading = 1;
	enum QueuedAfterFlush = 2;
	extern (D) auto ConnectionNumber(T)(auto ref T dpy)
	{
		return dpy.fd;
	}
	extern (D) auto RootWindow(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).root;
	}
	extern (D) auto DefaultScreen(T)(auto ref T dpy)
	{
		return dpy.default_screen;
	}
	extern (D) auto DefaultRootWindow(T)(auto ref T dpy)
	{
		return ScreenOfDisplay(dpy, DefaultScreen(dpy)).root;
	}
	extern (D) auto DefaultVisual(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).root_visual;
	}
	extern (D) auto DefaultGC(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).default_gc;
	}
	extern (D) auto BlackPixel(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).black_pixel;
	}
	extern (D) auto WhitePixel(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).white_pixel;
	}
	enum AllPlanes = cast(c_ulong) ~0L;
	extern (D) auto QLength(T)(auto ref T dpy)
	{
		return dpy.qlen;
	}
	extern (D) auto DisplayWidth(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).width;
	}
	extern (D) auto DisplayHeight(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).height;
	}
	extern (D) auto DisplayWidthMM(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).mwidth;
	}
	extern (D) auto DisplayHeightMM(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).mheight;
	}
	extern (D) auto DisplayPlanes(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).root_depth;
	}
	extern (D) auto DisplayCells(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return DefaultVisual(dpy, scr).map_entries;
	}
	extern (D) auto ScreenCount(T)(auto ref T dpy)
	{
		return dpy.nscreens;
	}
	extern (D) auto ServerVendor(T)(auto ref T dpy)
	{
		return dpy.vendor;
	}
	extern (D) auto ProtocolVersion(T)(auto ref T dpy)
	{
		return dpy.proto_major_version;
	}
	extern (D) auto ProtocolRevision(T)(auto ref T dpy)
	{
		return dpy.proto_minor_version;
	}
	extern (D) auto VendorRelease(T)(auto ref T dpy)
	{
		return dpy.release;
	}
	extern (D) auto DisplayString(T)(auto ref T dpy)
	{
		return dpy.display_name;
	}
	extern (D) auto DefaultDepth(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).root_depth;
	}
	extern (D) auto DefaultColormap(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return ScreenOfDisplay(dpy, scr).cmap;
	}
	extern (D) auto BitmapUnit(T)(auto ref T dpy)
	{
		return dpy.bitmap_unit;
	}
	extern (D) auto BitmapBitOrder(T)(auto ref T dpy)
	{
		return dpy.bitmap_bit_order;
	}
	extern (D) auto BitmapPad(T)(auto ref T dpy)
	{
		return dpy.bitmap_pad;
	}
	extern (D) auto ImageByteOrder(T)(auto ref T dpy)
	{
		return dpy.byte_order;
	}
	extern (D) auto NextRequest(T)(auto ref T dpy)
	{
		return dpy.request + 1;
	}
	extern (D) auto LastKnownRequestProcessed(T)(auto ref T dpy)
	{
		return dpy.last_request_read;
	}
	extern (D) auto ScreenOfDisplay(T0, T1)(auto ref T0 dpy, auto ref T1 scr)
	{
		return &dpy.screens[scr];
	}
	extern (D) auto DefaultScreenOfDisplay(T)(auto ref T dpy)
	{
		return ScreenOfDisplay(dpy, DefaultScreen(dpy));
	}
	extern (D) auto DisplayOfScreen(T)(auto ref T s)
	{
		return s.display;
	}
	extern (D) auto RootWindowOfScreen(T)(auto ref T s)
	{
		return s.root;
	}
	extern (D) auto BlackPixelOfScreen(T)(auto ref T s)
	{
		return s.black_pixel;
	}
	extern (D) auto WhitePixelOfScreen(T)(auto ref T s)
	{
		return s.white_pixel;
	}
	extern (D) auto DefaultColormapOfScreen(T)(auto ref T s)
	{
		return s.cmap;
	}
	extern (D) auto DefaultDepthOfScreen(T)(auto ref T s)
	{
		return s.root_depth;
	}
	extern (D) auto DefaultGCOfScreen(T)(auto ref T s)
	{
		return s.default_gc;
	}
	extern (D) auto DefaultVisualOfScreen(T)(auto ref T s)
	{
		return s.root_visual;
	}
	extern (D) auto WidthOfScreen(T)(auto ref T s)
	{
		return s.width;
	}
	extern (D) auto HeightOfScreen(T)(auto ref T s)
	{
		return s.height;
	}
	extern (D) auto WidthMMOfScreen(T)(auto ref T s)
	{
		return s.mwidth;
	}
	extern (D) auto HeightMMOfScreen(T)(auto ref T s)
	{
		return s.mheight;
	}
	extern (D) auto PlanesOfScreen(T)(auto ref T s)
	{
		return s.root_depth;
	}
	extern (D) auto CellsOfScreen(T)(auto ref T s)
	{
		return DefaultVisualOfScreen(s).map_entries;
	}
	extern (D) auto MinCmapsOfScreen(T)(auto ref T s)
	{
		return s.min_maps;
	}
	extern (D) auto MaxCmapsOfScreen(T)(auto ref T s)
	{
		return s.max_maps;
	}
	extern (D) auto DoesSaveUnders(T)(auto ref T s)
	{
		return s.save_unders;
	}
	extern (D) auto DoesBackingStore(T)(auto ref T s)
	{
		return s.backing_store;
	}
	extern (D) auto EventMaskOfScreen(T)(auto ref T s)
	{
		return s.root_input_mask;
	}
	extern (D) auto XAllocID(T)(auto ref T dpy)
	{
		return dpy.resource_alloc(dpy);
	}
	enum XNRequiredCharSet = "requiredCharSet";
	enum XNQueryOrientation = "queryOrientation";
	enum XNBaseFontName = "baseFontName";
	enum XNOMAutomatic = "omAutomatic";
	enum XNMissingCharSet = "missingCharSet";
	enum XNDefaultString = "defaultString";
	enum XNOrientation = "orientation";
	enum XNDirectionalDependentDrawing = "directionalDependentDrawing";
	enum XNContextualDrawing = "contextualDrawing";
	enum XNFontInfo = "fontInfo";
	enum XIMPreeditArea = 0x0001L;
	enum XIMPreeditCallbacks = 0x0002L;
	enum XIMPreeditPosition = 0x0004L;
	enum XIMPreeditNothing = 0x0008L;
	enum XIMPreeditNone = 0x0010L;
	enum XIMStatusArea = 0x0100L;
	enum XIMStatusCallbacks = 0x0200L;
	enum XIMStatusNothing = 0x0400L;
	enum XIMStatusNone = 0x0800L;
	enum XNVaNestedList = "XNVaNestedList";
	enum XNQueryInputStyle = "queryInputStyle";
	enum XNClientWindow = "clientWindow";
	enum XNInputStyle = "inputStyle";
	enum XNFocusWindow = "focusWindow";
	enum XNResourceName = "resourceName";
	enum XNResourceClass = "resourceClass";
	enum XNGeometryCallback = "geometryCallback";
	enum XNDestroyCallback = "destroyCallback";
	enum XNFilterEvents = "filterEvents";
	enum XNPreeditStartCallback = "preeditStartCallback";
	enum XNPreeditDoneCallback = "preeditDoneCallback";
	enum XNPreeditDrawCallback = "preeditDrawCallback";
	enum XNPreeditCaretCallback = "preeditCaretCallback";
	enum XNPreeditStateNotifyCallback = "preeditStateNotifyCallback";
	enum XNPreeditAttributes = "preeditAttributes";
	enum XNStatusStartCallback = "statusStartCallback";
	enum XNStatusDoneCallback = "statusDoneCallback";
	enum XNStatusDrawCallback = "statusDrawCallback";
	enum XNStatusAttributes = "statusAttributes";
	enum XNArea = "area";
	enum XNAreaNeeded = "areaNeeded";
	enum XNSpotLocation = "spotLocation";
	enum XNColormap = "colorMap";
	enum XNStdColormap = "stdColorMap";
	enum XNForeground = "foreground";
	enum XNBackground = "background";
	enum XNBackgroundPixmap = "backgroundPixmap";
	enum XNFontSet = "fontSet";
	enum XNLineSpace = "lineSpace";
	enum XNCursor = "cursor";
	enum XNQueryIMValuesList = "queryIMValuesList";
	enum XNQueryICValuesList = "queryICValuesList";
	enum XNVisiblePosition = "visiblePosition";
	enum XNR6PreeditCallback = "r6PreeditCallback";
	enum XNStringConversionCallback = "stringConversionCallback";
	enum XNStringConversion = "stringConversion";
	enum XNResetState = "resetState";
	enum XNHotKey = "hotKey";
	enum XNHotKeyState = "hotKeyState";
	enum XNPreeditState = "preeditState";
	enum XNSeparatorofNestedList = "separatorofNestedList";
	enum XBufferOverflow = -1;
	enum XLookupNone = 1;
	enum XLookupChars = 2;
	enum XLookupKeySym = 3;
	enum XLookupBoth = 4;
	enum XIMReverse = 1L;
	enum XIMUnderline = 1L << 1;
	enum XIMHighlight = 1L << 2;
	enum XIMPrimary = 1L << 5;
	enum XIMSecondary = 1L << 6;
	enum XIMTertiary = 1L << 7;
	enum XIMVisibleToForward = 1L << 8;
	enum XIMVisibleToBackword = 1L << 9;
	enum XIMVisibleToCenter = 1L << 10;
	enum XIMPreeditUnKnown = 0L;
	enum XIMPreeditEnable = 1L;
	enum XIMPreeditDisable = 1L << 1;
	enum XIMInitialState = 1L;
	enum XIMPreserveState = 1L << 1;
	enum XIMStringConversionLeftEdge = 0x00000001;
	enum XIMStringConversionRightEdge = 0x00000002;
	enum XIMStringConversionTopEdge = 0x00000004;
	enum XIMStringConversionBottomEdge = 0x00000008;
	enum XIMStringConversionConcealed = 0x00000010;
	enum XIMStringConversionWrapped = 0x00000020;
	enum XIMStringConversionBuffer = 0x0001;
	enum XIMStringConversionLine = 0x0002;
	enum XIMStringConversionWord = 0x0003;
	enum XIMStringConversionChar = 0x0004;
	enum XIMStringConversionSubstitution = 0x0001;
	enum XIMStringConversionRetrieval = 0x0002;
	enum XIMHotKeyStateON = 0x0001L;
	enum XIMHotKeyStateOFF = 0x0002L;
}

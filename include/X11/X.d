import core.stdc.config;

extern (C):

alias XID      = ulong;
alias Mask     = ulong;
alias Atom     = ulong;
alias VisualID = ulong;
alias Time     = ulong;

alias  Window   = XID;
alias  Drawable = XID;
alias  Font     = XID;
alias  Pixmap   = XID;
alias  Cursor   = XID;
alias  Colormap = XID;
alias  GContext = XID;
alias  KeySym   = XID;

alias KeyCode = ubyte;

enum None = 0L;
enum ParentRelative = 1L;
enum CopyFromParent = 0L;
enum PointerWindow = 0L;
enum InputFocus = 1L;
enum PointerRoot = 1L;
enum AnyPropertyType = 0L;
enum AnyKey = 0L;
enum AnyButton = 0L;
enum AllTemporary = 0L;
enum CurrentTime = 0L;
enum NoSymbol = 0L;

enum long NoEventMask = 0L;
enum long KeyPressMask = 1L << 0;
enum long KeyReleaseMask = 1L << 1;
enum long ButtonPressMask = 1L << 2;
enum long ButtonReleaseMask = 1L << 3;
enum long EnterWindowMask = 1L << 4;
enum long LeaveWindowMask = 1L << 5;
enum long PointerMotionMask = 1L << 6;
enum long PointerMotionHintMask = 1L << 7;
enum long Button1MotionMask = 1L << 8;
enum long Button2MotionMask = 1L << 9;
enum long Button3MotionMask = 1L << 10;
enum long Button4MotionMask = 1L << 11;
enum long Button5MotionMask = 1L << 12;
enum long ButtonMotionMask = 1L << 13;
enum long KeymapStateMask = 1L << 14;
enum long ExposureMask = 1L << 15;
enum long VisibilityChangeMask = 1L << 16;
enum long StructureNotifyMask = 1L << 17;
enum long ResizeRedirectMask = 1L << 18;
enum long SubstructureNotifyMask = 1L << 19;
enum long SubstructureRedirectMask = 1L << 20;
enum long FocusChangeMask = 1L << 21;
enum long PropertyChangeMask = 1L << 22;
enum long ColormapChangeMask = 1L << 23;
enum long OwnerGrabButtonMask = 1L << 24;

enum KeyPress = 2;
enum KeyRelease = 3;
enum ButtonPress = 4;
enum ButtonRelease = 5;
enum MotionNotify = 6;
enum EnterNotify = 7;
enum LeaveNotify = 8;
enum FocusIn = 9;
enum FocusOut = 10;
enum KeymapNotify = 11;
enum Expose = 12;
enum GraphicsExpose = 13;
enum NoExpose = 14;
enum VisibilityNotify = 15;
enum CreateNotify = 16;
enum DestroyNotify = 17;
enum UnmapNotify = 18;
enum MapNotify = 19;
enum MapRequest = 20;
enum ReparentNotify = 21;
enum ConfigureNotify = 22;
enum ConfigureRequest = 23;
enum GravityNotify = 24;
enum ResizeRequest = 25;
enum CirculateNotify = 26;
enum CirculateRequest = 27;
enum PropertyNotify = 28;
enum SelectionClear = 29;
enum SelectionRequest = 30;
enum SelectionNotify = 31;
enum ColormapNotify = 32;
enum ClientMessage = 33;
enum MappingNotify = 34;
enum GenericEvent = 35;
enum LASTEvent = 36;

enum ShiftMask = 1 << 0;
enum LockMask = 1 << 1;
enum ControlMask = 1 << 2;
enum Mod1Mask = 1 << 3;
enum Mod2Mask = 1 << 4;
enum Mod3Mask = 1 << 5;
enum Mod4Mask = 1 << 6;
enum Mod5Mask = 1 << 7;
enum ShiftMapIndex = 0;
enum LockMapIndex = 1;
enum ControlMapIndex = 2;
enum Mod1MapIndex = 3;
enum Mod2MapIndex = 4;
enum Mod3MapIndex = 5;
enum Mod4MapIndex = 6;
enum Mod5MapIndex = 7;

enum Button1Mask = 1 << 8;
enum Button2Mask = 1 << 9;
enum Button3Mask = 1 << 10;
enum Button4Mask = 1 << 11;
enum Button5Mask = 1 << 12;

enum AnyModifier = 1 << 15;
enum Button1 = 1;
enum Button2 = 2;
enum Button3 = 3;
enum Button4 = 4;
enum Button5 = 5;

enum NotifyNormal = 0;
enum NotifyGrab = 1;
enum NotifyUngrab = 2;
enum NotifyWhileGrabbed = 3;

enum NotifyHint = 1;

enum NotifyAncestor = 0;
enum NotifyVirtual = 1;
enum NotifyInferior = 2;
enum NotifyNonlinear = 3;
enum NotifyNonlinearVirtual = 4;
enum NotifyPointer = 5;
enum NotifyPointerRoot = 6;
enum NotifyDetailNone = 7;

enum VisibilityUnobscured = 0;
enum VisibilityPartiallyObscured = 1;
enum VisibilityFullyObscured = 2;

enum PlaceOnTop = 0;
enum PlaceOnBottom = 1;

enum FamilyInternet = 0;
enum FamilyDECnet = 1;
enum FamilyChaos = 2;
enum FamilyInternet6 = 6;
enum FamilyServerInterpreted = 5;

enum PropertyNewValue = 0;
enum PropertyDelete = 1;

enum ColormapUninstalled = 0;
enum ColormapInstalled = 1;

enum GrabModeSync = 0;
enum GrabModeAsync = 1;

enum GrabSuccess = 0;
enum AlreadyGrabbed = 1;
enum GrabInvalidTime = 2;
enum GrabNotViewable = 3;
enum GrabFrozen = 4;

enum AsyncPointer = 0;
enum SyncPointer = 1;
enum ReplayPointer = 2;
enum AsyncKeyboard = 3;
enum SyncKeyboard = 4;
enum ReplayKeyboard = 5;
enum AsyncBoth = 6;
enum SyncBoth = 7;

enum RevertToNone = cast(int) None;
enum RevertToPointerRoot = cast(int) PointerRoot;
enum RevertToParent = 2;
enum Success = 0;
enum BadRequest = 1;
enum BadValue = 2;
enum BadWindow = 3;
enum BadPixmap = 4;
enum BadAtom = 5;
enum BadCursor = 6;
enum BadFont = 7;
enum BadMatch = 8;
enum BadDrawable = 9;
enum BadAccess = 10;
enum BadAlloc = 11;
enum BadColor = 12;
enum BadGC = 13;
enum BadIDChoice = 14;
enum BadName = 15;
enum BadLength = 16;
enum BadImplementation = 17;

enum FirstExtensionError = 128;
enum LastExtensionError = 255;
enum InputOutput = 1;
enum InputOnly = 2;

enum CWBackPixmap = 1L << 0;
enum CWBackPixel = 1L << 1;
enum CWBorderPixmap = 1L << 2;
enum CWBorderPixel = 1L << 3;
enum CWBitGravity = 1L << 4;
enum CWWinGravity = 1L << 5;
enum CWBackingStore = 1L << 6;
enum CWBackingPlanes = 1L << 7;
enum CWBackingPixel = 1L << 8;
enum CWOverrideRedirect = 1L << 9;
enum CWSaveUnder = 1L << 10;
enum CWEventMask = 1L << 11;
enum CWDontPropagate = 1L << 12;
enum CWColormap = 1L << 13;
enum CWCursor = 1L << 14;

enum CWX = 1 << 0;
enum CWY = 1 << 1;
enum CWWidth = 1 << 2;
enum CWHeight = 1 << 3;
enum CWBorderWidth = 1 << 4;
enum CWSibling = 1 << 5;
enum CWStackMode = 1 << 6;

enum ForgetGravity = 0;
enum NorthWestGravity = 1;
enum NorthGravity = 2;
enum NorthEastGravity = 3;
enum WestGravity = 4;
enum CenterGravity = 5;
enum EastGravity = 6;
enum SouthWestGravity = 7;
enum SouthGravity = 8;
enum SouthEastGravity = 9;
enum StaticGravity = 10;

enum UnmapGravity = 0;

enum NotUseful = 0;
enum WhenMapped = 1;
enum Always = 2;

enum IsUnmapped = 0;
enum IsUnviewable = 1;
enum IsViewable = 2;

enum SetModeInsert = 0;
enum SetModeDelete = 1;

enum DestroyAll = 0;
enum RetainPermanent = 1;
enum RetainTemporary = 2;

enum Above = 0;
enum Below = 1;
enum TopIf = 2;
enum BottomIf = 3;
enum Opposite = 4;

enum RaiseLowest = 0;
enum LowerHighest = 1;

enum PropModeReplace = 0;
enum PropModePrepend = 1;
enum PropModeAppend = 2;
enum GXclear = 0x0;
enum GXand = 0x1;
enum GXandReverse = 0x2;
enum GXcopy = 0x3;
enum GXandInverted = 0x4;
enum GXnoop = 0x5;
enum GXxor = 0x6;
enum GXor = 0x7;
enum GXnor = 0x8;
enum GXequiv = 0x9;
enum GXinvert = 0xa;
enum GXorReverse = 0xb;
enum GXcopyInverted = 0xc;
enum GXorInverted = 0xd;
enum GXnand = 0xe;
enum GXset = 0xf;

enum LineSolid = 0;
enum LineOnOffDash = 1;
enum LineDoubleDash = 2;

enum CapNotLast = 0;
enum CapButt = 1;
enum CapRound = 2;
enum CapProjecting = 3;

enum JoinMiter = 0;
enum JoinRound = 1;
enum JoinBevel = 2;

enum FillSolid = 0;
enum FillTiled = 1;
enum FillStippled = 2;
enum FillOpaqueStippled = 3;

enum EvenOddRule = 0;
enum WindingRule = 1;

enum ClipByChildren = 0;
enum IncludeInferiors = 1;

enum Unsorted = 0;
enum YSorted = 1;
enum YXSorted = 2;
enum YXBanded = 3;

enum CoordModeOrigin = 0;
enum CoordModePrevious = 1;

enum Complex = 0;
enum Nonconvex = 1;
enum Convex = 2;

enum ArcChord = 0;
enum ArcPieSlice = 1;
enum GCFunction = 1L << 0;
enum GCPlaneMask = 1L << 1;
enum GCForeground = 1L << 2;
enum GCBackground = 1L << 3;
enum GCLineWidth = 1L << 4;
enum GCLineStyle = 1L << 5;
enum GCCapStyle = 1L << 6;
enum GCJoinStyle = 1L << 7;
enum GCFillStyle = 1L << 8;
enum GCFillRule = 1L << 9;
enum GCTile = 1L << 10;
enum GCStipple = 1L << 11;
enum GCTileStipXOrigin = 1L << 12;
enum GCTileStipYOrigin = 1L << 13;
enum GCFont = 1L << 14;
enum GCSubwindowMode = 1L << 15;
enum GCGraphicsExposures = 1L << 16;
enum GCClipXOrigin = 1L << 17;
enum GCClipYOrigin = 1L << 18;
enum GCClipMask = 1L << 19;
enum GCDashOffset = 1L << 20;
enum GCDashList = 1L << 21;
enum GCArcMode = 1L << 22;

enum GCLastBit = 22;

enum FontLeftToRight = 0;
enum FontRightToLeft = 1;

enum FontChange = 255;
enum XYBitmap = 0;
enum XYPixmap = 1;
enum ZPixmap = 2;
enum AllocNone = 0;
enum AllocAll = 1;

enum DoRed = 1 << 0;
enum DoGreen = 1 << 1;
enum DoBlue = 1 << 2;
enum CursorShape = 0;
enum TileShape = 1;
enum StippleShape = 2;
enum AutoRepeatModeOff = 0;
enum AutoRepeatModeOn = 1;
enum AutoRepeatModeDefault = 2;

enum LedModeOff = 0;
enum LedModeOn = 1;

enum KBKeyClickPercent = 1L << 0;
enum KBBellPercent = 1L << 1;
enum KBBellPitch = 1L << 2;
enum KBBellDuration = 1L << 3;
enum KBLed = 1L << 4;
enum KBLedMode = 1L << 5;
enum KBKey = 1L << 6;
enum KBAutoRepeatMode = 1L << 7;

enum MappingSuccess = 0;
enum MappingBusy = 1;
enum MappingFailed = 2;

enum MappingModifier = 0;
enum MappingKeyboard = 1;
enum MappingPointer = 2;
enum DontPreferBlanking = 0;
enum PreferBlanking = 1;
enum DefaultBlanking = 2;

enum DisableScreenSaver = 0;
enum DisableScreenInterval = 0;

enum DontAllowExposures = 0;
enum AllowExposures = 1;
enum DefaultExposures = 2;

enum ScreenSaverReset = 0;
enum ScreenSaverActive = 1;
enum HostInsert = 0;
enum HostDelete = 1;

enum EnableAccess = 1;
enum DisableAccess = 0;
enum StaticGray = 0;
enum GrayScale = 1;
enum StaticColor = 2;
enum PseudoColor = 3;
enum TrueColor = 4;
enum DirectColor = 5;

enum LSBFirst = 0;
enum MSBFirst = 1;

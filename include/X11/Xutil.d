import core.stdc.config;
import core.stdc.stddef;

import X11.Xlib;

extern (C):
enum NoValue = 0x0000;
enum XValue = 0x0001;
enum YValue = 0x0002;
enum WidthValue = 0x0004;
enum HeightValue = 0x0008;
enum AllValues = 0x000F;
enum XNegative = 0x0010;
enum YNegative = 0x0020;
struct XSizeHints
{
    c_long flags;
    int x;
    int y;
    int width;
    int height;
    int min_width;
    int min_height;
    int max_width;
    int max_height;
    int width_inc;
    int height_inc;
    struct _Anonymous_0
    {
        int x;
        int y;
    }
    _Anonymous_0 min_aspect;
    _Anonymous_0 max_aspect;
    int base_width;
    int base_height;
    int win_gravity;
}
enum USPosition = 1L << 0;
enum USSize = 1L << 1;
enum PPosition = 1L << 2;
enum PSize = 1L << 3;
enum PMinSize = 1L << 4;
enum PMaxSize = 1L << 5;
enum PResizeInc = 1L << 6;
enum PAspect = 1L << 7;
enum PBaseSize = 1L << 8;
enum PWinGravity = 1L << 9;
enum PAllHints = PPosition | PSize | PMinSize | PMaxSize | PResizeInc | PAspect;
struct XWMHints
{
    c_long flags;
    int input; /* does this application rely on the window manager to
    			get keyboard input? */
    int initial_state;
    Pixmap icon_pixmap;
    Window icon_window;
    int icon_x;
    int icon_y;
    Pixmap icon_mask;
    XID window_group;
}
enum InputHint = 1L << 0;
enum StateHint = 1L << 1;
enum IconPixmapHint = 1L << 2;
enum IconWindowHint = 1L << 3;
enum IconPositionHint = 1L << 4;
enum IconMaskHint = 1L << 5;
enum WindowGroupHint = 1L << 6;
enum AllHints = InputHint | StateHint | IconPixmapHint | IconWindowHint | IconPositionHint | IconMaskHint | WindowGroupHint;
enum XUrgencyHint = 1L << 8;
enum WithdrawnState = 0;
enum NormalState = 1;
enum IconicState = 3;
enum DontCareState = 0;
enum ZoomState = 2;
enum InactiveState = 4;
struct XTextProperty
{
    ubyte* value;
    Atom encoding;
    int format;
    c_ulong nitems;
}
enum XNoMemory = -1;
enum XLocaleNotSupported = -2;
enum XConverterNotFound = -3;
enum XICCEncodingStyle
{
    XStringStyle = 0,
    XCompoundTextStyle = 1,
    XTextStyle = 2,
    XStdICCTextStyle = 3,

    XUTF8StringStyle = 4
}
struct XIconSize
{
    int min_width;
    int min_height;
    int max_width;
    int max_height;
    int width_inc;
    int height_inc;
}
struct XClassHint
{
    const (char)* res_name;
    const (char)* res_class;
}
extern (D) auto XDestroyImage(T)(auto ref T ximage)
{
    return (*ximage.f.destroy_image)(ximage);
}
extern (D) auto XGetPixel(T0, T1, T2)(auto ref T0 ximage, auto ref T1 x, auto ref T2 y)
{
    return (*ximage.f.get_pixel)(ximage, x, y);
}
extern (D) auto XPutPixel(T0, T1, T2, T3)(auto ref T0 ximage, auto ref T1 x, auto ref T2 y, auto ref T3 pixel)
{
    return (*ximage.f.put_pixel)(ximage, x, y, pixel);
}
extern (D) auto XSubImage(T0, T1, T2, T3, T4)(auto ref T0 ximage, auto ref T1 x, auto ref T2 y, auto ref T3 width, auto ref T4 height)
{
    return (*ximage.f.sub_image)(ximage, x, y, width, height);
}
extern (D) auto XAddPixel(T0, T1)(auto ref T0 ximage, auto ref T1 value)
{
    return (*ximage.f.add_pixel)(ximage, value);
}
struct _XComposeStatus
{
    XPointer compose_ptr;
    int chars_matched;
}
alias XComposeStatus = _XComposeStatus;
extern (D) auto IsKeypadKey(T)(auto ref T keysym)
{
    return (cast(KeySym) keysym >= XK_KP_Space) && (cast(KeySym) keysym <= XK_KP_Equal);
}
extern (D) auto IsPrivateKeypadKey(T)(auto ref T keysym)
{
    return (cast(KeySym) keysym >= 0x11000000) && (cast(KeySym) keysym <= 0x1100FFFF);
}
extern (D) auto IsCursorKey(T)(auto ref T keysym)
{
    return (cast(KeySym) keysym >= XK_Home) && (cast(KeySym) keysym < XK_Select);
}
extern (D) auto IsPFKey(T)(auto ref T keysym)
{
    return (cast(KeySym) keysym >= XK_KP_F1) && (cast(KeySym) keysym <= XK_KP_F4);
}
extern (D) auto IsFunctionKey(T)(auto ref T keysym)
{
    return (cast(KeySym) keysym >= XK_F1) && (cast(KeySym) keysym <= XK_F35);
}
extern (D) auto IsMiscFunctionKey(T)(auto ref T keysym)
{
    return (cast(KeySym) keysym >= XK_Select) && (cast(KeySym) keysym <= XK_Break);
}
extern (D) auto IsModifierKey(T)(auto ref T keysym)
{
    return ((cast(KeySym) keysym >= XK_Shift_L) && (cast(KeySym) keysym <= XK_Hyper_R)) || ((cast(KeySym) keysym >= XK_ISO_Lock) && (cast(KeySym) keysym <= XK_ISO_Level5_Lock)) || (cast(KeySym) keysym == XK_Mode_switch) || (cast(KeySym) keysym == XK_Num_Lock);
}
struct _XRegion;
alias Region = _XRegion*;
enum RectangleOut = 0;
enum RectangleIn = 1;
enum RectanglePart = 2;
struct XVisualInfo
{
    Visual* visual;
    VisualID visualid;
    int screen;
    int depth;

    int class_;
    c_ulong red_mask;
    c_ulong green_mask;
    c_ulong blue_mask;
    int colormap_size;
    int bits_per_rgb;
}
enum VisualNoMask = 0x0;
enum VisualIDMask = 0x1;
enum VisualScreenMask = 0x2;
enum VisualDepthMask = 0x4;
enum VisualClassMask = 0x8;
enum VisualRedMaskMask = 0x10;
enum VisualGreenMaskMask = 0x20;
enum VisualBlueMaskMask = 0x40;
enum VisualColormapSizeMask = 0x80;
enum VisualBitsPerRGBMask = 0x100;
enum VisualAllMask = 0x1FF;
struct XStandardColormap
{
    Colormap colormap;
    c_ulong red_max;
    c_ulong red_mult;
    c_ulong green_max;
    c_ulong green_mult;
    c_ulong blue_max;
    c_ulong blue_mult;
    c_ulong base_pixel;
    VisualID visualid;
    XID killid;
}
enum ReleaseByFreeingColormap = cast(XID) 1L;
enum BitmapSuccess = 0;
enum BitmapOpenFailed = 1;
enum BitmapFileInvalid = 2;
enum BitmapNoMemory = 3;
enum XCSUCCESS = 0;
enum XCNOMEM = 1;
enum XCNOENT = 2;
alias XContext = int;
//extern (D) auto XUniqueContext()
//{
//    return cast(XContext) XrmUniqueQuark();
//}
//extern (D) auto XStringToContext(T)(auto ref T string)
//{
//    return cast(XContext) XrmStringToQuark(string);
//}
XClassHint* XAllocClassHint ();
XIconSize* XAllocIconSize ();
XSizeHints* XAllocSizeHints ();
XStandardColormap* XAllocStandardColormap ();
XWMHints* XAllocWMHints ();
int XClipBox (Region, XRectangle*);
Region XCreateRegion ();
const(char)* XDefaultString ();
int XDeleteContext (Display*, XID, XContext);
int XDestroyRegion (Region);
int XEmptyRegion (Region);
int XEqualRegion (Region, Region);
int XFindContext (Display*, XID, XContext, XPointer*);
int XGetClassHint (Display*, Window, XClassHint*);
int XGetIconSizes (Display*, Window, XIconSize**, int*);
int XGetNormalHints (Display*, Window, XSizeHints*);
int XGetRGBColormaps (Display*, Window, XStandardColormap**, int*, Atom);
int XGetSizeHints (Display*, Window, XSizeHints*, Atom);
int XGetStandardColormap (Display*, Window, XStandardColormap*, Atom);
int XGetTextProperty (Display*, Window, XTextProperty*, Atom);
XVisualInfo* XGetVisualInfo (Display*, c_long, XVisualInfo*, int*);
int XGetWMClientMachine (Display*, Window, XTextProperty*);
XWMHints* XGetWMHints (Display*, Window);
int XGetWMIconName (Display*, Window, XTextProperty*);
int XGetWMName (Display*, Window, XTextProperty*);
int XGetWMNormalHints (Display*, Window, XSizeHints*, c_long*);
int XGetWMSizeHints (Display*, Window, XSizeHints*, c_long*, Atom);
int XGetZoomHints (Display*, Window, XSizeHints*);
int XIntersectRegion (Region, Region, Region);
void XConvertCase (KeySym, KeySym*, KeySym*);
int XLookupString (XKeyEvent*, char*, int, KeySym*, XComposeStatus*);
int XMatchVisualInfo (Display*, int, int, int, XVisualInfo*);
int XOffsetRegion (Region, int, int);
int XPointInRegion (Region, int, int);
Region XPolygonRegion (XPoint*, int, int);
int XRectInRegion (Region, int, int, uint, uint);
int XSaveContext (Display*, XID, XContext, const(char)*);
int XSetClassHint (Display*, Window, XClassHint*);
int XSetIconSizes (Display*, Window, XIconSize*, int);
int XSetNormalHints (Display*, Window, XSizeHints*);
void XSetRGBColormaps (Display*, Window, XStandardColormap*, int, Atom);
int XSetSizeHints (Display*, Window, XSizeHints*, Atom);
int XSetStandardProperties (
    Display*,
    Window,
    const(char)*,
    const(char)*,
    Pixmap,
    char**,
    int,
    XSizeHints*);
void XSetTextProperty (Display*, Window, XTextProperty*, Atom);
void XSetWMClientMachine (Display*, Window, XTextProperty*);
int XSetWMHints (Display*, Window, XWMHints*);
void XSetWMIconName (Display*, Window, XTextProperty*);
void XSetWMName (Display*, Window, XTextProperty*);
void XSetWMNormalHints (Display*, Window, XSizeHints*);
void XSetWMProperties (
    Display*,
    Window,
    XTextProperty*,
    XTextProperty*,
    char**,
    int,
    XSizeHints*,
    XWMHints*,
    XClassHint*);
void XmbSetWMProperties (
    Display*,
    Window,
    const(char)*,
    const(char)*,
    char**,
    int,
    XSizeHints*,
    XWMHints*,
    XClassHint*);
void Xutf8SetWMProperties (
    Display*,
    Window,
    const(char)*,
    const(char)*,
    char**,
    int,
    XSizeHints*,
    XWMHints*,
    XClassHint*);
void XSetWMSizeHints (Display*, Window, XSizeHints*, Atom);
int XSetRegion (Display*, GC, Region);
void XSetStandardColormap (Display*, Window, XStandardColormap*, Atom);
int XSetZoomHints (Display*, Window, XSizeHints*);
int XShrinkRegion (Region, int, int);
int XStringListToTextProperty (const (char)**, int, XTextProperty*);
int XSubtractRegion (Region, Region, Region);
int XmbTextListToTextProperty (
    Display* display,
    char** list,
    int count,
    XICCEncodingStyle style,
    XTextProperty* text_prop_return);
int XwcTextListToTextProperty (
    Display* display,
    wchar_t** list,
    int count,
    XICCEncodingStyle style,
    XTextProperty* text_prop_return);
int Xutf8TextListToTextProperty (
    Display* display,
    char** list,
    int count,
    XICCEncodingStyle style,
    XTextProperty* text_prop_return);
void XwcFreeStringList (wchar_t** list);
int XTextPropertyToStringList (XTextProperty*, char***, int*);
int XmbTextPropertyToTextList (
    Display* display,
    const(XTextProperty)* text_prop,
    char*** list_return,
    int* count_return);
int XwcTextPropertyToTextList (
    Display* display,
    const(XTextProperty)* text_prop,
    wchar_t*** list_return,
    int* count_return);
int Xutf8TextPropertyToTextList (
    Display* display,
    const(XTextProperty)* text_prop,
    char*** list_return,
    int* count_return);
int XUnionRectWithRegion (XRectangle*, Region, Region);
int XUnionRegion (Region, Region, Region);
int XWMGeometry (
    Display*,
    int,
    const(char)*,
    const(char)*,
    uint,
    XSizeHints*,
    int*,
    int*,
    int*,
    int*,
    int*);
int XXorRegion (Region, Region, Region);
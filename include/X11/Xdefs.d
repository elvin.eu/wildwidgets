import core.stdc.config;

extern (C):

alias Atom = c_ulong;
alias Bool = int;
alias pointer = void*;
struct _Client;
alias ClientPtr = _Client*;
alias XID = c_ulong;
alias Mask = c_ulong;
struct _Font;
alias FontPtr = _Font*; /* also in fonts/include/font.h */
alias Font = c_ulong;
alias FSID = c_ulong;
alias AccContext = c_ulong;
struct timeval;
alias OSTimePtr = timeval**;
alias BlockHandlerProcPtr = void function (void*, OSTimePtr, void*);

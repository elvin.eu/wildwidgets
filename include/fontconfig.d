module fontconfig;

import std.format;

extern (C)
{

enum FC_MAJOR = 2;
enum FC_MINOR = 12;
enum FC_REVISION = 6;

int FC_VERSION ()
{
	return (FC_MAJOR * 10000 + FC_MINOR * 100 + FC_REVISION);
}

enum FC_CACHE_VERSION_NUMBER = 7;
enum FC_CACHE_VERSION = format("%s", FC_CACHE_VERSION_NUMBER);

enum FcTrue = 1;
enum FcFalse = 0;

enum FC_FAMILY = "family";
enum FC_STYLE = "style";
enum FC_SLANT = "slant";
enum FC_WEIGHT = "weight";
enum FC_SIZE = "size";
enum FC_ASPECT = "aspect";
enum FC_PIXEL_SIZE = "pixelsize";
enum FC_SPACING = "spacing";
enum FC_FOUNDRY = "foundry";
enum FC_ANTIALIAS = "antialias";
enum FC_HINTING = "hinting";
enum FC_HINT_STYLE = "hintstyle";
enum FC_VERTICAL_LAYOUT = "verticallayout";
enum FC_AUTOHINT = "autohint";

enum FC_GLOBAL_ADVANCE = "globaladvance";
enum FC_WIDTH = "width";
enum FC_FILE = "file";
enum FC_INDEX = "index";
enum FC_FT_FACE = "ftface";
enum FC_RASTERIZER = "rasterizer";
enum FC_OUTLINE = "outline";
enum FC_SCALABLE = "scalable";
enum FC_COLOR = "color";
enum FC_SCALE = "scale";
enum FC_SYMBOL = "symbol";
enum FC_DPI = "dpi";
enum FC_RGBA = "rgba";
enum FC_MINSPACE = "minspace";
enum FC_SOURCE = "source";
enum FC_CHARSET = "charset";
enum FC_LANG = "lang";
enum FC_FONTVERSION = "fontversion";
enum FC_FULLNAME = "fullname";
enum FC_FAMILYLANG = "familylang";
enum FC_STYLELANG = "stylelang";
enum FC_FULLNAMELANG = "fullnamelang";
enum FC_CAPABILITY = "capability";
enum FC_FONTFORMAT = "fontformat";
enum FC_EMBOLDEN = "embolden";
enum FC_EMBEDDED_BITMAP = "embeddedbitmap";
enum FC_DECORATIVE = "decorative";
enum FC_LCD_FILTER = "lcdfilter";
enum FC_FONT_FEATURES = "fontfeatures";
enum FC_NAMELANG = "namelang";
enum FC_PRGNAME = "prgname";
enum FC_HASH = "hash";
enum FC_POSTSCRIPT_NAME = "postscriptname";

enum FC_CACHE_SUFFIX    = ".cache-"~FC_CACHE_VERSION;
enum FC_DIR_CACHE_FILE	= "fonts.cache-"~FC_CACHE_VERSION;
enum FC_USER_CACHE_FILE	= ".fonts.cache-"~FC_CACHE_VERSION;

enum FC_CHARWIDTH = "charwidth";
enum FC_CHAR_HEIGHT = "charheight";
enum FC_MATRIX = "matrix";

enum FC_WEIGHT_THIN = 0;
enum FC_WEIGHT_EXTRALIGHT = 40;
enum FC_WEIGHT_LIGHT = 50;
enum FC_WEIGHT_DEMILIGHT = 55;
enum FC_WEIGHT_BOOK = 75;
enum FC_WEIGHT_REGULAR = 80;
enum FC_WEIGHT_MEDIUM = 100;
enum FC_WEIGHT_DEMIBOLD = 180;
enum FC_WEIGHT_BOLD = 200;
enum FC_WEIGHT_EXTRABOLD = 205;
enum FC_WEIGHT_BLACK = 210;
enum FC_WEIGHT_EXTRABLACK = 215;


enum FC_SLANT_ROMAN = 0;
enum FC_SLANT_ITALIC = 100;
enum FC_SLANT_OBLIQUE = 110;

enum FC_WIDTH_ULTRACONDENSED = 50;
enum FC_WIDTH_EXTRACONDENSED = 63;
enum FC_WIDTH_CONDENSED = 75;
enum FC_WIDTH_SEMICONDENSED = 87;
enum FC_WIDTH_NORMAL = 100;
enum FC_WIDTH_SEMIEXPANDED = 113;
enum FC_WIDTH_EXPANDED = 125;
enum FC_WIDTH_EXTRAEXPANDED = 150;
enum FC_WIDTH_ULTRAEXPANDED = 200;

enum FC_PROPORTIONAL = 0;
enum FC_DUAL = 90;
enum FC_MONO = 100;
enum FC_CHARCELL = 110;

enum FC_RGBA_UNKNOWN = 0;
enum FC_RGBA_RGB = 1;
enum FC_RGBA_BGR = 2;
enum FC_RGBA_VRGB = 3;
enum FC_RGBA_VBGR = 4;
enum FC_RGBA_NONE = 5;

enum FC_HINT_NONE = 0;
enum FC_HINT_SLIGHT = 1;
enum FC_HINT_MEDIUM = 2;
enum FC_HINT_FULL = 3;

enum FC_LCD_NONE = 0;
enum FC_LCD_DEFAULT = 1;
enum FC_LCD_LIGHT = 2;
enum FC_LCD_LEGACY = 3;

enum FcType {
    FcTypeUnknown = -1,
    FcTypeVoid,
    FcTypeInteger,
    FcTypeDouble,
    FcTypeString,
    FcTypeBool,
    FcTypeMatrix,
    FcTypeCharSet,
    FcTypeFTFace,
    FcTypeLangSet,
    FcTypeRange
}

struct FcMatrix {
    double xx=0, xy=1, yx=1, yy=0;
}

struct FcCharSet {};

struct FcObjectType
{
    char	*object;
    FcType	type;
}

struct FcConstant
{
    const char  *name;
    const char	*object;
    int		value;
}

enum FcResult
{
    FcResultMatch,
	FcResultNoMatch,
	FcResultTypeMismatch,
	FcResultNoId,
    FcResultOutOfMemory
}

enum FcValueBinding
{
    FcValueBindingWeak,
	FcValueBindingStrong,
	FcValueBindingSame,
    FcValueBindingEnd = int.max
}

struct FcPattern {};
struct FcLangSet {};
struct FcRange {};

struct FcValue
{
    FcType	type;
    union
	{
		const char	*s;
		int		i;
		int		b;
		double		d;
		const (FcMatrix)	*m;
		const (FcCharSet)	*c;
		void		*f;
		const (FcLangSet)	*l;
		const (FcRange)	*r;
    }
};

struct FcFontSet
{
    int		nfont;
    int		sfont;
    FcPattern	**fonts;
}

struct FcObjectSet
{
    int		nobject;
    int		sobject;
    const char	**objects;
}

enum FcMatchKind
{
    FcMatchPattern,
	FcMatchFont,
	FcMatchScan
}

enum FcLangResult {
    FcLangEqual = 0,
    FcLangDifferentCountry = 1,
    FcLangDifferentTerritory = 1,
    FcLangDifferentLang = 2
}

enum FcSetName {
    FcSetSystem = 0,
    FcSetApplication = 1
}

struct FcAtomic {};


enum FcEndian
{
	FcEndianBig,
	FcEndianLittle
}

struct FcConfig {};
struct FcFileCache {};
struct FcBlanks {};
struct FcStrList {};
struct FcStrSet {};
struct FcCache {};

FcBlanks * FcBlanksCreate ();
void FcBlanksDestroy (FcBlanks *b);
int FcBlanksAdd (FcBlanks *b, uint ucs4);
int FcBlanksIsMember (FcBlanks *b, uint ucs4);
const (char) * FcCacheDir(const (FcCache) *c);
FcFontSet * FcCacheCopySet(const (FcCache) *c);
const (char) * FcCacheSubdir (const (FcCache) *c, int i);
int FcCacheNumSubdir (const (FcCache) *c);
int FcCacheNumFont (const (FcCache) *c);
int FcDirCacheUnlink (const (char) *dir, FcConfig *config);
int FcDirCacheValid (const (char) *cache_file);
int FcDirCacheClean (const (char) *cache_dir, int verbose);
void FcCacheCreateTagFile (const (FcConfig) *config);
char * FcConfigHome ();
int FcConfigEnableHome (int enable);
char * FcConfigFilename (const (char) *url);
FcConfig * FcConfigCreate ();
FcConfig * FcConfigReference (FcConfig *config);
void FcConfigDestroy (FcConfig *config);
int FcConfigSetCurrent (FcConfig *config);
FcConfig * FcConfigGetCurrent ();
int FcConfigUptoDate (FcConfig *config);
int FcConfigBuildFonts (FcConfig *config);
FcStrList * FcConfigGetFontDirs (FcConfig *config);
FcStrList * FcConfigGetConfigDirs (FcConfig *config);
FcStrList * FcConfigGetConfigFiles (FcConfig *config);
char * FcConfigGetCache (FcConfig *config);
FcBlanks * FcConfigGetBlanks (FcConfig *config);
FcStrList * FcConfigGetCacheDirs (const (FcConfig) *config);
int FcConfigGetRescanInterval (FcConfig *config);
int FcConfigSetRescanInterval (FcConfig *config, int rescanInterval);
FcFontSet * FcConfigGetFonts (FcConfig *config, FcSetName set);
int FcConfigAppFontAddFile (FcConfig *config, const (char) *file);
int FcConfigAppFontAddDir (FcConfig *config, const (char) *dir);
void FcConfigAppFontClear (FcConfig *config);
int FcConfigSubstituteWithPat (FcConfig *config, FcPattern *p, FcPattern *p_pat, FcMatchKind kind);
int FcConfigSubstitute (FcConfig *config, FcPattern *p, FcMatchKind kind);
const (char) * FcConfigGetSysRoot (const (FcConfig) *config);
void FcConfigSetSysRoot (FcConfig *config, const (char) *sysroot);
FcCharSet* FcCharSetCreate ();
FcCharSet * FcCharSetNew ();
void FcCharSetDestroy (FcCharSet *fcs);
int FcCharSetAddChar (FcCharSet *fcs, uint ucs4);
int FcCharSetDelChar (FcCharSet *fcs, uint ucs4);
FcCharSet* FcCharSetCopy (FcCharSet *src);
int FcCharSetEqual (const (FcCharSet) *a, const (FcCharSet) *b);
FcCharSet* FcCharSetIntersect (const (FcCharSet) *a, const (FcCharSet) *b);
FcCharSet* FcCharSetUnion (const (FcCharSet) *a, const (FcCharSet) *b);
FcCharSet* FcCharSetSubtract (const (FcCharSet) *a, const (FcCharSet) *b);
int FcCharSetMerge (FcCharSet *a, const (FcCharSet) *b, int *changed);
int FcCharSetHasChar (const (FcCharSet) *fcs, uint ucs4);
uint FcCharSetCount (const (FcCharSet) *a);
uint FcCharSetIntersectCount (const (FcCharSet) *a, const (FcCharSet) *b);
uint FcCharSetSubtractCount (const (FcCharSet) *a, const (FcCharSet) *b);
int FcCharSetIsSubset (const (FcCharSet) *a, const (FcCharSet) *b);

enum FC_CHARSET_MAP_SIZE = (256/32);
enum FC_CHARSET_DONE = (cast (uint) -1);

uint FcCharSetFirstPage (const (FcCharSet) *a, uint[FC_CHARSET_MAP_SIZE] map, uint *next);
uint FcCharSetNextPage (const (FcCharSet) *a, uint[FC_CHARSET_MAP_SIZE] map, uint *next);
uint FcCharSetCoverage (const (FcCharSet) *a, uint page, uint *result);
void FcValuePrint (const (FcValue) v);
void FcPatternPrint (const (FcPattern) *p);
void FcFontSetPrint (const (FcFontSet) *s);
FcStrSet * FcGetDefaultLangs ();
void FcDefaultSubstitute (FcPattern *pattern);
int FcFileIsDir (const (char) *file);
int FcFileScan (FcFontSet *set, FcStrSet *dirs, FcFileCache *cache, FcBlanks *blanks, const (char) *file, int force);
int FcDirScan (FcFontSet *set, FcStrSet *dirs, FcFileCache *cache, FcBlanks *blanks, const (char) *dir, int force);
int FcDirSave (FcFontSet *set, FcStrSet *dirs, const (char) *dir);
FcCache * FcDirCacheLoad (const (char) *dir, FcConfig *config, char **cache_file);
FcCache * FcDirCacheRescan (const (char) *dir, FcConfig *config);
FcCache * FcDirCacheRead (const (char) *dir, int force, FcConfig *config);
struct stat {};
FcCache * FcDirCacheLoadFile (const (char) *cache_file, stat *file_stat);
void FcDirCacheUnload (FcCache *cache);
FcPattern * FcFreeTypeQuery (const (char) *file, int id, FcBlanks *blanks, int *count);
FcFontSet * FcFontSetCreate ();
void FcFontSetDestroy (FcFontSet *s);
int FcFontSetAdd (FcFontSet *s, FcPattern *font);
FcConfig * FcInitLoadConfig ();
FcConfig * FcInitLoadConfigAndFonts ();
int FcInit ();
void FcFini ();
int FcGetVersion ();
int FcInitReinitialize ();
int FcInitBringUptoDate ();
FcStrSet * FcGetLangs ();
char * FcLangNormalize (const (char) *lang);
const (FcCharSet) * FcLangGetCharSet (const (char) *lang);
FcLangSet* FcLangSetCreate ();
void FcLangSetDestroy (FcLangSet *ls);
FcLangSet* FcLangSetCopy (const (FcLangSet) *ls);
int FcLangSetAdd (FcLangSet *ls, const (char) *lang);
int FcLangSetDel (FcLangSet *ls, const (char) *lang);
FcLangResult FcLangSetHasLang (const (FcLangSet) *ls, const (char) *lang);
FcLangResult FcLangSetCompare (const (FcLangSet) *lsa, const (FcLangSet) *lsb);
int FcLangSetContains (const (FcLangSet) *lsa, const (FcLangSet) *lsb);
int FcLangSetEqual (const (FcLangSet) *lsa, const (FcLangSet) *lsb);
uint FcLangSetHash (const (FcLangSet) *ls);
FcStrSet * FcLangSetGetLangs (const (FcLangSet) *ls);
FcLangSet * FcLangSetUnion (const (FcLangSet) *a, const (FcLangSet) *b);
FcLangSet * FcLangSetSubtract (const (FcLangSet) *a, const (FcLangSet) *b);
FcObjectSet * FcObjectSetCreate ();
int FcObjectSetAdd (FcObjectSet *os, const (char) *object);
void FcObjectSetDestroy (FcObjectSet *os);
//FcObjectSet * FcObjectSetVaBuild (const (char) *first, va_list va);
FcObjectSet * FcObjectSetBuild (const (char) *first, ...);
FcFontSet * FcFontSetList (FcConfig *config, FcFontSet **sets, int nsets, FcPattern *p, FcObjectSet *os);
FcFontSet * FcFontList (FcConfig *config, FcPattern *p, FcObjectSet *os);
FcAtomic * FcAtomicCreate (const (char) *file);
int FcAtomicLock (FcAtomic *atomic);
char * FcAtomicNewFile (FcAtomic *atomic);
char * FcAtomicOrigFile (FcAtomic *atomic);
int FcAtomicReplaceOrig (FcAtomic *atomic);
void FcAtomicDeleteNew (FcAtomic *atomic);
void FcAtomicUnlock (FcAtomic *atomic);
void FcAtomicDestroy (FcAtomic *atomic);
FcPattern * FcFontSetMatch (FcConfig *config, FcFontSet **sets, int nsets, FcPattern *p, FcResult *result);
FcPattern * FcFontMatch (FcConfig *config, FcPattern *p, FcResult *result);
FcPattern * FcFontRenderPrepare (FcConfig *config, FcPattern *pat, FcPattern *font);
FcFontSet * FcFontSetSort (FcConfig *config, FcFontSet **sets, int nsets, FcPattern *p, int trim, FcCharSet **csp, FcResult *result);
FcFontSet * FcFontSort (FcConfig *config, FcPattern *p, int trim, FcCharSet **csp, FcResult *result);
void FcFontSetSortDestroy (FcFontSet *fs);
FcMatrix * FcMatrixCopy (const (FcMatrix) *mat);
int FcMatrixEqual (const (FcMatrix) *mat1, const (FcMatrix) *mat2);
void FcMatrixMultiply (FcMatrix *result, const (FcMatrix) *a, const (FcMatrix) *b);
void FcMatrixRotate (FcMatrix *m, double c, double s);
void FcMatrixScale (FcMatrix *m, double sx, double sy);
void FcMatrixShear (FcMatrix *m, double sh, double sv);
int FcNameRegisterObjectTypes (const (FcObjectType) *types, int ntype);
int FcNameUnregisterObjectTypes (const (FcObjectType) *types, int ntype);
const (FcObjectType) * FcNameGetObjectType (const (char) *object);
int FcNameRegisterConstants (const (FcConstant) *consts, int nconsts);
int FcNameUnregisterConstants (const (FcConstant) *consts, int nconsts);
const (FcConstant) * FcNameGetConstant (const (char) *string);
int FcNameConstant (const (char) *string, int *result);
FcPattern * FcNameParse (const (char) *name);
char * FcNameUnparse (FcPattern *pat);
FcPattern * FcPatternCreate ();
FcPattern * FcPatternDuplicate (const (FcPattern) *p);
void FcPatternReference (FcPattern *p);
FcPattern * FcPatternFilter (FcPattern *p, const (FcObjectSet) *os);
void FcValueDestroy (FcValue v);
int FcValueEqual (FcValue va, FcValue vb);
FcValue FcValueSave (FcValue v);
void FcPatternDestroy (FcPattern *p);
int FcPatternEqual (const (FcPattern) *pa, const (FcPattern) *pb);
int FcPatternEqualSubset (const (FcPattern) *pa, const (FcPattern) *pb, const (FcObjectSet) *os);
uint FcPatternHash (const (FcPattern) *p);
int FcPatternAdd (FcPattern *p, const (char) *object, FcValue value, int append);
int FcPatternAddWeak (FcPattern *p, const (char) *object, FcValue value, int append);
FcResult FcPatternGet (const (FcPattern) *p, const (char) *object, int id, FcValue *v);
FcResult FcPatternGetWithBinding (const (FcPattern) *p, const (char) *object, int id, FcValue *v, FcValueBinding *b);
int FcPatternDel (FcPattern *p, const (char) *object);
int FcPatternRemove (FcPattern *p, const (char) *object, int id);
int FcPatternAddInteger (FcPattern *p, const (char) *object, int i);
int FcPatternAddDouble (FcPattern *p, const (char) *object, double d);
int FcPatternAddString (FcPattern *p, const (char) *object, const (char) *s);
int FcPatternAddMatrix (FcPattern *p, const (char) *object, const (FcMatrix) *s);
int FcPatternAddCharSet (FcPattern *p, const (char) *object, const (FcCharSet) *c);
int FcPatternAddBool (FcPattern *p, const (char) *object, int b);
int FcPatternAddLangSet (FcPattern *p, const (char) *object, const (FcLangSet) *ls);
int FcPatternAddRange (FcPattern *p, const (char) *object, const (FcRange) *r);
FcResult FcPatternGetInteger (const (FcPattern) *p, const (char) *object, int n, int *i);
FcResult FcPatternGetDouble (const (FcPattern) *p, const (char) *object, int n, double *d);
FcResult FcPatternGetString (const (FcPattern) *p, const (char) *object, int n, char ** s);
FcResult FcPatternGetMatrix (const (FcPattern) *p, const (char) *object, int n, FcMatrix **s);
FcResult FcPatternGetCharSet (const (FcPattern) *p, const (char) *object, int n, FcCharSet **c);
FcResult FcPatternGetBool (const (FcPattern) *p, const (char) *object, int n, int *b);
FcResult FcPatternGetLangSet (const (FcPattern) *p, const (char) *object, int n, FcLangSet **ls);
FcResult FcPatternGetRange (const (FcPattern) *p, const (char) *object, int id, FcRange **r);
//FcPattern * FcPatternVaBuild (FcPattern *p, va_list va);
FcPattern * FcPatternBuild (FcPattern *p, ...);
char * FcPatternFormat (FcPattern *pat, const (char) *format);
FcRange * FcRangeCreateDouble (double begin, double end);
FcRange * FcRangeCreateInteger (uint begin, uint end);
void FcRangeDestroy (FcRange *range);
FcRange * FcRangeCopy (const (FcRange) *r);
int FcRangeGetDouble(const (FcRange) *range, double *begin, double *end);
int FcWeightFromOpenType (int ot_weight);
int FcWeightToOpenType (int fc_weight);
char * FcStrCopy (const (char) *s);
char * FcStrCopyFilename (const (char) *s);
char * FcStrPlus (const (char) *s1, const (char) *s2);
void FcStrFree (char *s);

//#define FcIsUpper(c) ((0101 <= (c) && (c) <= 0132))
//#define FcIsLower(c) ((0141 <= (c) && (c) <= 0172))
//#define FcToLower(c) (FcIsUpper(c) ? (c) - 0101 + 0141 : (c))

char * FcStrDowncase (const (char) *s);
int FcStrCmpIgnoreCase (const (char) *s1, const (char) *s2);
int FcStrCmp (const (char) *s1, const (char) *s2);
const (char) * FcStrStrIgnoreCase (const (char) *s1, const (char) *s2);
const (char) * FcStrStr (const (char) *s1, const (char) *s2);
int FcUtf8ToUcs4 (const (char) *src_orig, uint *dst, int len);
int FcUtf8Len (const (char) *string, int len, int *n_char, int *w_char);

enum FC_UTF8_MAX_LEN = 6;

int FcUcs4ToUtf8 (uint ucs4, char[FC_UTF8_MAX_LEN] dest); // not a pointer to a char?
int FcUtf16ToUcs4 (const (char) *src_orig, FcEndian endian, uint *dst, int len);
int FcUtf16Len (const (char) *string, FcEndian endian, int len, int *n_char, int *w_char);
char * FcStrDirname (const (char) *file);
char * FcStrBasename (const (char) *file);
FcStrSet * FcStrSetCreate ();
int FcStrSetMember (FcStrSet *set, const (char) *s);
int FcStrSetEqual (FcStrSet *sa, FcStrSet *sb);
int FcStrSetAdd (FcStrSet *set, const (char) *s);
int FcStrSetAddFilename (FcStrSet *set, const (char) *s);
int FcStrSetDel (FcStrSet *set, const (char) *s);
void FcStrSetDestroy (FcStrSet *set);
FcStrList * FcStrListCreate (FcStrSet *set);
void FcStrListFirst (FcStrList *list);
char * FcStrListNext (FcStrList *list);
void FcStrListDone (FcStrList *list);
int FcConfigParseAndLoad (FcConfig *config, const (char) *file, int complain);
int FcConfigParseAndLoadFromMemory (FcConfig *config, const (char) *buffer, int complain);

}

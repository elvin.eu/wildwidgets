module cairo.pattern;

import std.exception;

import cairo.classic;
import cairo.surface;
import cairo.matrix;

class CairoPattern
{
	private cairo_pattern_t* _pattern;

	cairo_pattern_t* pattern () {return _pattern;}
	alias pattern this;

	this (cairo_pattern_t* pattern, bool create_reference = true)
	{
		if (create_reference) _pattern = cairo_pattern_reference (pattern);
		else                  _pattern = pattern;
		enforce (status() == CairoStatus.SUCCESS);
	}

	~this ()
	{
		cairo_pattern_destroy (_pattern);
	}

	CairoStatus status ()
	{
		return cairo_pattern_status (_pattern);
	}

	void set_matrix (CairoMatrix matrix)
	{
		cairo_pattern_set_matrix (_pattern, matrix.ptr);
	}

	CairoMatrix get_matrix ()
	{
		CairoMatrix m;
		cairo_pattern_get_matrix (_pattern, m.ptr);
		return m;
	}

	void set_filter (CairoFilter filter)
	{
		cairo_pattern_set_filter (_pattern, filter);
	}

	CairoFilter get_filter ()
	{
		return cairo_pattern_get_filter (_pattern);
	}
}




class CairoSolidPattern : CairoPattern
{
	this (double red, double green, double blue, double alpha=1.0)
	{
		super (cairo_pattern_create_rgba (red, green, blue, alpha));
	}
}

class CairoSurfacePattern : CairoPattern
{
	this (CairoSurface surface)
	{
		super (cairo_pattern_create_for_surface (surface.surface));
	}
}

class CairoLinearPattern : CairoPattern
{
	this (double x0, double y0, double x1, double y1)
	{
		super(cairo_pattern_create_linear (x0, y0, x1, y1));
	}

	void add_color_stop_rgb (double offset, double red, double green, double blue)
	{
		cairo_pattern_add_color_stop_rgb (_pattern, offset, red, green, blue);
	}

	void add_color_stop_rgba (double offset, double red, double green, double blue, double alpha=1.0)
	{
		cairo_pattern_add_color_stop_rgba (_pattern, offset, red, green, blue, alpha);
	}

	void get_linear_points (out double x0, out double y0, out double x1, out double y1)
	{
		cairo_pattern_get_linear_points (_pattern, &x0, &y0, &x1, &y1);
	}

	CairoStatus get_color_stop (int index, out double offset, out double red, out double green, out double blue, out double alpha)
	{
		return cairo_pattern_get_color_stop_rgba (_pattern, index, &offset, &red, &green, &blue, &alpha);
	}
}

class CairoRadialPattern : CairoPattern
{
	this (double cx0, double cy0, double radius0, double cx1, double cy1, double radius1)
	{
		super (cairo_pattern_create_radial (cx0, cy0, radius0, cx1, cy1, radius1));
	}

	void add_color_stop_rgb (double offset, double red, double green, double blue)
	{
		cairo_pattern_add_color_stop_rgb (_pattern, offset, red, green, blue);
	}

	void add_color_stop_rgba (double offset, double red, double green, double blue, double alpha=1.0)
	{
		cairo_pattern_add_color_stop_rgba (_pattern, offset, red, green, blue, alpha);
	}

	CairoStatus get_color_stop (int index, out double offset, out double red, out double green, out double blue, out double alpha)
	{
		return cairo_pattern_get_color_stop_rgba (_pattern, index, &offset, &red, &green, &blue, &alpha);
	}
}

class CairoMeshPattern : CairoPattern
{
	this ()
	{
		super (cairo_pattern_create_mesh ());
	}
}

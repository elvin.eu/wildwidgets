module cairo.surface;

static import X11;

import cairo.classic;
import cairo.context;

import std.exception;
import std.string;

class CairoSurface
{
private:
	cairo_surface_t* _surface;

	void surface (cairo_surface_t* s)
	{
		assert (s);
		_surface = s;
		enforce (status() == CairoStatus.SUCCESS);
	}

	this () {}

public:

	cairo_surface_t* surface()
	{
		assert (_surface);
		return _surface;
	}

	~this ()
	{
		cairo_surface_destroy (_surface);
	}

	static CairoSurface create_reference (cairo_surface_t* sf)
	{
		auto s = new CairoSurface ();
		s.surface = cairo_surface_reference (sf);
		return s;
	}

	static CairoSurface create_for_rectangle (CairoSurface sf, double x, double y, double width, double height)
	{
		auto s = new CairoSurface ();
		s.surface = cairo_surface_create_for_rectangle (sf.surface, x, y, width, height);
		return s;
	}

	static CairoSurface create_similar (CairoSurface sf, CairoContent content, int width, int height)
	{
		auto s = new CairoSurface ();
		s.surface = cairo_surface_create_similar (sf.surface, content, width, height);
		return s;
	}

	void copy_page ()
	{
		cairo_surface_copy_page (_surface);
	}

	void finish ()
	{
		cairo_surface_finish (_surface);
	}

	void flush ()
	{
		cairo_surface_flush (_surface);
	}
/*
	CairoContent cairo_surface_get_content ()
	{
		return cairo_surface_get_content (_surface);
	}

	CairoDevice cairo_surface_get_device ()
	{
		return cairo_surface_get_device (_surface);
	}
*/
	void get_device_offset (out double x_offset, out double y_offset)
	{
		cairo_surface_get_device_offset (_surface, &x_offset, &y_offset);
	}

	void get_fallback_resolution (out double x_pixels_per_inch, out double y_pixels_per_inch)
	{
		cairo_surface_get_fallback_resolution (_surface, &x_pixels_per_inch, &y_pixels_per_inch);
	}
/*
	void cairo_surface_get_font_options (, cairo_font_options_t* options)
	{
		void cairo_surface_get_font_options (_surface, cairo_font_options_t* options);
	}

	void cairo_surface_get_mime_data (, const char* mime_type, const char** data, ulong* length)
	{
		void cairo_surface_get_mime_data (_surface, const char* mime_type, const char** data, ulong* length);
	}
*/

	CairoSurfaceType get_type ()
	{
		return cairo_surface_get_type (_surface);
	}
/*
	void* cairo_surface_get_user_data (, const cairo_user_data_key_t* key)
	{
		void* cairo_surface_get_user_data (_surface, const cairo_user_data_key_t* key);
	}

	cairo_bool_t cairo_surface_has_show_text_glyphs ()
	{
		cairo_bool_t cairo_surface_has_show_text_glyphs (_surface);
	}

	cairo_surface_t* cairo_surface_map_to_image (, const cairo_rectangle_int_t* extents)
	{
		cairo_surface_t* cairo_surface_map_to_image (_surface, const cairo_rectangle_int_t* extents);
	}

	void unmap_image (CairoImageSurface image)
	{
		cairo_surface_unmap_image (_surface, image.ptr);
	}
*/
	void mark_dirty ()
	{
		cairo_surface_mark_dirty (_surface);
	}

	void mark_dirty_rectangle (int x, int y, int width, int height)
	{
		cairo_surface_mark_dirty_rectangle (_surface, x, y, width, height);
	}

	void set_device_offset (double x_offset, double y_offset)
	{
		cairo_surface_set_device_offset (_surface, x_offset, y_offset);
	}

	void set_fallback_resolution (double x_pixels_per_inch, double y_pixels_per_inch)
	{
		cairo_surface_set_fallback_resolution (_surface, x_pixels_per_inch, y_pixels_per_inch);
	}
/*
	cairo_status_t cairo_surface_set_mime_data (const char* mime_type, const char* data, ulong length, cairo_destroy_func_t destroy, void* closure)
	{
		cairo_status_t cairo_surface_set_mime_data (_surface, const char* mime_type, const char* data, ulong length, cairo_destroy_func_t destroy, void* closure);
	}

	cairo_status_t cairo_surface_set_user_data (, const cairo_user_data_key_t* key, void* user_data, cairo_destroy_func_t destroy)
	{
		cairo_status_t cairo_surface_set_user_data (_surface, const cairo_user_data_key_t* key, void* user_data, cairo_destroy_func_t destroy);
	}
*/
	void show_page ()
	{
		cairo_surface_show_page (_surface);
	}

	CairoStatus status ()
	{
		return  cairo_surface_status (_surface);
	}

	bool supports_mime_type (string mime_type)
	{
		return cast(bool) cairo_surface_supports_mime_type (_surface, mime_type.toStringz);
	}

	CairoStatus write_to_png (string filename)
	{
		return cairo_surface_write_to_png (_surface, filename.toStringz);
	}
/*
	cairo_status_t cairo_surface_write_to_png_stream (, cairo_write_func_t write_func, void* closure)
	{
		cairo_status_t cairo_surface_write_to_png_stream (_surface, cairo_write_func_t write_func, void* closure);
	}
*/
// uint cairo_surface_get_reference_count ()
}


class CairoImageSurface : CairoSurface
{
	private this () {}

	this (CairoFormat format, int width, int height)
	{
		surface = cairo_image_surface_create (format, width, height);
	}

	static CairoImageSurface create_similar_image (CairoImageSurface sf, CairoFormat format, int width, int height)
	{
		auto s = new CairoImageSurface();
		s.surface = cairo_surface_create_similar_image (sf._surface, format, width, height);
		return s;
	}

	static CairoImageSurface create_for_data (byte[] data, CairoFormat format, int width, int height, int stride)
	{
		auto s = new CairoImageSurface();
		s.surface = cairo_image_surface_create_for_data (cast(char*) data, format, width, height, stride);
		return s;
	}

	static CairoImageSurface create_from_png_file (string png_filename)
	{
		auto s = new CairoImageSurface();
		s.surface = cairo_image_surface_create_from_png (png_filename.toStringz);
		return s;
	}

	private struct png_closure {int pos; const ubyte[] data;}

	private static extern (C) cairo_status_t read_png (void* closure, ubyte *target, uint length)
	{
		png_closure* c = cast(png_closure*)closure;

		for (int i=0; i<length; i++)
		{
			if (c.pos >= c.data.length) return CAIRO_STATUS_READ_ERROR;
			target[i] = c.data[c.pos];
			c.pos++;
		}

		return CAIRO_STATUS_SUCCESS;
	}

	static CairoImageSurface create_from_png_data (const ubyte[] data)
	{
		enforce (data[0..8] == [137,80,78,71,13,10,26,10]);

		png_closure closure = {0, data};
		auto s = new CairoImageSurface;
		s.surface = cairo_image_surface_create_from_png_stream (&read_png, cast(void*)&closure);
		return s;
	}

/*
cairo_surface_t* cairo_image_surface_create_from_png_stream (cairo_read_func_t read_func, void* closure);
*/


	char* get_data ()
	{
		return cairo_image_surface_get_data (_surface);
	}

	CairoFormat get_format ()
	{
		return cairo_image_surface_get_format (_surface);
	}

	int get_height ()
	{
		return cairo_image_surface_get_height (_surface);
	}

	int get_stride ()
	{
		return cairo_image_surface_get_stride (_surface);
	}

	int get_width ()
	{
		return cairo_image_surface_get_width (_surface);
	}
}

class CairoSvgSurface : CairoSurface
{
	private this() {}

	this (string fn, double width, double height)
	{
		if (fn == null || fn == "") surface = cairo_svg_surface_create (null, width, height);
		else                        surface = cairo_svg_surface_create (fn.toStringz, width, height);
	}

	static CairoSvgSurface create_for_stream (cairo_write_func_t func,
		void *closure,
		double width_in_points,
		double height_in_points)
	{
		auto s = new CairoSvgSurface;
		s.surface = cairo_svg_surface_create_for_stream (func, closure, width_in_points, height_in_points);
		return s;
	}

	void restrict_to_version (CairoSvgVersion v)
	{
		cairo_svg_surface_restrict_to_version (_surface, v);
	}

	static CairoSvgVersion[] get_versions ()
	{
		CairoSvgVersion *vptr;
		int n;
		cairo_svg_get_versions (&vptr, &n);

		CairoSvgVersion[] v;
		for (int i=0; i<n; i++) v ~= *vptr++;
		return v;
	}

	static string version_to_string (CairoSvgVersion v)
	{
		auto s = cairo_svg_version_to_string (v);
		return s.fromStringz.idup;
	}

	void set_document_unit (CairoSvgUnit unit)
	{
		cairo_svg_surface_set_document_unit (_surface, unit);
	}

	CairoSvgUnit get_document_unit ()
	{
		return cairo_svg_surface_get_document_unit (_surface);
	}
}

class CairoXlibSurface : CairoSurface
{
	private this () {}

	this (X11.Display* dpy, X11.Drawable drawable, X11.Visual* visual, int width, int height)
	{
		surface = cairo_xlib_surface_create (dpy, drawable, visual, width, height);
	}

	CairoXlibSurface create_for_bitmap (X11.Display* dpy, X11.Pixmap bitmap, X11.Screen *screen, int width, int height)
	{
		auto s = new CairoXlibSurface;
		s.surface = cairo_xlib_surface_create_for_bitmap (dpy, bitmap, screen, width, height);
		return s;
	}

	void set_size (int width, int height)
	{
		cairo_xlib_surface_set_size (_surface, width, height);
	}

	void set_drawable (X11.Drawable drawable, int width, int height)
	{
		cairo_xlib_surface_set_drawable (_surface, drawable, width, height);
	}

	X11.Display* get_display ()
	{
		return cairo_xlib_surface_get_display (_surface);
	}

	X11.Drawable get_drawable ()
	{
		return cairo_xlib_surface_get_drawable (_surface);
	}

	X11.Screen* get_screen ()
	{
		return cairo_xlib_surface_get_screen (_surface);
	}

	X11.Visual* get_visual ()
	{
		return cairo_xlib_surface_get_visual (_surface);
	}

	int get_depth ()
	{
		return cairo_xlib_surface_get_depth (_surface);
	}

	int get_width ()
	{
		return cairo_xlib_surface_get_width (_surface);
	}

	int get_height ()
	{
		return cairo_xlib_surface_get_height (_surface);
	}
}

class CairoPsSurface : CairoSurface
{
	private this () {}

	this (string filename, double width_in_points, double height_in_points)
	{
		surface = cairo_ps_surface_create (toStringz(filename), width_in_points, height_in_points);
	}

	static CairoPsSurface create_for_stream (cairo_write_func_t func,
		void *closure,
		double width_in_points,
		double height_in_points)
	{
		auto s = new CairoPsSurface;
		s.surface = cairo_ps_surface_create_for_stream (func, closure, width_in_points, height_in_points);
		return s;
	}

	void restrict_to_level (cairo_ps_level_t level)
	{
		cairo_ps_surface_restrict_to_level (_surface, level);
	}

	immutable (cairo_ps_level_t)[] get_levels ()
	{
		cairo_ps_level_t *level_ptr;
		int num_levels;
		cairo_ps_get_levels (&level_ptr, &num_levels);

		cairo_ps_level_t[] l;
		foreach (i; 0..num_levels) l ~= *(level_ptr+i);

		return cast (immutable) l;
	}

	static string level_to_string (cairo_ps_level_t level)
	{
		auto str = cairo_ps_level_to_string (level);
		return cast (immutable) fromStringz(str);
	}

	void set_eps (bool eps)
	{
		cairo_ps_surface_set_eps (_surface,	cast (cairo_bool_t) eps);
	}

	bool get_eps ()
	{
		return cast (bool) cairo_ps_surface_get_eps (_surface);
	}

	void set_size (double width_in_points, double height_in_points)
	{
		cairo_ps_surface_set_size (_surface, width_in_points, height_in_points);
	}

	void dsc_comment (string comment)
	{
		auto stringz = toStringz(comment);
		cairo_ps_surface_dsc_comment (_surface, stringz);
	}

	void dsc_begin_setup ()
	{
		cairo_ps_surface_dsc_begin_setup (_surface);
	}

	void dsc_begin_page_setup ()
	{
		cairo_ps_surface_dsc_begin_page_setup (_surface);
	}
}
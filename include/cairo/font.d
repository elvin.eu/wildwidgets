module cairo.font;

import std.string;
import std.exception;
import std.conv;

import freetype2;
import fontconfig;

import cairo.classic;
import cairo.matrix;

alias CairoFontExtents = cairo_font_extents_t;
alias CairoTextExtents = cairo_text_extents_t;
alias CairoGlyph = cairo_glyph_t;
alias CairoTextCluster = cairo_text_cluster_t;

class CairoFontFace
{
	private cairo_font_face_t* _font_face;

	inout (cairo_font_face_t*) font_face () inout {return _font_face;}
	alias font_face this;

	this (cairo_font_face_t *font_face, bool create_reference = true)
	{
		if (create_reference) _font_face = cairo_font_face_reference (font_face);
		else                  _font_face = font_face;
		enforce (status () == CAIRO_STATUS_SUCCESS);
	}

	~this ()
	{
		cairo_font_face_destroy (_font_face);
	}

	CairoFontType get_type ()
	{
		return cairo_font_face_get_type (_font_face);
	}

	CairoStatus status ()
	{
		return cairo_font_face_status (_font_face);
	}
}

class CairoFtFontFace : CairoFontFace
{
	this (FT_Face face, int flags)
	{
		super (cairo_ft_font_face_create_for_ft_face (face, flags), false);
	}

	this (FcPattern* pattern)
	{
		super (cairo_ft_font_face_create_for_pattern (pattern));
	}
}

class CairoFontOptions
{
	private cairo_font_options_t* _options;

	inout (cairo_font_options_t*) options () inout {return _options;}
	alias options this;

	this()
	{
		_options = cairo_font_options_create ();
		enforce (status () == CAIRO_STATUS_SUCCESS);
	}

	this (cairo_font_options_t *options)
	{
		_options = options;
		enforce (status () == CAIRO_STATUS_SUCCESS);
	}

	this (CairoFontOptions original)
	{
		_options = cairo_font_options_copy (original);
		enforce (status () == CAIRO_STATUS_SUCCESS);
	}

	~this()
	{
		cairo_font_options_destroy (_options);
	}

	CairoStatus status ()
	{
		return cairo_font_options_status (_options);
	}

	void merge (const CairoFontOptions other)
	{
		cairo_font_options_merge (_options, other);
	}

	bool equal (const CairoFontOptions other)
	{
		return cast(bool) cairo_font_options_equal (_options, other);
	}

	ulong hash ()
	{
		return cairo_font_options_hash (_options);
	}

	void set_antialias (CairoAntialias antialias)
	{
		cairo_font_options_set_antialias (_options, antialias);
	}

	CairoAntialias get_antialias ()
	{
		return cairo_font_options_get_antialias (_options);
	}

	void set_subpixel_order (CairoSubpixelOrder subpixel_order)
	{
		cairo_font_options_set_subpixel_order (_options, subpixel_order);
	}

	CairoSubpixelOrder get_subpixel_order ()
	{
		return cairo_font_options_get_subpixel_order (_options);
	}

	void set_hint_style (CairoHintStyle hint_style)
	{
		cairo_font_options_set_hint_style (_options, hint_style);
	}

	CairoHintStyle get_hint_style ()
	{
		return cairo_font_options_get_hint_style (_options);
	}

	void set_hint_metrics (CairoHintMetrics hint_metrics)
	{
		cairo_font_options_set_hint_metrics (_options, hint_metrics);
	}

	CairoHintMetrics get_hint_metrics ()
	{
		return cairo_font_options_get_hint_metrics (_options);
	}
}

class CairoScaledFont
{
	private cairo_scaled_font_t* _scaled_font;

	inout (cairo_scaled_font_t*) scaled_font () inout {return _scaled_font;}
	alias scaled_font this;

	this (cairo_scaled_font_t *font, bool create_reference = true)
	{
		if (create_reference) _scaled_font = cairo_scaled_font_reference (font);
		else                  _scaled_font = font;
		enforce (status () == CAIRO_STATUS_SUCCESS);
	}

	this (CairoFontFace font_face, in CairoMatrix font_matrix, in CairoMatrix ctm, in CairoFontOptions options)
	{
		_scaled_font = cairo_scaled_font_create (font_face, font_matrix.ptr, ctm.ptr, options);
	}

	~this ()
	{
		cairo_scaled_font_destroy (_scaled_font);
	}

	CairoStatus status ()
	{
		return cairo_scaled_font_status (_scaled_font);
	}

	CairoFontType get_type ()
	{
		return cairo_scaled_font_get_type (_scaled_font);
	}

	CairoFontExtents extents ()
	{
		CairoFontExtents e;
		cairo_scaled_font_extents (_scaled_font, &e);
		return e;
	}

	CairoTextExtents text_extents (const char[] text)
	{
		CairoTextExtents e;
		cairo_scaled_font_text_extents (_scaled_font, text.toStringz(), &e);
		return e;
	}

	CairoTextExtents glyph_extents (const CairoGlyph[] glyphs)
	{
		CairoTextExtents e;
		cairo_scaled_font_glyph_extents (_scaled_font, glyphs.ptr, cast(int)glyphs.length, &e);
		return e;
	}

	CairoGlyph[] text_to_glyphs (double x, double y, const char[] text)
	{
		auto count = text.count(); 			// number of code points

		CairoGlyph[] glyphs;
		glyphs.length = count;
		cairo_glyph_t* g_ptr = glyphs.ptr;
		int g_len = count.to!int;

		auto status = cairo_scaled_font_text_to_glyphs (_scaled_font, x, y, text.toStringz, text.length.to!int, &g_ptr, &g_len, null, null, null);
		assert (g_ptr == glyphs.ptr);
		assert (glyphs.length == g_len);
/*
		if (g_ptr != g_ptr_orig)
		{		if (c_ptr != c_ptr_orig)
		{
			writeln (__FILE__, " ", __LINE__, ": ups, text cluster array was too short");
			clusters.length = 0;
			for (int i=0; i<c_len; i++) clusters ~= c_ptr[i];
			cairo_text_clusters_free (g_ptr);
		}

			writeln (__FILE__, " ", __LINE__, ": ups, glyph array was too short");
			glyphs.length = 0;
			for (int i=0; i<g_len; i++) glyphs ~= g_ptr[i];
			cairo_glyph_free (g_ptr);
		}
*/
		if (status != CairoStatus.SUCCESS) glyphs.length = 0;
		return glyphs;
	}

	CairoStatus text_to_glyphs (double x, double y, const char[] text, out CairoGlyph[] glyphs, out CairoTextCluster[] clusters, out CairoTextClusterFlags flags)
	{
		auto count = text.count(); 			// number of code points

		cairo_glyph_t* g_ptr = glyphs.ptr;
		glyphs.length = count;
		int g_len = count.to!int;

		cairo_text_cluster_t* c_ptr = clusters.ptr;
		clusters.length = count;
		int c_len = count.to!int;

		auto status = cairo_scaled_font_text_to_glyphs (_scaled_font, x, y, text.toStringz, text.length.to!int, &g_ptr, &g_len, &c_ptr, &c_len, &flags);
		assert (c_ptr == clusters.ptr);
		assert (g_ptr == glyphs.ptr);
		assert (glyphs.length == g_len);

		clusters.length = c_len;
/*
		if (c_ptr != c_ptr_orig)
		{
			writeln (__FILE__, " ", __LINE__, ": ups, text cluster array was too short");
			clusters.length = 0;
			for (int i=0; i<c_len; i++) clusters ~= c_ptr[i];
			cairo_text_clusters_free (g_ptr);
		}

		if (g_ptr != g_ptr_orig)
		{
			writeln (__FILE__, " ", __LINE__, ": ups, glyph array was too short");
			glyphs.length = 0;
			for (int i=0; i<g_len; i++) glyphs ~= g_ptr[i];
			cairo_glyph_free (g_ptr);
		}
*/
		return status;
	}

	CairoFontFace get_font_face ()
	{
		return new CairoFontFace (cairo_scaled_font_get_font_face (_scaled_font));
	}

	CairoMatrix get_font_matrix ()
	{
		CairoMatrix m;
		cairo_scaled_font_get_font_matrix (_scaled_font, m.ptr);
		return m;
	}

	CairoMatrix get_ctm ()
	{
		CairoMatrix m;
		cairo_scaled_font_get_ctm (_scaled_font, m.ptr);
		return m;
	}

	CairoMatrix get_scale_matrix ()
	{
		CairoMatrix m;
		cairo_scaled_font_get_scale_matrix (_scaled_font, m.ptr);
		return m;
	}

	CairoFontOptions get_font_options ()
	{
		auto o = new CairoFontOptions ();
		cairo_scaled_font_get_font_options (_scaled_font, o);
		return (o);
	}
}

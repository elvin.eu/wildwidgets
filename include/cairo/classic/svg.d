module cairo.classic.svg;

import cairo.classic;

enum cairo_svg_version_t
{
    _1_1,
    _1_2
};

alias CairoSvgVersion = cairo_svg_version_t;
alias CAIRO_SVG_VERSION_1_1 = cairo_svg_version_t._1_1;
alias CAIRO_SVG_VERSION_1_2 = cairo_svg_version_t._1_2;

enum cairo_svg_unit_t
{
    USER = 0,
    EM,
    EX,
    PX,
    IN,
    CM,
    MM,
    PT,
    PC,
    PERCENT
};

alias CairoSvgUnit = cairo_svg_unit_t;
alias CAIRO_SVG_UNIT_USER = cairo_svg_unit_t.USER;
alias CAIRO_SVG_UNIT_EM = cairo_svg_unit_t.EM;
alias CAIRO_SVG_UNIT_EX = cairo_svg_unit_t.EX;
alias CAIRO_SVG_UNIT_PX = cairo_svg_unit_t.PX;
alias CAIRO_SVG_UNIT_IN = cairo_svg_unit_t.IN;
alias CAIRO_SVG_UNIT_CM = cairo_svg_unit_t.CM;
alias CAIRO_SVG_UNIT_MM = cairo_svg_unit_t.MM;
alias CAIRO_SVG_UNIT_PT = cairo_svg_unit_t.PT;
alias CAIRO_SVG_UNIT_PC = cairo_svg_unit_t.PC;
alias CAIRO_SVG_UNIT_PERCENT = cairo_svg_unit_t.PERCENT;

extern (C)
{
	cairo_surface_t* cairo_svg_surface_create (const char* filename, double width_in_points, double height_in_points);

	cairo_surface_t* cairo_svg_surface_create_for_stream (cairo_write_func_t write_func, void* closure, double width_in_points, double height_in_points);

	void cairo_svg_surface_restrict_to_version (cairo_surface_t* surface, cairo_svg_version_t v);

	void cairo_svg_get_versions (const cairo_svg_version_t* *versions, int* num_versions);

	const (char*) cairo_svg_version_to_string (cairo_svg_version_t v);

	void cairo_svg_surface_set_document_unit (cairo_surface_t* surface, cairo_svg_unit_t unit);

	cairo_svg_unit_t cairo_svg_surface_get_document_unit (cairo_surface_t* surface);
}

module cairo.classic;

public import cairo.classic.cairo;
public import cairo.classic.enums;
public import cairo.classic.ft;
public import cairo.classic.svg;
public import cairo.classic.ps;

version (X11) public import cairo.classic.xlib;
version (XCB) public import cairo.classic.xcb;

module cairo.classic.enums;

enum CAIRO_HAS_EGL_FUNCTIONS = 1;
enum CAIRO_HAS_FC_FONT = 1;
enum CAIRO_HAS_FT_FONT = 1;
enum CAIRO_HAS_GLX_FUNCTIONS = 1;
enum CAIRO_HAS_GL_SURFACE = 1;
enum CAIRO_HAS_GOBJECT_FUNCTIONS = 1;
enum CAIRO_HAS_IMAGE_SURFACE = 1;
enum CAIRO_HAS_MIME_SURFACE = 1;
enum CAIRO_HAS_OBSERVER_SURFACE = 1;
enum CAIRO_HAS_PDF_SURFACE = 1;
enum CAIRO_HAS_PNG_FUNCTIONS = 1;
enum CAIRO_HAS_PS_SURFACE = 1;
enum CAIRO_HAS_RECORDING_SURFACE = 1;
enum CAIRO_HAS_SCRIPT_SURFACE = 1;
enum CAIRO_HAS_SVG_SURFACE = 1;
enum CAIRO_HAS_TEE_SURFACE = 1;
enum CAIRO_HAS_USER_FONT = 1;
enum CAIRO_HAS_XCB_SHM_FUNCTIONS = 1;
enum CAIRO_HAS_XCB_SURFACE = 1;
enum CAIRO_HAS_XLIB_SURFACE = 1;
enum CAIRO_HAS_XLIB_XRENDER_SURFACE = 1;

enum CAIRO_VERSION_MAJOR = 1;
enum CAIRO_VERSION_MINOR = 12;
enum CAIRO_VERSION_MICRO = 16;

enum cairo_status_t
{
	SUCCESS = 0,

	NO_MEMORY,
	INVALID_RESTORE,
	INVALID_POP_GROUP,
	NO_CURRENT_POINT,
	INVALID_MATRIX,
	INVALID_STATUS,
	NULL_POINTER,
	INVALID_STRING,
	INVALID_PATH_DATA,
	READ_ERROR,
	WRITE_ERROR,
	SURFACE_FINISHED,
	SURFACE_TYPE_MISMATCH,
	PATTERN_TYPE_MISMATCH,
	INVALID_CONTENT,
	INVALID_FORMAT,
	INVALID_VISUAL,
	FILE_NOT_FOUND,
	INVALID_DASH,
	INVALID_DSC_COMMENT,
	INVALID_INDEX,
	CLIP_NOT_REPRESENTABLE,
	TEMP_FILE_ERROR,
	INVALID_STRIDE,
	FONT_TYPE_MISMATCH,
	USER_FONT_IMMUTABLE,
	USER_FONT_ERROR,
	NEGATIVE_COUNT,
	INVALID_CLUSTERS,
	INVALID_SLANT,
	INVALID_WEIGHT,
	INVALID_SIZE,
	USER_FONT_NOT_IMPLEMENTED,
	DEVICE_TYPE_MISMATCH,
	DEVICE_ERROR,
	INVALID_MESH_CONSTRUCTION,
	DEVICE_FINISHED,
	JBIG2_GLOBAL_MISSING,
	PNG_ERROR,
	FREETYPE_ERROR,
	WIN32_GDI_ERROR,
	TAG_ERROR,

	LAST_STATUS
}

alias CAIRO_STATUS_SUCCESS = cairo_status_t.SUCCESS;
alias CAIRO_STATUS_NO_MEMORY = cairo_status_t.NO_MEMORY;
alias CAIRO_STATUS_INVALID_RESTORE = cairo_status_t.INVALID_RESTORE;
alias CAIRO_STATUS_INVALID_POP_GROUP = cairo_status_t.INVALID_POP_GROUP;
alias CAIRO_STATUS_NO_CURRENT_POINT = cairo_status_t.NO_CURRENT_POINT;
alias CAIRO_STATUS_INVALID_MATRIX = cairo_status_t.INVALID_MATRIX;
alias CAIRO_STATUS_INVALID_STATUS = cairo_status_t.INVALID_STATUS;
alias CAIRO_STATUS_NULL_POINTER = cairo_status_t.NULL_POINTER;
alias CAIRO_STATUS_INVALID_STRING = cairo_status_t.INVALID_STRING;
alias CAIRO_STATUS_INVALID_PATH_DATA = cairo_status_t.INVALID_PATH_DATA;
alias CAIRO_STATUS_READ_ERROR = cairo_status_t.READ_ERROR;
alias CAIRO_STATUS_WRITE_ERROR = cairo_status_t.WRITE_ERROR;
alias CAIRO_STATUS_SURFACE_FINISHED = cairo_status_t.SURFACE_FINISHED;
alias CAIRO_STATUS_SURFACE_TYPE_MISMATCH = cairo_status_t.SURFACE_TYPE_MISMATCH;
alias CAIRO_STATUS_PATTERN_TYPE_MISMATCH = cairo_status_t.PATTERN_TYPE_MISMATCH;
alias CAIRO_STATUS_INVALID_CONTENT = cairo_status_t.INVALID_CONTENT;
alias CAIRO_STATUS_INVALID_FORMAT = cairo_status_t.INVALID_FORMAT;
alias CAIRO_STATUS_INVALID_VISUAL = cairo_status_t.INVALID_VISUAL;
alias CAIRO_STATUS_FILE_NOT_FOUND = cairo_status_t.FILE_NOT_FOUND;
alias CAIRO_STATUS_INVALID_DASH = cairo_status_t.INVALID_DASH;
alias CAIRO_STATUS_INVALID_DSC_COMMENT = cairo_status_t.INVALID_DSC_COMMENT;
alias CAIRO_STATUS_INVALID_INDEX = cairo_status_t.INVALID_INDEX;
alias CAIRO_STATUS_CLIP_NOT_REPRESENTABLE = cairo_status_t.CLIP_NOT_REPRESENTABLE;
alias CAIRO_STATUS_TEMP_FILE_ERROR = cairo_status_t.TEMP_FILE_ERROR;
alias CAIRO_STATUS_INVALID_STRIDE = cairo_status_t.INVALID_STRIDE;
alias CAIRO_STATUS_FONT_TYPE_MISMATCH = cairo_status_t.FONT_TYPE_MISMATCH;
alias CAIRO_STATUS_USER_FONT_IMMUTABLE = cairo_status_t.USER_FONT_IMMUTABLE;
alias CAIRO_STATUS_USER_FONT_ERROR = cairo_status_t.USER_FONT_ERROR;
alias CAIRO_STATUS_NEGATIVE_COUNT = cairo_status_t.NEGATIVE_COUNT;
alias CAIRO_STATUS_INVALID_CLUSTERS = cairo_status_t.INVALID_CLUSTERS;
alias CAIRO_STATUS_INVALID_SLANT = cairo_status_t.INVALID_SLANT;
alias CAIRO_STATUS_INVALID_WEIGHT = cairo_status_t.INVALID_WEIGHT;
alias CAIRO_STATUS_INVALID_SIZE = cairo_status_t.INVALID_SIZE;
alias CAIRO_STATUS_USER_FONT_NOT_IMPLEMENTED = cairo_status_t.USER_FONT_NOT_IMPLEMENTED;
alias CAIRO_STATUS_DEVICE_TYPE_MISMATCH = cairo_status_t.DEVICE_TYPE_MISMATCH;
alias CAIRO_STATUS_DEVICE_ERROR = cairo_status_t.DEVICE_ERROR;
alias CAIRO_STATUS_INVALID_MESH_CONSTRUCTION = cairo_status_t.INVALID_MESH_CONSTRUCTION;
alias CAIRO_STATUS_DEVICE_FINISHED = cairo_status_t.DEVICE_FINISHED;
alias CAIRO_STATUS_JBIG2_GLOBAL_MISSING = cairo_status_t.JBIG2_GLOBAL_MISSING;
alias CAIRO_STATUS_PNG_ERROR = cairo_status_t.PNG_ERROR;
alias CAIRO_STATUS_FREETYPE_ERROR = cairo_status_t.FREETYPE_ERROR;
alias CAIRO_STATUS_WIN32_GDI_ERROR = cairo_status_t.WIN32_GDI_ERROR;
alias CAIRO_STATUS_TAG_ERROR = cairo_status_t.TAG_ERROR;
alias CAIRO_STATUS_LAST_STATUS = cairo_status_t.LAST_STATUS;


enum cairo_content_t
{
    COLOR		= 0x1000,
    ALPHA		= 0x2000,
    COLOR_ALPHA	= 0x3000
}

alias CAIRO_CONTENT_COLOR = cairo_content_t.COLOR;
alias CAIRO_CONTENT_ALPHA = cairo_content_t.ALPHA;
alias CAIRO_CONTENT_COLOR_ALPHA = cairo_content_t.COLOR_ALPHA;

enum cairo_format_t {
    INVALID   = -1,
    ARGB32    = 0,
    RGB24     = 1,
    A8        = 2,
    A1        = 3,
    RGB16_565 = 4,
    RGB30     = 5
}

alias CAIRO_FORMAT_INVALID = cairo_format_t.INVALID;
alias CAIRO_FORMAT_ARGB32 = cairo_format_t.ARGB32;
alias CAIRO_FORMAT_RGB24 = cairo_format_t.RGB24;
alias CAIRO_FORMAT_A8 = cairo_format_t.A8;
alias CAIRO_FORMAT_A1 = cairo_format_t.A1;
alias CAIRO_FORMAT_RGB16_565 = cairo_format_t.RGB16_565;
alias CAIRO_FORMAT_RGB30 = cairo_format_t.RGB30;

enum cairo_operator_t
{
    CLEAR,
    SOURCE,
    OVER,
    IN,
    OUT,
    ATOP,
    DEST,
    DEST_OVER,
    DEST_IN,
    DEST_OUT,
    DEST_ATOP,
    XOR,
    ADD,
    SATURATE,
    MULTIPLY,
    SCREEN,
    OVERLAY,
    DARKEN,
    LIGHTEN,
    COLOR_DODGE,
    COLOR_BURN,
    HARD_LIGHT,
    SOFT_LIGHT,
    DIFFERENCE,
    EXCLUSION,
    HSL_HUE,
    HSL_SATURATION,
    HSL_COLOR,
    HSL_LUMINOSITY
}

alias CAIRO_OPERATOR_T_CLEAR = cairo_operator_t.CLEAR;
alias CAIRO_OPERATOR_T_SOURCE = cairo_operator_t.SOURCE;
alias CAIRO_OPERATOR_T_OVER = cairo_operator_t.OVER;
alias CAIRO_OPERATOR_T_IN = cairo_operator_t.IN;
alias CAIRO_OPERATOR_T_OUT = cairo_operator_t.OUT;
alias CAIRO_OPERATOR_T_ATOP = cairo_operator_t.ATOP;
alias CAIRO_OPERATOR_T_DEST = cairo_operator_t.DEST;
alias CAIRO_OPERATOR_T_DEST_OVER = cairo_operator_t.DEST_OVER;
alias CAIRO_OPERATOR_T_DEST_IN = cairo_operator_t.DEST_IN;
alias CAIRO_OPERATOR_T_DEST_OUT = cairo_operator_t.DEST_OUT;
alias CAIRO_OPERATOR_T_DEST_ATOP = cairo_operator_t.DEST_ATOP;
alias CAIRO_OPERATOR_T_XOR = cairo_operator_t.XOR;
alias CAIRO_OPERATOR_T_ADD = cairo_operator_t.ADD;
alias CAIRO_OPERATOR_T_SATURATE = cairo_operator_t.SATURATE;
alias CAIRO_OPERATOR_T_MULTIPLY = cairo_operator_t.MULTIPLY;
alias CAIRO_OPERATOR_T_SCREEN = cairo_operator_t.SCREEN;
alias CAIRO_OPERATOR_T_OVERLAY = cairo_operator_t.OVERLAY;
alias CAIRO_OPERATOR_T_DARKEN = cairo_operator_t.DARKEN;
alias CAIRO_OPERATOR_T_LIGHTEN = cairo_operator_t.LIGHTEN;
alias CAIRO_OPERATOR_T_COLOR_DODGE = cairo_operator_t.COLOR_DODGE;
alias CAIRO_OPERATOR_T_COLOR_BURN = cairo_operator_t.COLOR_BURN;
alias CAIRO_OPERATOR_T_HARD_LIGHT = cairo_operator_t.HARD_LIGHT;
alias CAIRO_OPERATOR_T_SOFT_LIGHT = cairo_operator_t.SOFT_LIGHT;
alias CAIRO_OPERATOR_T_DIFFERENCE = cairo_operator_t.DIFFERENCE;
alias CAIRO_OPERATOR_T_EXCLUSION = cairo_operator_t.EXCLUSION;
alias CAIRO_OPERATOR_T_HSL_HUE = cairo_operator_t.HSL_HUE;
alias CAIRO_OPERATOR_T_HSL_SATURATION = cairo_operator_t.HSL_SATURATION;
alias CAIRO_OPERATOR_T_HSL_COLOR = cairo_operator_t.HSL_COLOR;
alias CAIRO_OPERATOR_T_HSL_LUMINOSITY= cairo_operator_t.HSL_LUMINOSITY;

enum cairo_antialias_t
{
    DEFAULT,
    NONE,
    GRAY,
    SUBPIXEL,
    FAST,
    GOOD,
    BEST
}

alias CAIRO_ANTIALIAS_DEFAULT = cairo_antialias_t.DEFAULT;
alias CAIRO_ANTIALIAS_NONE = cairo_antialias_t.NONE;
alias CAIRO_ANTIALIAS_GRAY = cairo_antialias_t.GRAY;
alias CAIRO_ANTIALIAS_SUBPIXEL = cairo_antialias_t.SUBPIXEL;
alias CAIRO_ANTIALIAS_FAST = cairo_antialias_t.FAST;
alias CAIRO_ANTIALIAS_GOOD = cairo_antialias_t.GOOD;
alias CAIRO_ANTIALIAS_BEST = cairo_antialias_t.BEST;

enum cairo_fill_rule_t
{
    WINDING,
    EVEN_ODD
}

alias CAIRO_FILL_RULE_WINDING = cairo_fill_rule_t.WINDING;
alias CAIRO_FILL_RULE_EVEN_ODD = cairo_fill_rule_t.EVEN_ODD;

enum cairo_line_cap_t
{
	BUTT,
	ROUND,
	SQUARE,
}

alias CAIRO_LINE_CAP_BUTT = cairo_line_cap_t.BUTT;
alias CAIRO_LINE_CAP_ROUND = cairo_line_cap_t.ROUND;
alias CAIRO_LINE_CAP_SQUARE = cairo_line_cap_t.SQUARE;

enum cairo_line_join_t
{
    MITER,
    ROUND,
    BEVEL
}

alias CAIRO_LINE_JOIN_MITER = cairo_line_join_t.MITER;
alias CAIRO_LINE_JOIN_ROUND = cairo_line_join_t.ROUND;
alias CAIRO_LINE_JOIN_BEVEL = cairo_line_join_t.BEVEL;

enum cairo_text_cluster_flags_t
{
    BACKWARD = 0x00000001
}

alias CAIRO_TEXT_CLUSTER_FLAG_BACKWARD = cairo_text_cluster_flags_t.BACKWARD;

enum cairo_font_slant_t
{
    NORMAL,
    ITALIC,
    OBLIQUE
}

alias CAIRO_FONT_SLANT_NORMAL = cairo_font_slant_t.NORMAL;
alias CAIRO_FONT_SLANT_ITALIC = cairo_font_slant_t.ITALIC;
alias CAIRO_FONT_SLANT_OBLIQUE = cairo_font_slant_t.OBLIQUE;

enum cairo_font_weight_t
{
    NORMAL,
    BOLD
}

alias CAIRO_FONT_WEIGHT_NORMAL = cairo_font_weight_t.NORMAL;
alias CAIRO_FONT_WEIGHT_BOLD = cairo_font_weight_t.BOLD;

enum cairo_subpixel_order_t
{
    DEFAULT,
    RGB,
    BGR,
    VRGB,
    VBGR
}

alias CAIRO_SUBPIXEL_ORDER_DEFAULT = cairo_subpixel_order_t.DEFAULT;
alias CAIRO_SUBPIXEL_ORDER_RGB = cairo_subpixel_order_t.RGB;
alias CAIRO_SUBPIXEL_ORDER_BGR = cairo_subpixel_order_t.BGR;
alias CAIRO_SUBPIXEL_ORDER_VRGB = cairo_subpixel_order_t.VRGB;
alias CAIRO_SUBPIXEL_ORDER_VBGR = cairo_subpixel_order_t.VBGR;

enum cairo_hint_style_t
{
    DEFAULT,
    NONE,
    SLIGHT,
    MEDIUM,
    FULL
}

alias CAIRO_HINT_STYLE_DEFAULT = cairo_hint_style_t.DEFAULT;
alias CAIRO_HINT_STYLE_NONE = cairo_hint_style_t.NONE;
alias CAIRO_HINT_STYLE_SLIGHT = cairo_hint_style_t.SLIGHT;
alias CAIRO_HINT_STYLE_MEDIUM = cairo_hint_style_t.MEDIUM;
alias CAIRO_HINT_STYLE_FULL = cairo_hint_style_t.FULL;

enum cairo_hint_metrics_t
{
    DEFAULT,
    OFF,
    ON
}

alias CAIRO_HINT_METRICS_DEFAULT = cairo_hint_metrics_t.DEFAULT;
alias CAIRO_HINT_METRICS_OFF = cairo_hint_metrics_t.OFF;
alias CAIRO_HINT_METRICS_ON = cairo_hint_metrics_t.ON;

enum cairo_font_type_t
{
    TOY,
    FT,
    WIN32,
    QUARTZ,
    USER
}

alias CAIRO_FONT_TYPE_TOY = cairo_font_type_t.TOY;
alias CAIRO_FONT_TYPE_FT = cairo_font_type_t.FT;
alias CAIRO_FONT_TYPE_WIN32 = cairo_font_type_t.WIN32;
alias CAIRO_FONT_TYPE_QUARTZ = cairo_font_type_t.QUARTZ;
alias CAIRO_FONT_TYPE_USER = cairo_font_type_t.USER;

enum cairo_path_data_type_t
{
    MOVE_TO,
    LINE_TO,
    CURVE_TO,
    CLOSE_PATH
}

alias CAIRO_PATH_DATA_TYPE_MOVE_TO = cairo_path_data_type_t.MOVE_TO;
alias CAIRO_PATH_DATA_TYPE_LINE_TO = cairo_path_data_type_t.LINE_TO;
alias CAIRO_PATH_DATA_TYPE_CURVE_TO = cairo_path_data_type_t.CURVE_TO;
alias CAIRO_PATH_DATA_TYPE_CLOSE_PATH = cairo_path_data_type_t.CLOSE_PATH;

enum cairo_device_type_t
{
    DRM,
    GL,
    SCRIPT,
    XCB,
    XLIB,
    XML,
    COGL,
    WIN32,

    INVALID = -1
}

alias CAIRO_DEVICE_TYPE_DRM = cairo_device_type_t.DRM;
alias CAIRO_DEVICE_TYPE_GL = cairo_device_type_t.GL;
alias CAIRO_DEVICE_TYPE_SCRIPT = cairo_device_type_t.SCRIPT;
alias CAIRO_DEVICE_TYPE_XCB = cairo_device_type_t.XCB;
alias CAIRO_DEVICE_TYPE_XLIB = cairo_device_type_t.XLIB;
alias CAIRO_DEVICE_TYPE_XML = cairo_device_type_t.XML;
alias CAIRO_DEVICE_TYPE_COGL = cairo_device_type_t.COGL;
alias CAIRO_DEVICE_TYPE_WIN32 = cairo_device_type_t.WIN32;
alias CAIRO_DEVICE_TYPE_INVALID = cairo_device_type_t.INVALID;

enum cairo_surface_observer_mode_t
{
	NORMAL = 0,
	RECORD_OPERATIONS = 0x1
}

alias CAIRO_SURFACE_OBSERVER_MODE_NORMAL =  cairo_surface_observer_mode_t.NORMAL;
alias CAIRO_SURFACE_OBSERVER_MODE_RECORD_OPERATIONS = cairo_surface_observer_mode_t.RECORD_OPERATIONS;

enum cairo_surface_type_t
{
    IMAGE,
    PDF,
    PS,
    XLIB,
    XCB,
    GLITZ,
    QUARTZ,
    WIN32,
    BEOS,
    DIRECTFB,
    SVG,
    OS2,
    WIN32_PRINTING,
    QUARTZ_IMAGE,
    SCRIPT,
    QT,
    RECORDING,
    VG,
    GL,
    DRM,
    TEE,
    XML,
    SKIA,
    SUBSURFACE,
    COGL
}

alias CAIRO_SURFACE_TYPE_IMAGE = cairo_surface_type_t.IMAGE;
alias CAIRO_SURFACE_TYPE_PDF = cairo_surface_type_t.PDF;
alias CAIRO_SURFACE_TYPE_PS = cairo_surface_type_t.PS;
alias CAIRO_SURFACE_TYPE_XLIB = cairo_surface_type_t.XLIB;
alias CAIRO_SURFACE_TYPE_XCB = cairo_surface_type_t.XCB;
alias CAIRO_SURFACE_TYPE_GLITZ = cairo_surface_type_t.GLITZ;
alias CAIRO_SURFACE_TYPE_QUARTZ = cairo_surface_type_t.QUARTZ;
alias CAIRO_SURFACE_TYPE_WIN32 = cairo_surface_type_t.WIN32;
alias CAIRO_SURFACE_TYPE_BEOS = cairo_surface_type_t.BEOS;
alias CAIRO_SURFACE_TYPE_DIRECTFB = cairo_surface_type_t.DIRECTFB;
alias CAIRO_SURFACE_TYPE_SVG = cairo_surface_type_t.SVG;
alias CAIRO_SURFACE_TYPE_OS2 = cairo_surface_type_t.OS2;
alias CAIRO_SURFACE_TYPE_WIN32_PRINTING = cairo_surface_type_t.WIN32_PRINTING;
alias CAIRO_SURFACE_TYPE_QUARTZ_IMAGE = cairo_surface_type_t.QUARTZ_IMAGE;
alias CAIRO_SURFACE_TYPE_SCRIPT = cairo_surface_type_t.SCRIPT;
alias CAIRO_SURFACE_TYPE_QT = cairo_surface_type_t.QT;
alias CAIRO_SURFACE_TYPE_RECORDING = cairo_surface_type_t.RECORDING;
alias CAIRO_SURFACE_TYPE_VG = cairo_surface_type_t.VG;
alias CAIRO_SURFACE_TYPE_GL = cairo_surface_type_t.GL;
alias CAIRO_SURFACE_TYPE_DRM = cairo_surface_type_t.DRM;
alias CAIRO_SURFACE_TYPE_TEE = cairo_surface_type_t.TEE;
alias CAIRO_SURFACE_TYPE_XML = cairo_surface_type_t.XML;
alias CAIRO_SURFACE_TYPE_SKIA = cairo_surface_type_t.SKIA;
alias CAIRO_SURFACE_TYPE_SUBSURFACE = cairo_surface_type_t.SUBSURFACE;
alias CAIRO_SURFACE_TYPE_COGL = cairo_surface_type_t.COGL;

enum CAIRO_MIME_TYPE_JPEG = "image/jpeg";
enum CAIRO_MIME_TYPE_PNG = "image/png";
enum CAIRO_MIME_TYPE_JP2 = "image/jp2";
enum CAIRO_MIME_TYPE_URI = "text/x-uri";
enum CAIRO_MIME_TYPE_UNIQUE_ID = "application/x-cairo.uuid";

enum cairo_pattern_type_t
{
    SOLID,
    SURFACE,
    LINEAR,
    RADIAL,
    MESH,
    RASTER_SOURCE
}

alias CAIRO_PATTERN_TYPE_SOLID = cairo_pattern_type_t.SOLID;
alias CAIRO_PATTERN_TYPE_SURFACE = cairo_pattern_type_t.SURFACE;
alias CAIRO_PATTERN_TYPE_LINEAR = cairo_pattern_type_t.LINEAR;
alias CAIRO_PATTERN_TYPE_RADIAL = cairo_pattern_type_t.RADIAL;
alias CAIRO_PATTERN_TYPE_MESH = cairo_pattern_type_t.MESH;
alias CAIRO_PATTERN_TYPE_RASTER_SOURCE = cairo_pattern_type_t.RASTER_SOURCE;

enum cairo_extend_t
{
    NONE,
    REPEAT,
    REFLECT,
    PAD
}

alias CAIRO_EXTEND_NONE = cairo_extend_t.NONE;
alias CAIRO_EXTEND_REPEAT = cairo_extend_t.REPEAT;
alias CAIRO_EXTEND_REFLECT = cairo_extend_t.REFLECT;
alias CAIRO_EXTEND_PAD = cairo_extend_t.PAD;

enum cairo_filter_t
{
    FAST,
    GOOD,
    BEST,
    NEAREST,
    BILINEAR,
    GAUSSIAN
}

enum cairo_region_overlap_t
{
    IN,
    OUT,
    PART
}

alias CAIRO_REGION_OVERLAP_IN = cairo_region_overlap_t.IN;
alias CAIRO_REGION_OVERLAP_OUT = cairo_region_overlap_t.OUT;
alias CAIRO_REGION_OVERLAP_PART = cairo_region_overlap_t.PART;

alias CairoAntialias = cairo_antialias_t;
alias CairoContent = cairo_content_t;
alias CairoDeviceType = cairo_device_type_t;
alias CairoExtend = cairo_extend_t;
alias CairoFillRule = cairo_fill_rule_t;
alias CairoFilter = cairo_filter_t;
alias CairoFontSlant = cairo_font_slant_t;
alias CairoFontType = cairo_font_type_t;
alias CairoFormat = cairo_format_t;
alias CairoHintMetrics = cairo_hint_metrics_t;
alias CairoHintStyle = cairo_hint_style_t;
alias CairoLineCap = cairo_line_cap_t;
alias CairoLineJoin = cairo_line_join_t;
alias CairoOperator = cairo_operator_t;
alias CairoPathDataType = cairo_path_data_type_t;
alias CairoPatternType = cairo_pattern_type_t;
alias CairoRegionOverlap = cairo_region_overlap_t;
alias CairoSubpixelOrder = cairo_subpixel_order_t;
alias CairoSurfaceObserverMode = cairo_surface_observer_mode_t;
alias CairoSurfaceType = cairo_surface_type_t;
alias CairoStatus = cairo_status_t;
alias CairoTextClusterFlags = cairo_text_cluster_flags_t;
alias CairoWeight = cairo_font_weight_t;

module cairo.context;


import std.exception;
import std.math;

import cairo.classic;
import cairo.surface;
import cairo.matrix;
import cairo.pattern;
import cairo.font;

class CairoContext
{
private:
	cairo_t* _context;

	void context (cairo_t* c)
	{
		assert (c);
		_context = c;
		enforce (status() == CairoStatus.SUCCESS);
	}

	this () {}

public:
	cairo_t* context ()
	{
		assert (_context);
		return _context;
	}

	this (CairoSurface s)
	{
        context = cairo_create (s.surface);
	}

	~this ()
	{
		cairo_destroy (_context);
	}

	static CairoContext create_reference (cairo_t* ct)
	{
		auto c = new CairoContext ();
		c.context = cairo_reference (ct);
		return c;
	}


/*
	void cairo_append_path (const cairo_path_t* path)
	{
		void cairo_append_path (_context, path);
	}
*/
	void arc (double xc, double yc, double radius, double angle1, double angle2)
	{
		cairo_arc (_context, xc, yc, radius, angle1, angle2);
	}

	void arc_negative (double xc, double yc, double radius, double angle1, double angle2)
	{
		cairo_arc_negative (_context, xc, yc, radius, angle1, angle2);
	}

	void clip ()
	{
		cairo_clip (_context);
	}

	void clip_extents (out double x1, out double y1, out double x2, out double y2)
	{
		cairo_clip_extents (_context, &x1, &y1, &x2, &y2);
	}

	void preserve ()
	{
		cairo_clip_preserve (_context);
	}

	void close_path ()
	{
		cairo_close_path (_context);
	}
/*
	cairo_rectangle_list_t* cairo_copy_clip_rectangle_list ()
	{
		cairo_rectangle_list_t* cairo_copy_clip_rectangle_list ();
	}
*/
	void copy_page ()
	{
		cairo_copy_page (_context);
	}
/*
	cairo_path_t* cairo_copy_path ()
	{
		cairo_path_t* cairo_copy_path ();
	}

	cairo_path_t* cairo_copy_path_flat ()
	{
		cairo_path_t* cairo_copy_path_flat ();
	}
*/
	void curve_to (double x1, double y1, double x2, double y2, double x3, double y3)
	{
		cairo_curve_to (_context, x1, y1, x2, y2, x3, y3);
	}

	void device_to_user (out double x, out double y)
	{
		cairo_device_to_user (_context, &x, &y);
	}

	void device_to_user_distance (out double dx, out double dy)
	{
		cairo_device_to_user_distance (_context, &dx, &dy);
	}

	void fill ()
	{
		cairo_fill (_context);
	}

	void fill_extents (out double x1, out double y1, out double x2, out double y2)
	{
		cairo_fill_extents (_context, &x1, &y1, &x2, &y2);
	}

	void fill_preserve ()
	{
		cairo_fill_preserve (_context);
	}
/*
	void cairo_font_extents (cairo_font_extents_t* extents)
	{
		void cairo_font_extents (_context, cairo_font_extents_t* extents);
	}
*/
	CairoAntialias get_antialias ()
	{
		return cairo_get_antialias (_context);
	}

	void get_current_point (out double x, out double y)
	{
		cairo_get_current_point (_context, &x, &y);
	}
/*
	void cairo_get_dash (double* dashes, double* offset)
	{
		void cairo_get_dash (_context, double* dashes, double* offset);
	}
*/
	int get_dash_count ()
	{
		return cairo_get_dash_count (_context);
	}

	CairoFillRule get_fill_rule ()
	{
		return cairo_get_fill_rule (_context);
	}

	CairoFontFace get_font_face ()
	{
		return new CairoFontFace (cairo_get_font_face (_context));
	}

	CairoMatrix get_font_matrix ()
	{
		CairoMatrix matrix;
		cairo_get_font_matrix (_context, matrix.ptr);
		return matrix;
	}

	CairoFontOptions get_font_options ()
	{
		auto options = cairo_font_options_create ();
		cairo_get_font_options (_context, options);
		return new CairoFontOptions (options);
	}

	CairoSurface get_group_target ()
	{
		return CairoSurface.create_reference (cairo_get_group_target (_context));
	}

	CairoLineCap get_line_cap ()
	{
		return cairo_get_line_cap (_context);
	}

	CairoLineJoin get_line_join ()
	{
		return cairo_get_line_join (_context);
	}

	double get_line_width ()
	{
		return cairo_get_line_width (_context);
	}

	CairoMatrix get_matrix ()
	{
		CairoMatrix matrix;
		cairo_get_matrix (_context, matrix.ptr);
		return matrix;
	}

	double get_miter_limit ()
	{
		return cairo_get_miter_limit (_context);
	}

	CairoOperator get_operator ()
	{
		return cairo_get_operator (_context);
	}

	CairoScaledFont get_scaled_font ()
	{
		return new CairoScaledFont (cairo_get_scaled_font (_context));
	}

	CairoPattern get_source ()
	{
		return new CairoPattern (cairo_get_source (_context));
	}

	CairoSurface get_target ()
	{
		return CairoSurface.create_reference (cairo_get_target (_context));
	}

	double get_tolerance ()
	{
		return cairo_get_tolerance (_context);
	}
/*
	void* cairo_get_user_data (const cairo_user_data_key_t* key)
	{
		void* cairo_get_user_data (_context, const cairo_user_data_key_t* key);
	}

	void cairo_glyph_extents (const cairo_glyph_t* glyphs, int num_glyphs, cairo_text_extents_t* extents)
	{
		void cairo_glyph_extents (_context, const cairo_glyph_t* glyphs, int num_glyphs, cairo_text_extents_t* extents);
	}

	void cairo_glyph_path (const cairo_glyph_t* glyphs, int num_glyphs)
	{
		void cairo_glyph_path (_context, const cairo_glyph_t* glyphs, int num_glyphs);
	}
*/
	bool has_current_point ()
	{
		return cast(bool) cairo_has_current_point (_context);
	}

	void identity_matrix ()
	{
		cairo_identity_matrix (_context);
	}

	bool in_clip (double x, double y)
	{
		return cast(bool) cairo_in_clip (_context, x, y);
	}

	bool in_fill (double x, double y)
	{
		return cast(bool) cairo_in_fill (_context, x, y);
	}

	bool in_stroke (double x, double y)
	{
		return cast(bool) cairo_in_stroke (_context, x, y);
	}

	void line_to (double x, double y)
	{
		cairo_line_to (_context, x, y);
	}
/*
	void mask (cairo_pattern_t* pattern)
	{
		cairo_mask (_context, cairo_pattern_t* pattern);
	}
*/
	void mask_surface (CairoSurface surface, double surface_x, double surface_y)
	{
		cairo_mask_surface (_context, surface.surface, surface_x, surface_y);
	}

	void move_to (double x, double y)
	{
		cairo_move_to (_context, x, y);
	}

	void new_path ()
	{
		cairo_new_path (_context);
	}

	void new_sub_path ()
	{
		cairo_new_sub_path (_context);
	}

	void paint ()
	{
		cairo_paint (_context);
	}

	void paint_with_alpha (double alpha)
	{
		cairo_paint_with_alpha (_context, alpha);
	}

	void path_extents (out double x1, out double y1, out double x2, out double y2)
	{
		cairo_path_extents (_context, &x1, &y1, &x2, &y2);
	}
/*
	cairo_pattern_t* cairo_pop_group ()
	{
		cairo_pattern_t* cairo_pop_group ();
	}
*/
	void pop_group_to_source ()
	{
		void cairo_pop_group_to_source ();
	}

	void push_group ()
	{
		cairo_push_group (_context);
	}
/*
	void push_group_with_content (cairo_content_t content)
	{
		void cairo_push_group_with_content (_context, cairo_content_t content);
	}
*/
	void rectangle (double x, double y, double width, double height, double edge=0.0)
	{
		if (edge == 0.0) return cairo_rectangle (_context, x, y, width, height);

		double x0 = x;
		double x1 = x+edge;
		double x2 = x+width-edge;
		double x3 = x+width;
		double y0 = y;
		double y1 = y+edge;
		double y2 = y+height-edge;
		double y3 = y+height;

		move_to (x1, y0);
		line_to (x2, y0);
		arc     (x2, y1, edge, -PI_2, 0.0);
		line_to (x3, y2);
		arc     (x2, y2, edge, 0.0, PI_2);
		line_to (x1, y3);
		arc     (x1, y2, edge, PI_2, PI);
		line_to (x0, y1);
		arc     (x1, y1, edge, PI, -PI_2);
		close_path ();
		move_to (x0, y0);
	}

	void rel_curve_to (double dx1, double dy1, double dx2, double dy2, double dx3, double dy3)
	{
		cairo_rel_curve_to (_context, dx1, dy1, dx2, dy2, dx3, dy3);
	}

	void rel_line_to (double dx, double dy)
	{
		cairo_rel_line_to (_context, dx, dy);
	}

	void rel_move_to (double dx, double dy)
	{
		cairo_rel_move_to (_context, dx, dy);
	}

	void reset_clip ()
	{
		cairo_reset_clip (_context);
	}

	void restore ()
	{
		cairo_restore (_context);
	}

	void rotate (double angle)
	{
		cairo_rotate (_context, angle);
	}

	void save ()
	{
		cairo_save (_context);
	}

	void scale (double sx, double sy)
	{
		cairo_scale (_context, sx, sy);
	}
/*
	void cairo_select_font_face (const char* family, cairo_font_slant_t slant, cairo_font_weight_t weight)
	{
		void cairo_select_font_face (_context, const char* family, cairo_font_slant_t slant, cairo_font_weight_t weight);
	}
*/
	void set_antialias (CairoAntialias antialias)
	{
		cairo_set_antialias (_context, antialias);
	}
/*
	void cairo_set_dash (const double* dashes, int num_dashes, double offset)
	{
		void cairo_set_dash (_context, const double* dashes, int num_dashes, offset);
	}
/*
	void cairo_set_fill_rule (cairo_fill_rule_t fill_rule)
	{
		void cairo_set_fill_rule (_context, cairo_fill_rule_t fill_rule);
	}
/*
	void cairo_set_font_face (cairo_font_face_t* font_face)
	{
		void cairo_set_font_face (_context, cairo_font_face_t* font_face);
	}
*/
	void set_font_matrix (in CairoMatrix matrix)
	{
		cairo_set_font_matrix (_context, matrix.ptr);
	}
/*
	void cairo_set_font_options (const cairo_font_options_t* options)
	{
		void cairo_set_font_options (_context, const cairo_font_options_t* options);
	}
*/
	void set_font_size (double size)
	{
		cairo_set_font_size (_context, size);
	}

	void set_line_cap (CairoLineCap line_cap)
	{
		cairo_set_line_cap (_context, line_cap);
	}

	void set_line_join (CairoLineJoin line_join)
	{
		cairo_set_line_join (_context, line_join);
	}

	void set_line_width (double width)
	{
		cairo_set_line_width (_context, width);
	}

	void set_matrix (in CairoMatrix matrix)
	{
		cairo_set_matrix (_context, matrix.ptr);
	}

	void set_miter_limit (double limit)
	{
		cairo_set_miter_limit (_context, limit);
	}

	void set_operator (CairoOperator op)
	{
		cairo_set_operator (_context, op);
	}

	void set_scaled_font (const CairoScaledFont scaled_font)
	{
		cairo_set_scaled_font (_context, scaled_font);
	}

	void set_source (CairoPattern source)
	{
		cairo_set_source (_context, source);
	}

	void set_source_rgb (double red, double green, double blue)
	{
		cairo_set_source_rgb (_context, red, green, blue);
	}

	void set_source_rgba (double red, double green, double blue, double alpha)
	{
		cairo_set_source_rgba (_context, red, green, blue, alpha);
	}

	void set_source_surface (CairoSurface surface, double x, double y)
	{
		cairo_set_source_surface (_context, surface.surface, x, y);
	}

	void set_tolerance (double tolerance)
	{
		cairo_set_tolerance (_context, tolerance);
	}
/*
	cairo_status_t cairo_set_user_data (const cairo_user_data_key_t* key, void* user_data, cairo_destroy_func_t destroy)
	{
		cairo_status_t cairo_set_user_data (_context, const cairo_user_data_key_t* key, void* user_data, cairo_destroy_func_t destroy);
	}
*/
	void show_glyphs (const CairoGlyph[] glyphs)
	{
		enforce (glyphs.length < int.max);
		cairo_show_glyphs (_context, glyphs.ptr, cast(int)glyphs.length);
	}

	void show_page ()
	{
		cairo_show_page (_context);
	}
/*
	void cairo_show_text (const char* utf8)
	{
		void cairo_show_text (_context, const char* utf8);
	}
/*
	void cairo_show_text_glyphs (const char* utf8, int utf8_len, const cairo_glyph_t* glyphs, int num_glyphs, const cairo_text_cluster_t* clusters, int num_clusters, cairo_text_cluster_flags_t cluster_flags)
	{
		void cairo_show_text_glyphs (_context, const char* utf8, int utf8_len, const cairo_glyph_t* glyphs, int num_glyphs, const cairo_text_cluster_t* clusters, int num_clusters, cairo_text_cluster_flags_t cluster_flags);
	}
*/

	CairoStatus status ()
	{
		return cairo_status (_context);
	}

	void stroke ()
	{
		cairo_stroke (_context);
	}

	void stroke_extents (out double x1, out double y1, out double x2, out double y2)
	{
		cairo_stroke_extents (_context, &x1, &y1, &x2, &y2);
	}

	void stroke_preserve ()
	{
		cairo_stroke_preserve (_context);
	}
/*
	void cairo_text_extents (const char* utf8, cairo_text_extents_t* extents)
	{
		void cairo_text_extents (_context, const char* utf8, cairo_text_extents_t* extents);
	}
/*
	void cairo_text_path (const char* utf8)
	{
		void cairo_text_path (_context, const char* utf8);
	}
*/
	void transform (in CairoMatrix m)
	{
		cairo_transform (_context, m.ptr);
	}

	void translate (double tx, double ty)
	{
		cairo_translate (_context, tx, ty);
	}

	void user_to_device (out double x, out double y)
	{
		cairo_user_to_device (_context, &x, &y);
	}

	void user_to_device_distance (out double dx, out double dy)
	{
		cairo_user_to_device_distance (_context, &dx, &dy);
	}
}

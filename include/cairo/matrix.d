module cairo.matrix;

import cairo.classic;

import std.exception;

struct CairoMatrix
{
	cairo_matrix_t _matrix = {1, 0, 0, 1, 0, 0};
	alias _matrix this;

	this (cairo_matrix_t m)
	{
		_matrix = m;
	}

	this (double xx, double yx, double xy, double yy, double x0, double y0)
	{
		_matrix.xx = xx;
		_matrix.yx = yx;
		_matrix.xy = xy;
		_matrix.yy = yy;
		_matrix.x0 = x0;
		_matrix.y0 = y0;
	}

	static CairoMatrix init_translate (double tx, double ty)
	{
		cairo_matrix_t m;
		cairo_matrix_init_translate (&m, tx, ty);
		return CairoMatrix (m);
	}

	static CairoMatrix init_scale (double sx, double sy)
	{
		cairo_matrix_t m;
		cairo_matrix_init_scale (&m, sx, sy);
		return CairoMatrix (m);
	}

	static CairoMatrix init_rotate (double radians)
	{
		cairo_matrix_t m;
		cairo_matrix_init_rotate (&m, radians);
		return CairoMatrix (m);
	}

	cairo_matrix_t matrix ()
	{
        return _matrix;
    }

	inout (cairo_matrix_t*) ptr () inout
	{
		auto ptr = &_matrix;
		return ptr;
	}

	void translate (double tx, double ty)
	{
		cairo_matrix_translate (&_matrix, tx, ty);
	}

	void scale (double sx, double sy)
	{
		cairo_matrix_scale (&_matrix, sx, sy);
	}

	void rotate (double radians)
	{
		cairo_matrix_rotate (&_matrix, radians);
	}

	void invert ()
	{
		auto status = cairo_matrix_invert (&_matrix);
		if (status != CAIRO_STATUS_SUCCESS) reset;
	}

	void reset ()
	{
		_matrix = cairo_matrix_t (1, 0, 0, 1, 0, 0);
	}

	CairoMatrix translated (double tx, double ty)
	{
		cairo_matrix_t m = _matrix;
		cairo_matrix_translate (&m, tx, ty);
		return CairoMatrix (m);
	}

	CairoMatrix scaled (double sx, double sy)
	{
		cairo_matrix_t m = _matrix;
		cairo_matrix_scale (&m, sx, sy);
		return CairoMatrix (m);
	}

	CairoMatrix rotated (double radians)
	{
		cairo_matrix_t m = _matrix;
		cairo_matrix_rotate (&m, radians);
		return CairoMatrix (m);
	}

	CairoMatrix inverted ()
	{
		cairo_matrix_t m = _matrix;
		auto status = cairo_matrix_invert (&m);
		if (status == CAIRO_STATUS_SUCCESS) return CairoMatrix (m);
		else                                return CairoMatrix ();
	}

	void transform_distance (ref double dx, ref double dy)
	{
		cairo_matrix_transform_distance (&_matrix, &dx, &dy);
	}

	void transform_distance (double x, double y, ref double tx, ref double ty)
	{
		tx = x;
		ty = y;
		cairo_matrix_transform_distance (&_matrix, &tx, &ty);
	}

	void transform_point (ref double x, ref double y)
	{
		cairo_matrix_transform_point (&_matrix, &x, &y);
	}

	void transform_point (double x, double y, ref double tx, ref double ty)
	{
		tx = x;
		ty = y;
		cairo_matrix_transform_point (&_matrix, &tx, &ty);
	}

	static CairoMatrix multiply (in CairoMatrix a, in CairoMatrix b)
	{
		cairo_matrix_t result;
		cairo_matrix_multiply (&result, a.ptr, b.ptr);
		return CairoMatrix (result);
	}

	const CairoMatrix opBinary(string op) (in CairoMatrix b) if (op == "*")
	{
		return multiply (this, b);
	}
}
